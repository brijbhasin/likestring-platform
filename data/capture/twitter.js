var config = require('../../config/auth').twitterAuth;
var Twit = require('twit');
var querystring = require('querystring');
var async = require('async');
var urlExpander = require('expand-url');
var embedly = require('embedly'),
    util = require('util');

var log = require('../../app/log');
var Utils = require('../../app/models/utils');

var Twitter = module.exports = function() {

  var cfg = {};

  cfg.consumer_key = config.consumerKey;
  cfg.consumer_secret = config.consumerSecret;
  cfg.access_token = config.access_token_key;
  cfg.access_token_secret = config.access_token_secret;
  
  this.twit = new Twit(cfg);
};

Twitter.prototype.verifyCredentials = function(callback) {
  var url = 'account/verify_credentials';
  this.twit.get(url, null, callback);
  return this;
}

//Returns a variety of information about the user specified by the required user_id or screen_name parameter.
Twitter.prototype.getUserProfile = function(id, callback) {
  if ( typeof id === 'function') {
     throw new Error('User ID or screen_name is  required');
     return this;
  }

  if ( typeof callback !== 'function') {
     throw new Error('INVALID CALLBACK');
     return this;
  }

  var params = {};
  if (typeof id === 'string')
    params.screen_name = id;
  else if (typeof id === 'number')
    params.user_id = id;

  log.debug("Twitter:getUserProfile. Params = %s", JSON.stringify(params));

  this.twit.get('users/show', params, callback);
  return this;
}

//
// This API will first get a list of friend IDs and then make another call
// to get the actual twitter profiles for each of those IDs.
// Twitter might not return profiles for some of those IDs as result of the 'user/lookup'
// call. SO THE NUMBER OF PROFILES RETURNED COULD BE LESS THAN the "friends_count" for the
// given user.
//     For example, for 'twitterapi' I didnt get profiles for these friend IDs - 14488353,11750202
//                  this meant that I got 46 profiles whereas the "friends_count" was 48!
//
Twitter.prototype.getFriends = function(id, callback) {
  if (typeof id === 'function') {
    callback = name;
    name = null;
  }

  var params = { key: 'ids' };
  if (typeof id === 'string')
    params.screen_name = id;
  else if (typeof id === 'number')
    params.user_id = id;
  
  //Twitter will return the UserIDs of all the people followed by the given 'id'.
  //Once we get this list of ids, we will have to query for their profiles and return the
  //array of profiles.
  //
  //
  var self = this;
  self._getUsingCursor('friends/ids', params, function(err, idsArray) {
    if (err) return callback(err);
    
    if ((idsArray instanceof Array) && (idsArray.length > 0)) {

      log.debug("Twitter:getFriends. For %s, we got friend IDs: %s", id, idsArray.toString());
      
      //Note: The twitter user lookup API v1.1 will accept only upto 100 ids at a time.
      //So slice it up and fetch 100 profiles at a time.
      var idSubsets = [];
      while (idsArray.length) {
        idSubsets.push( idsArray.splice(0, 100) );
      }
      
      async.concat(idSubsets, fetchProfiles, callback);

      function fetchProfiles(ids, cb) {

        self.twit.get('users/lookup', {user_id: ids.toString()}, function(e, d) {
          if (e) cb(e);
          return cb(null, d);
        });
      }
    }
  });

  return this;

}

Twitter.prototype.search = function(q, callback) {
  if (typeof q === 'function') {
    throw new Error('FAIL: Search conditions are required.');
    return this;
  }

  if ( typeof callback !== 'function' ) {
    throw new Error('FAIL: INVALID CALLBACK.');
    return this;
  }

  log.debug("Twitter:Search. Search Query: %s", q);
  this._getUsingMaxId('search/tweets', {q:q}, callback);

  return this;
}

Twitter.prototype.tweets = function(id, params, callback) {
  if (typeof id === 'function') {
    throw new Error('FAIL: User ID or screen_name is required.');
    return this;
  }

  if (typeof params === 'function') {
     callback = params;
     params = {};
  } 

  if ( typeof callback !== 'function' ) {
    throw new Error('FAIL: INVALID CALLBACK.');
    return this;
  }

  var newParams = {};
  
  //Unless explicitly asked for, filter out the retweets.
  if (!params || !params.include_rts) {
    newParams.include_rts = false;
  }
  params = this.mergeParms(params, newParams);

  if (typeof id === 'string')
    params.screen_name = id;
  else if (typeof id === 'number')
    params.user_id = id;

  log.debug("Twitter:Tweets. Getting Tweets from the user timeline with parameters: %s", JSON.stringify(params));
  this._getUsingMaxId('statuses/user_timeline', params, callback);
  return this;
}

Twitter.prototype.rateLimitStatus = function(callback) {
  var url = '/application/rate_limit_status';
  this.twit.get(url, null, callback);
  return this;
}

/*
 * INTERNAL UTILITY FUNCTIONS
 */

/*
 * Merge objects into the first one
 */

Twitter.prototype.mergeParms = function(defaults) {
  for(var i = 1; i < arguments.length; i++){
    for(var opt in arguments[i]){
      defaults[opt] = arguments[i][opt];
    }
  }
  return defaults;
};

Twitter.prototype._getUsingCursor = function(url, params, callback) {
  var self = this,
    params = params || {},
    key = params.key || null,
    result = [];

  // if we don't have a key to fetch, we're screwed
  if (!key)
    callback(new Error('FAIL: Results key must be provided to _getUsingCursor().'));
  delete params.key;

  // kick off the first request, using cursor -1
  params = self.mergeParms(params, {cursor:-1});
  this.twit.get(url, params, fetch);

  function fetch(err, data) {
    if (err) {
      return callback(err);
    }

    // FIXME: what if data[key] is not a list?
    if (data[key]) result = result.concat(data[key]);

    if (data.next_cursor_str === '0') {
      callback(null, result);
    } else {
      params.cursor = data.next_cursor_str;
      self.twit.get(url, params, fetch);
    }
  }

  return this;
}

Twitter.prototype._getUsingMaxId = function(url, params, callback) {
  var self = this,
    params = params || {},
    max_id = null,
    result = [];

  //In case the user only wants N tweets then return just that much or less.
  var maxCount;
  if (params.hasOwnProperty('count')) {
     maxCount = params.count;
  } else {
    //Limiting the maximum number of tweets to fetch.
    maxCount = 3000;
  }
  params.count = params.count || 200; //Number of tweets fetched per API call

  // kick off the first request without using the max_id
  log.debug("Twitter:GetTweetsUsingMaxID: URL - %s, Parameters - %s", url, JSON.stringify(params));
  this.twit.get(url, params, fetch);

  function fetch(err, data) {
    if (err) {
      log.error("Twitter:GetTweetsUsingMaxID: Error - %s", (err.err || err.message));
      return callback(err);
    }

    // If this called by the twitter search, then the data is returned in a sub-document - "statuses"
    // and hence this correction below to ensure this function works for user timeline tweets and searches as well.
    if (data.statuses) {
      data = data.statuses;
      log.debug("Twitter.GetTweetsUsingMaxID: Number of tweets obtained %d", data.length);
    }    
    if (data.length == 0) {
       log.debug("Twitter.GetTweetsUsingMaxID: Got %d tweets", result.length);
       callback(null, result);
    } else {
       result = result.concat(data);
       if (result.length >= maxCount) {
          //User only wants N tweets so lets give him only that much!
          log.debug("Twitter.GetTweetsUsingMaxID: Returning %d tweets", result.length);
          return callback(null, result);
       }

       if (data.search_metadata) {
         if (data.search_metadata.next_results) {
           params.max_id = Number(data.search_metadata.next_results.split('max_id=')[1].split('&')[0]);
         } else {
           //End of search results
           return callback(null, result);
         }
       } else {
         // This is not search. It is user timeline
         var id = data.slice(-1)[0].id - 1;
         if (id == params.max_id) {
           //We have exhausted the timeline since the tweet with the max id is the only one being returned...
           return callback(null, result);
         } else {
           params.max_id = id;
           log.debug("Twitter.GetTweetsUsingMaxID: Getting tweets with max ID", params.max_id);
         }
       }

       log.debug("Twitter:GetTweetsUsingMaxID: URL - %s, Parameters - %s", url, JSON.stringify(params));
       self.twit.get(url, params, fetch);
    }
    
  }
}

//This routine shall remove duplicate media (photo/video/article links) from the tweets
Twitter.prototype.removeDuplicates = function(tweets, callback) {
  
  log.info("Twitter.removeDuplicates: About to remove duplicate media/urls in the tweets.");

  if (typeof tweets === 'function') {
    callback(new Error('No tweets available!'));
    return this;
  }

  if ( typeof callback !== 'function') {
     callback(new Error('INVALID CALLBACK'));
     return this;
  }

  if (!tweets || (tweets && (!tweets.length || (tweets.length == 0)))) {
    log.silly("Twitter.removeDuplicates: No tweets to fix. Returning...");
    callback(null, []);
    return this;
  }

  var self = this;
  process.nextTick(function() {
    var mediaMap = {};
    var urlMap = {};
    tweets.forEach(function(tweet) {
      if (!tweet.entities)
        return;
      if (tweet.entities.media && (tweet.entities.media.length > 0)) {
        var i=0;
        while ((tweet.entities.media.length > 0) && (i < tweet.entities.media.length)) {

          if (!tweet.entities.media[i]) {
            //Should never occur, but if it does then remove these empty URL entries.
            tweet.entities.media.splice(i,1);
            continue;
          }

          if (tweet.entities.media[i].media_url_https in mediaMap) {
            //discard the duplicate URL entity entry!
            log.silly("Twitter.removeDuplicates: Found duplicate media for ID: %s. Original ID: %s", 
                      tweet.id_str, mediaMap[tweet.entities.media[i].media_url_https]);
            tweet.entities.media.splice(i,1);
          } else {
            mediaMap[tweet.entities.media[i].media_url_https] = tweet.id_str;
            i++;
          }
        }

      } //Are there any media URLs in this tweet?

      if (tweet.entities.urls && (tweet.entities.urls.length > 0)) {

        var i=0;
        while ((tweet.entities.urls.length > 0) && (i < tweet.entities.urls.length)) {

          if (!tweet.entities.urls[i]) {
            //Should never occur, but if it does then remove these empty URL entries.
            tweet.entities.urls.splice(i,1);
            continue;
          }

          if (tweet.entities.urls[i].expanded_url in urlMap) {
            //discard the duplicate URL entity entry!
            log.silly("Twitter.removeDuplicates: Found duplicate URL for ID: %s. Original ID: %s", tweet.id_str, urlMap[tweet.entities.urls[i].expanded_url]);
            tweet.entities.urls.splice(i,1);
          } else {
            urlMap[tweet.entities.urls[i].expanded_url] = tweet.id_str;
            i++;
          }
        }
      }
    }); //foreach Tweet...

    callback(null, tweets);
    return self;
  });
} //removeDuplicates()

/*
 * Sometimes twitter doesnt expand shortened URLs, or some of the embedded links in a tweet could be dead.
 * This routine will attempt to fix some of these issues in the tweets.
 * It will also call external modules/services to extract the right form for displaying articles or links. (We are using embed.ly for now)
 */
Twitter.prototype.processEntityURLs = function(tweets, callback) {
  
  log.info("Twitter.processEntityURLs: About to process the embedded URLs in the tweets.");

  if (typeof tweets === 'function') {
    log.error("Twitter.processEntityURLs: No tweets available!");
    callback(new Error('No tweets available!'));
    return this;
  }

  if ( typeof callback !== 'function') {
     log.error("Twitter.processEntityURLs: No callback!");
     callback(new Error('INVALID CALLBACK'));
     return this;
  }

  if (!tweets || (tweets && (!tweets.length || (tweets.length == 0)))) {
    log.info("Twitter.processEntityURLs: No tweets to fix. Returning...");
    callback(null, []);
    return this;
  }

  log.debug("Twitter.processEntityURLs: %d tweets to be processed.", tweets.length);
  var self = this;

  try {

    async.waterfall([
      function gatherURLs(asyncCB) {
        log.debug("Twitter.processEntityURLs.gatherURLs. ");

        var shortURLs = [];

        tweets.forEach(function(tweet) {
          if (!tweet.entities || !tweet.entities.urls || (tweet.entities.urls.length == 0))
            return; //ignore tweets with no embedded links
          
          var i;
          for (i=0; i < tweet.entities.urls.length; i++) {
            var url = tweet.entities.urls[i];

            /* if (url.expanded_url.match(/\/\/www\.|\.com\b/i)) {
              continue; //No need to expand URLs which have '//www.' or ending with .com
            }*/

            if( (new RegExp( '\\b' + Utils.IGNORE_URL_LIST.join('\\b|\\b') + '\\b', "i") ).test(url.expanded_url) ) {
              continue; //Matching URLs are of known type and easy to classify (later), hence no need to expand.
            }

            //Save this "unique" URL so that we can expand it later.
            if (shortURLs.indexOf(url.expanded_url) < 0) {
              log.silly("Twitter.processEntityURLs. Tweet - %s Embedded URL: %s", tweet.id_str, url.expanded_url);
              shortURLs.push(url.expanded_url);
            }
          }
        }); //foreach tweet...

        asyncCB(null, shortURLs);
      },

      /*function expandURLs(urls, asyncCB) {
        //Lets expand the collected URLs and then replace them within the respective tweets.
        log.debug("Twitter.processEntityURLs.expandURLs: %s URLs have to be expanded.", urls.length);

        var counter=0;
        var longURLMap = {};
        var q = async.queue(function (shortUrl, cb) {
          urlExpander.expand(shortUrl, function(err, longUrl){
            if (err) {
              log.error("Expanding URL: %s. Error - %s", shortUrl, (err.err || err.message));
              longUrl = shortUrl;
            }
            counter++;
            log.silly(counter+": "+longUrl);
            cb(null, longUrl);
          });
        }, 100);

        q.drain = function() {
          log.silly("Twitter.processEntityURLs: All short URLs expanded. Total URLs - %d", counter);

          tweets.forEach(function(tweet) {
            if (!tweet.entities || !tweet.entities.urls || (tweet.entities.urls.length == 0))
              return;
           
            for (i=0; i < tweet.entities.urls.length; i++) {
              var url = tweet.entities.urls[i].expanded_url;
              var j;
              for (j=0; j < urls.length; j++) {
                if (urls[j].localeCompare(url) == 0) {
                  log.silly("Twitter.processEntityURLs: for Tweet - %s Replacing short url [%s] with [%s]", 
                            tweet.id_str, tweet.entities.urls[i].expanded_url, longURLMap[urls[j]]);

                  tweet.entities.urls[i].expanded_url = longURLMap[urls[j]];
                }
              }
            } //for each entity URL...
          }); //foreach Tweet...      

          log.info("Twitter.processEntityURLs: Fixing URLs done. Number of tweets - %d", tweets.length); 
          asyncCB(null, urls, tweets);
        } //q.drain()

        urls.forEach(function(url) {
          q.push(url, function(err, longURL) {
            if (!err) {
              longURLMap[url] = longURL;
            }
          });
        });
      },*/

      function resolveURLs(urls, asyncCB) {
        log.debug("Twitter.resolveURLs. urls - %s", JSON.stringify(urls));
        Utils.resolveURLsWithExtService(urls, function(error, origURLs, expandedURLs) {
          log.debug("Twitter.resolveURLs. Expanded Urls - %s", JSON.stringify(expandedURLs));
          asyncCB(error, origURLs, expandedURLs);
        });
      },

      function addExtendedDataForURLs(urls, expandedURLs, asyncCB) {

        log.debug("Twitter.addExtendedDataForURLs. Total Embedded URLs - %d URL count after expansion - %d", 
                   urls.length, expandedURLs.length);
        
        tweets.forEach(function(tweet) {
          if (!tweet.entities || !tweet.entities.urls || (tweet.entities.urls.length == 0))
            return;
         
          var i, j;
          for (i=0; i < tweet.entities.urls.length; i++) {
            var url = tweet.entities.urls[i].expanded_url;
            for (j=0; j < urls.length; j++) {
              if (urls[j].localeCompare(url) !== 0) {
                continue;
              }
              
              log.silly("Twitter.addExtendedDataForURLs: for Tweet - %s with url [%s] Adding this embedly [%s]", 
                        tweet.id_str, url, JSON.stringify(expandedURLs[j]));

              if (expandedURLs[j].error_code === 404) {
                tweet.entities.urls.splice(i, 1); //Discard this URL since it is inaccessible!
              } else {
                tweet.entities.urls[i].embedlyData = expandedURLs[j];
                log.silly("Twitter.addExtendedDataForURLs. Modifying the URL. Tweet: %s", tweet.id_str);
                log.silly(util.inspect(tweet.entities.urls[i]));
              }
            }
          } //for each entity URL...
        }); //foreach Tweet...      
        
        asyncCB(null, tweets);
      }],

      function(error, modtweets) {
        if (!error) {
          log.info("Twitter.processEntityURLs. Done with processing %d tweets", modtweets.length);
        }
        callback(error, modtweets);
      }
    );

  } catch (err) 
  {
    callback(err);
  };
} //()