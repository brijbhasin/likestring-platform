var config   = require('config');
var mongoose = require('mongoose');
var async    = require('async');
var urlExpander = require('expand-url');

// load up the user model
var UserProfile = require('../../app/models/twitter_user_profile');
var Tweet = require('../../app/models/twitter_tweet');
var log   = require('../../app/log');
var Utils = require('../../app/models/utils');

var TwitterStore = module.exports = function() {
};

TwitterStore.prototype.saveUser = function(user, callback) {
  if (typeof user === 'function') {
    throw new Error('Twitter user profile JSON object is missing');
    return this;
  }

  if ( typeof callback !== 'function') {
     throw new Error('INVALID CALLBACK');
     return this;
  }
  
  process.nextTick(function() {
    UserProfile.findOne({id: user.id}, function(error, existingUser) {
    
      if (error) callback(error);
      if (user.status) delete user.status; //Get rid of the latest tweet for this user.

      if (existingUser) {
         // If the twitter user already exists, then refresh the "raw" data in our store.
         // Also, there might be a list of friend IDs (appended by one of our APIs), in that
         // case bubble up the friend list and delete from "raw" so that it resembles the
         // original JSON doc returned by Twitter.
         if (user.friends) {
            existingUser.friends = user.friends;
            delete user.friends;
         }
         existingUser.raw = user;
         existingUser.save(function(err, savedUser) {
           if (err) callback(err);
           callback(null, savedUser);
         });

      } else {
  
        var profile = new UserProfile();
        profile.id = user.id;
        
        // the JSON twitter profile might already have a new property called 'friends'
        // sent from our API - saveFriends(). If that case bubble this property to the
        // document itself and change the profile to be exactly same as what we received
        // from twitter. 
        if (user.friends) {
        	profile.friends = user.friends;
        	delete user.friends;
        } else {
        	profile.friends = [];
        }
        profile.raw = user;

        profile.save(function(err, savedUser) {
          if (err) throw err;
          callback(null, savedUser);
        });
      } // User exists?
    });
  });

  return this;
};

TwitterStore.prototype.saveFriends = function(user, friends, callback) {

  if (typeof user === 'function') {
    throw new Error('Twitter user profile JSON object is missing');
    return this;
  }

  if ( typeof callback !== 'function') {
     throw new Error('INVALID CALLBACK');
     return this;
  }
  
  //deal with empty array
  var numFriends = friends.length;
  if (numFriends === 0) {
  	callback(null, []);
  	return this;
  }

  var self = this;
  var incomingFriendsHash = {};
  var incomingFriendIDs = friends.map(function(f) { incomingFriendsHash[f.id] = f; return f.id });

  process.nextTick(function() {
  	
  	var savedFriendIDs = [];

    // Only add friends who are missing in our database!
    // Friends_to_save = Incoming - existing
    //
    var existingFriendIDs = [];
    UserProfile.find({id: {$in : incomingFriendIDs}}, {_id: 0, id: 1}, function(error, docs) {
      if (error) callback(error);
      existingFriendIDs = docs.map(function(d) { return d.id});

      var friendsToSaveIDs  = incomingFriendIDs.filter(function(x) { return existingFriendIDs.indexOf(x) < 0 });
      // Only save friends not in the database
      if (friendsToSaveIDs.length > 0) {

        var friendsToSave = [];
        friendsToSaveIDs.forEach(function(id) {
          friendsToSave.push(incomingFriendsHash[id]);
        });

        async.concat(friendsToSave, saveOneFriend, function(error, ids) {
          if (error) callback(error);
        });

        function saveOneFriend(friend, cb) {
          self.saveUser(friend, function(error, saved) {
      	    if (error) cb(error);
            return cb(null, saved.id);
          });
        }
      } //New friend profiles to save?

      // Update friend list for the given user record
      user.friends = incomingFriendIDs;
      self.saveUser(user, function(error, saved) {
        if (error) callback(error);
      	callback(null, incomingFriendIDs);
      });

      return this;
    }); // Find pre-existing friends in the database
  }); //nextTick
  
  return this;
};

TwitterStore.prototype.saveTweets = function(tweets, callback) {
  if (typeof tweets === 'function') {
    throw new Error('Need some tweets to save to the database.');
    return this;
  }

  if ( typeof callback !== 'function') {
     throw new Error('INVALID CALLBACK');
     return this;
  }

  if (!tweets || (tweets && (!tweets.length || (tweets.length == 0)))) {
  	callback(null, []);
  	return this;
  }

  var self = this;
  process.nextTick(function() {

    var db = mongoose.connection.db;

    //Note: Save a large amount of tweets in one go is not working. So lets do it in chunks
    // of 100 each...
    var tweetSlices = [];
    while (tweets.length) {
      tweetSlices.push( tweets.splice(0, 100) );
    }

    async.concat(tweetSlices, saveChunk, function(error, data) {
      if (error) {
        callback(error);
        return self;
      }
      callback(null, data);
      return self;
    });

    function saveChunk(chunk, cb) {
      db.collection('twitter_tweets').insert(chunk, cb);
    }
  });  

}

//This routine shall remove duplicate media (photo/video/article links) from the tweets
TwitterStore.prototype.removeDuplicates = function(tweets, callback) {
  
  log.info("TwitterStore.removeDuplicates: About to remove duplicate media/urls in the tweets.");

  if (typeof tweets === 'function') {
    callback(new Error('No tweets available!'));
    return this;
  }

  if ( typeof callback !== 'function') {
     callback(new Error('INVALID CALLBACK'));
     return this;
  }

  if (!tweets || (tweets && (!tweets.length || (tweets.length == 0)))) {
    log.silly("TwitterStore.removeDuplicates: No tweets to fix. Returning...");
    callback(null, []);
    return this;
  }

  var self = this;
  process.nextTick(function() {
    var mediaMap = {};
    var urlMap = {};
    tweets.forEach(function(tweet) {
      if (!tweet.entities)
        return;
      if (tweet.entities.media && (tweet.entities.media.length > 0)) {
        var i=0;
        while ((tweet.entities.media.length > 0) && (i < tweet.entities.media.length)) {

          if (!tweet.entities.media[i]) {
            //Should never occur, but if it does then remove these empty URL entries.
            tweet.entities.media.splice(i,1);
            continue;
          }

          if (tweet.entities.media[i].media_url_https in mediaMap) {
            //discard the duplicate URL entity entry!
            log.silly("TwitterStore.removeDuplicates: Found duplicate media for ID: %s. Original ID: %s", 
                      tweet.id_str, mediaMap[tweet.entities.media[i].media_url_https]);
            tweet.entities.media.splice(i,1);
          } else {
            mediaMap[tweet.entities.media[i].media_url_https] = tweet.id_str;
            i++;
          }
        }

      } //Are there any media URLs in this tweet?

      if (tweet.entities.urls && (tweet.entities.urls.length > 0)) {

        var i=0;
        while ((tweet.entities.urls.length > 0) && (i < tweet.entities.urls.length)) {

          if (!tweet.entities.urls[i]) {
            //Should never occur, but if it does then remove these empty URL entries.
            tweet.entities.urls.splice(i,1);
            continue;
          }

          if (tweet.entities.urls[i].expanded_url in urlMap) {
            //discard the duplicate URL entity entry!
            log.silly("TwitterStore.removeDuplicates: Found duplicate URL for ID: %s. Original ID: %s", tweet.id_str, urlMap[tweet.entities.urls[i].expanded_url]);
            tweet.entities.urls.splice(i,1);
          } else {
            urlMap[tweet.entities.urls[i].expanded_url] = tweet.id_str;
            i++;
          }
        }
      }
    }); //foreach Tweet...

    callback(null, tweets);
    return self;
  });
} //removeDuplicates()

/*
 * Sometimes twitter doesnt expand shortened URLs, or some of the embedded links in a tweet could be dead.
 * This routine will attempt to fix some of these issues in the tweets
 */
TwitterStore.prototype.fixMediaURLs = function(tweets, callback) {
  
  log.info("TwitterStore.fixMediaURLs: About to cleanse the tweets.");

  if (typeof tweets === 'function') {
    log.error("TwitterStore.fixMediaURLs: No tweets available!");
    callback(new Error('No tweets available!'));
    return this;
  }

  if ( typeof callback !== 'function') {
     log.error("TwitterStore.fixMediaURLs: No callback!");
     callback(new Error('INVALID CALLBACK'));
     return this;
  }

  if (!tweets || (tweets && (!tweets.length || (tweets.length == 0)))) {
    log.info("TwitterStore.fixMediaURLs: No tweets to fix. Returning...");
    callback(null, []);
    return this;
  }

  log.debug("TwitterStore.fixMediaURLs: %d tweets to be processed.", tweets.length);
  var self = this;

  process.nextTick(function() {
    var shortURLs = [];
    var i;
    tweets.forEach(function(tweet) {
      if (!tweet.entities || !tweet.entities.urls || (tweet.entities.urls.length == 0))
        return;
      
      for (i=0; i < tweet.entities.urls.length; i++) {
        var url = tweet.entities.urls[i];

        if (url.expanded_url.match(/\/\/www\.|\.com\b/i)) {
          continue; //No need to expand URLs which have '//www.' or ending with .com
        }

        if( (new RegExp( '\\b' + Utils.IGNORE_URL_LIST.join('\\b|\\b') + '\\b', "i") ).test(url.expanded_url) ) {
          continue; //Matching URLs are of known type and easy to classify (later), hence no need to expand.
        }

        //Save this URL so that we can expand it later.
        if (shortURLs.indexOf(url.expanded_url) < 0) {
          shortURLs.push(url.expanded_url);
        }
      }
    }); //foreach tweet...
    
    //Lets expand the collected URLs and then replace them within the respective tweets.
    log.debug("TwitterStore.fixMediaURLs: %s URLs have to be expanded.", shortURLs.length);

    var counter=0;
    var longURLMap = {};
    var q = async.queue(function (shortUrl, callback) {
      urlExpander.expand(shortUrl, function(err, longUrl){
        if (err) {
          log.error("Expanding URL: %s. Error - %s", shortUrl, (err.err || err.message));
          longUrl = shortUrl;
        }
        counter++;
        log.silly(counter+": "+longUrl);
        callback(null, longUrl);
      });
    }, 100);

    q.drain = function() {
      log.silly("TwitterStore.fixMediaURLs: All short URLs expanded. Total URLs - %d", counter);

      tweets.forEach(function(tweet) {
        if (!tweet.entities || !tweet.entities.urls || (tweet.entities.urls.length == 0))
          return;
       
        for (i=0; i < tweet.entities.urls.length; i++) {
          var url = tweet.entities.urls[i].expanded_url;
          var j;
          for (j=0; j < shortURLs.length; j++) {
            if (shortURLs[j].localeCompare(url) == 0) {
              log.silly("TwitterStore.fixMediaURLs: for Tweet - %s Replacing short url [%s] with [%s]", 
                        tweet.id_str, tweet.entities.urls[i].expanded_url, longURLMap[shortURLs[j]]);

              tweet.entities.urls[i].expanded_url = longURLMap[shortURLs[j]];
            }
          }
        } //for each entity URL...
      }); //foreach Tweet...      

      log.info("TwitterStore.fixMediaURLs: Fixing URLs done. Number of tweets - %d", tweets.length); 
      callback(null, tweets);
      return self;
      
    } //q.drain()

    shortURLs.forEach(function(url) {
      q.push(url, function(err, longURL) {
        if (!err) {
          longURLMap[url] = longURL;
        }
      });
    });

  });
} //()