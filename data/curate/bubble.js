/*
  This module creates and maintains a Bubble within the likeString database.
  Bubble contents are updated with data from social media via "Data source objects".
     Social media data is assumed to be already saved in its raw form within the likeString 
  database. Each social media content is parsed by a different social media algorithm 
  and all of these algorithms return uniform bubble content which is then appended to the bubble
  in the database.
*/

var config   = require('config');
var mongoose = require('mongoose');
var async    = require('async');
var fs       = require('fs');
var moment = require('moment'); //Date formatting module

var log    = require('../../app/log');
var Utils    = require('../../app/models/utils');
var BubbleCategory = require('../../app/models/bubble_category');
var BubbleSchemas = require('../../app/models/bubble'),
    BubbleModel = BubbleSchemas.Bubble,
    BubbleContentModel = BubbleSchemas.BubbleContent;


var BubbleDataSource = require('./bubble_data_source');


var Bubble = module.exports = function(name) {
  if (!name || name.isEmpty()) {
  	throw new Error('Bubble\'s name is mandatory.');
  }
  this.name = name;
  this.slug = Utils.createSlug(name);
};

//Creates a new bubble with no content...
Bubble.prototype.create = function(params, callback) {
  if (typeof params === 'function') {
    throw new Error('Bubble creation parameters are missing.');
    return this;
  }

  if ( typeof callback !== 'function') {
     throw new Error('INVALID CALLBACK');
     return this;
  }

  //'Name' is required to pick the correct database object
  params.name = this.name;
  params.slug = this.slug;

  var self = this;
  var bubbleDatabaseDoc = new BubbleModel();
  bubbleDatabaseDoc.fill(params, function(error, data) {
    if (error) {
      callback(error);
      return self;
    }

    BubbleModel.create(data, function(error, data) {
      callback(error, data);
      return self;
    });
  });

  return this;
}

Bubble.prototype.setProperties = function(params, callback) {

  log.info("Bubble.setProperties: params: %s", JSON.stringify(params));

  if (typeof params === 'function') {
    throw new Error('Bubble selection parameters are missing.');
    return this;
  }

  if ( typeof callback !== 'function') {
     throw new Error('INVALID CALLBACK');
     return this;
  }

  var self = this;
  log.debug("Bubble.setProperties: Check the database to see if we have a bubble named [%s]", this.slug);
  BubbleModel.findOne({slug: this.slug}, function(error, bubble) {
    if (error) {
      callback(error);
      return self;
    }
    
    if (!bubble) {
      callback(new Error("Bubble: " + this.slug + " not found in the database"));
      return self;
    }

    log.debug("Bubble.setProperties: Found a bubble named [%s] in the database.", self.name);
    var modifiableAttr = ["description", "image"]; 
    var i;
    for (i=0; i < modifiableAttr.length; i++) {
      var attr = modifiableAttr[i];
      log.debug("Bubble.setProperties: Old value of property [%s] is [%s]", attr, (bubble[attr] || "null"));
      log.debug("Bubble.setProperties: Setting the property [%s] to value [%s]", attr, (params[attr] || "null"));
      if (params[attr])
        bubble[attr] = params[attr];
    }

    if (params.category && !params.category.isEmpty()) {

      BubbleCategory.findOne({slug: Utils.createSlug(params.category)}, function(error, category) {
        if (error) callback(error);
        log.debug("Bubble.setProperties: Saving the bubble after changing its meta data");
        bubble.category = category;
        bubble.save(function(error, dbRecord) {
          if (error) {
            log.debug("Bubble.setProperties: Could not save the bubble. Error: %s", error.err);
            callback(error);
          }
          else {
            log.debug("Bubble.setProperties: Saved Bubble - %s", JSON.stringify(dbRecord));
            callback(null, dbRecord);
          }
          return self;
        });
      });

    } else { //No explicit category assigned for this bubble

      bubble.category = bubble.category || null;
      log.debug("Bubble.setProperties: No Category set. Saving the bubble after changing its meta data");
      bubble.save(function(error, dbRecord) {
        if (error) {
          log.debug("Bubble.setProperties: Could not save the bubble. Error: %s", error.err);
          callback(error);
        } else {
          log.debug("Bubble.setProperties: Saved Bubble - %s", JSON.stringify(dbRecord));
          callback(null, dbRecord);
        }
        return self;
      });
    }

    return self;
  });

  return this;
}

Bubble.prototype.update = function(data, callback) {

  if (typeof data === 'function' || (data == null)) {
    throw new Error("Need data before updating the bubble's meta data.");
    return this;
  }

  log.info("Bubble.update: data: %s", JSON.stringify(data));

  if ( typeof callback !== 'function') {
     throw new Error('INVALID CALLBACK');
     return this;
  }

  if ( data.slug == null) {
    throw new Error("Bubble KEY is missing!");
    return this;
  }

  var self = this;
  BubbleModel.findOne({slug: data.slug}, {}, function(error, dbRec) {
    if (error) {
      log.error("Bubble.update: Could not find the bubble [%s]", data.slug);
      log.error(error);
      callback(error);
      return self;
    }

    dbRec.description = data.description || dbRec.description;
    dbRec.image = data.image || dbRec.image;
    dbRec.save(callback);    
    return self;
  });

}

Bubble.prototype.remove = function(callback) {
  if ( this.slug == null) {
    throw new Error("Bubble KEY is missing!");
    return this;
  }

  log.info("Bubble.remove: Deleting the bubble - %s", JSON.stringify(this.slug));

  if ( typeof callback !== 'function') {
     throw new Error('INVALID CALLBACK');
     return this;
  }

  var self = this;
  BubbleModel.remove({slug: this.slug}, callback);
  return self;
}

Bubble.prototype.addContentSource = function(src, callback) {

  log.info("Bubble.addContentSource: Bubble [%s] is getting a new source with config [%s]", this.name, JSON.stringify(src));

  if (typeof src === 'function') {
    throw new Error('Bubble content source object are missing.');
    return this;
  }

  if ( typeof callback !== 'function') {
     throw new Error('INVALID CALLBACK');
     return this;
  }

  //Add a twitter account or search as content source to this bubble
  var self = this;
  log.debug("Bubble.addContentSource: Check the database to see if we have a bubble named [%s]", this.slug);
  BubbleModel.findOne({slug: this.slug}, function(error, bubble) {
    if (error || !bubble) {
      callback(error);
      return self;
    }
    log.debug("Bubble.addContentSource: Found a bubble named [%s] in the database. Adding source [%s]", 
              self.slug, JSON.stringify(src));
    //Check for duplicates, before adding the new source
    var bFound = -1;
    for (var i=0; (i < bubble.sources.length) && (bFound < 0); i++) {
      switch (bubble.sources[i].name) {
        case "twitter":
          if ((bubble.sources[i].handle === src.handle) ||
              (bubble.sources[i].search === src.search)) {
             bFound = i;
          }
          break;
      }
    } //for() - Search for duplicates

    if (bFound >= 0) {
      //If its already there, then dont bother adding the source...
      log.error("Bubble.addContentSource: For Bubble [%s], Unable to add data source - %s, as it already exists!", 
                self.slug, JSON.stringify(src));
      callback(null, bubble);
      return self;
    }

    bubble.sources.push(JSON.parse(JSON.stringify(src)));
    bubble.save(function(error, data) {
      log.debug("Bubble.addContentSource: Added the new source to the bubble");
      callback(error, data);
      return self;
    });
  });

  return this;

};

Bubble.prototype.refreshContent = function(callback) {

  log.info("Bubble.refreshContents: Bubble [%s] ", this.name);

  if ( typeof callback !== 'function') {
     throw new Error('INVALID CALLBACK');
     return this;
  }

  var self = this;
  log.debug("Bubble.refreshContents: Check the database to see if we have a bubble named [%s]", this.slug);
  BubbleModel.findOne({slug: this.slug}, function(error, bubble) {
  	if (error || !bubble || (bubble.sources.length == 0)) {
      log.error("Bubble.refreshContents: Couldnt locate the bubble [%s] in the database!", this.slug);
      callback(null, bubble);
      return this;
    }
  
    log.debug("Bubble.refreshContents: Found the bubble [%s] in the database.", self.slug);
    async.concat(bubble.sources, fetchContent, function(error, data) {
      if (error) { 
        log.error("Bubble.refreshContents: fetchContent() failed. Error: %s", error.err);
      	callback(error);
      }
      else {

      	if (data && data.length && (data.length != 0) ) {
      	  // Data source parameters might have been updated. Save them
      	  // so that the next call to getContent() gets the incremental data only.
      	  //
          log.debug("Bubble.refreshContents: Refreshed the bubble with sources [%s]", JSON.stringify(data));

          bubble.sources = data;
          bubble.save(callback(null, data));
      	} else {
      	  callback(null, data);
      	}
      }
    });

    function fetchContent(srcCfg, cb) {
      var dataSource = new BubbleDataSource(srcCfg);
 
      log.debug("Bubble.fetchContent: For Bubble [%s], about to fetch content using source: [%s]", 
                self.slug, JSON.stringify(dataSource));

      dataSource.getContent(function(error, contents) {
        if (error) {
          cb(error);
          return;
        }

        //Update the bubble with the newly fetched contents
        //and return the updated the data source (with its
        //meta data updated - Max_Tweetid, Max_post_id fetched etc.)

        if (contents)
          log.debug("Bubble.fetchContent: Got %d content records", contents.length);

        contents.forEach(function(c) {
          switch (c.type) {
            case "photo":
              bubble.counters.photos++;
              break;
            case "video":
              bubble.counters.videos++;
            case "tweet":
              bubble.counters.tweets++;
              break;
            case "article":
              bubble.counters.articles++;
              break;
          }
        })

        bubble.contents = bubble.contents.concat(contents);
        bubble.save(function(error, data) {
          if (error) { 
            log.error("Bubble.fetchContent: Failed. Error: [%s]", JSON.stringify(error));
            cb(error);
          } else {
            log.debug("Bubble.fetchContent: Refreshed the bubble contents for the bubble [%s]", self.slug);
            cb(null, dataSource.source);
          }
        });
      });
    }

    return self;
  });

  return this;
}

//Return the meta data of the bubble
Bubble.prototype.getDetails = function(params, callback) {
  if (typeof params === 'function') {
    callback = params;
    params = {};
  }

  log.info("Bubble.getDetails: Getting the meta data for the bubble [%s]. Params: [%s]", this.name, JSON.stringify(params));
  var self = this;
  if (!params.slug) params.slug = Utils.createSlug(this.name);

  BubbleModel.findOne(params, {contents: 0}, function(error, data) {
    if (error) {
      log.error('Bubble.getDetails: Could not find the bubble [%s] in the database');
      return callback(error);
    }
  
    log.info("Bubble.getDetails: Meta data for the bubble [%s] = [%s].", params.slug, JSON.stringify(data));

    callback(null, data);
    return self;
  });

}

Bubble.prototype.getItems = function(params, callback) {
  if (typeof params === 'function') {
    callback = params;
    params = {};
  }

  //Default settings
  params.page  = params.page || '0';
  params.page = Number(params.page);
  params.count = params.count || '6';
  params.count = Number(params.count);

  log.debug("Bubble:getItems(%s):  Params: %s", this.name, JSON.stringify(params));

  var aggregatePipeline = [];
  aggregatePipeline.push({$match: {slug: this.slug}}); //Make sure to return contents for the right bubble
  aggregatePipeline.push({$unwind: "$contents"});

  if (params.type && !params.type.isEmpty()) {
    aggregatePipeline.push({$match: {"contents.type": params.type}});
  } else {
    aggregatePipeline.push({$match: {"contents.type": {$in: ['photo', 'video', 'article', 'tweet']}}});
  }
  
  if (params.item_id && !params.item_id.isEmpty()) aggregatePipeline.push({$match: {"contents.slug": params.item_id}});
 
  aggregatePipeline.push({$project: {_id: 0, slug: "$contents.slug", description: "$contents.description", type: "$contents.type", title: "$contents.title",
                         resourceLink: "$contents.resourceLink", imageLink: "$contents.imageLink", stats: "$contents.stats", created_at: "$contents.created_at"}});

  aggregatePipeline.push({$sort: {/*"contents.socialScore": -1, */"created_at": -1}});

  aggregatePipeline.push({$skip: (params.page * params.count)});
  aggregatePipeline.push({$limit: params.count});
  var self = this;
  BubbleModel.aggregate( aggregatePipeline, function(error, contents) {
    if (error) {
      callback(error);
      return self;
    }

    var results = {};
    if (contents && (contents.length > 0) && !params.item_id) {
      results.page = Number(params.page) + 1;
      results.count = contents.length;
    }
    results.data = contents;
    callback(null, results);
    return self;
  });

  return this;
}


Bubble.prototype.deleteItems = function(list, callback) {

  log.info("Bubble: DeleteItems(%s) entered", this.slug);

  if (typeof list === 'function') {
    callback = list;
    list = [];
  }

  if (!list.isArray) {
    log.error('Bubble: DeleteItems(%s) - The first argument passed is not a proper array. Please pass the array of item keys.', this.slug);
    callback(new Error('Please pass an array with keys of the items to be deleted'));
    return this;    
  }

  if (list.length === 0) {
    log.info('Bubble: DeleteItems(%s) - Nothing to delete.', this.slug);    
    callback(null, null);
    return this;
  }

  log.debug("Bubble: DeleteItems(%s) - deleting records: %s", this.slug, JSON.stringify(list));

  var self = this;
  BubbleModel.update({slug: self.slug}, {$pull: {contents: {slug: {$in: list}}}},
    function(error, val) {
      if (error) {
        log.error('Bubble: DeleteItems(%s) - Error: %s', self.slug, (error.err || error.message));
        callback(error);
        return self;
      }

      //Update the 'type' counters for this bubble to reflect the deletions...
      BubbleModel.aggregate([{$match: {slug: self.slug}},
                             {$unwind: "$contents"},
                             {$match: {"contents.type": {$in: ['photo', 'video', 'article', 'tweet']}}},
                             {$project: {"contents.type": 1, _id: 0}},
                             {$group: {_id: "$contents.type", count: {$sum: 1}}}
                            ], function(error, data) {
        if (error) {
          log.error('Bubble: DeleteItems(%s) - Error while counting the item types: %s', self.slug, (error.err || error.message));
          callback(error);
          return self;
        }
        
        var counters = {photos: 0, videos: 0, articles: 0, tweets: 0};
        log.debug('Bubble: DeleteItems(%s) - Aggregation results', self.slug, JSON.stringify(data));

        data.forEach(function(d) {
          counters[d._id + "s"] = Number(d.count);
        });

        log.debug('Bubble: DeleteItems(%s) - Count of item types after deletion: %s', self.slug, JSON.stringify(counters));
        BubbleModel.update({slug: self.slug}, {$set: {counters: counters, updated_at: Date.now()}},  
          function(error, val) {
            if (error) {
              log.error('Bubble: DeleteItems(%s) - Error while updating the counters: %s', self.slug, (error.err || error.message));
              callback(error);
              return self;
            }

            if (!val) {
              log.debug('Bubble: DeleteItems(%s) - Failed to update the counters.', self.slug);              
            }

            callback(null, null);
            log.info("Bubble: DeleteItems(%s) Done! Deleted %d records.", self.slug, list.length);
            return self;
        });

      }); //aggregate() - Update the counters...       
    }); //update()
}

Bubble.prototype.updateItems = function(list, callback) {

  log.info("Bubble: UpdateItems(%s) entered", this.slug);

  if (typeof list === 'function') {
    callback = list;
    list = [];
  }
  
  log.debug('Bubble: UpdateItems(%s). List = %s', this.slug, JSON.stringify(list));

  if (!list.isArray) {
    log.error('Bubble: UpdateItems(%s) - The first argument passed is not a proper array. Please pass the array of items.', this.slug);
    callback(new Error('Please pass an array with keys of the items.'));
    return this;    
  }

  if (list.length === 0) {
    log.info('Bubble: UpdateItems(%s) - Nothing to update.', this.slug);    
    callback(null, null);
    return this;
  }

  var counter = 0;
  var self = this;

  var q = async.queue(function(item, cb) {
    log.silly("Bubble: UpdateItems([%s]: [%s]). About to update the item.", self.slug, item.slug);

    BubbleModel.findOne({slug: self.slug}).update({contents: {$elemMatch: {slug: item.slug}}}, 
                                                  {"contents.$.type": item.type,
                                                   "contents.$.description": item.description,
                                                   "contents.$.stats": item.stats, 
                                                   "contents.$.updated_at": Date.now()}).
      exec(function(error, val) {
        if (error) {
          log.error('Bubble: UpdateItems(%s:%s) - Error: %s', self.slug, item.slug, (error.err || error.message));
          cb(error);
          return self;
        }
                         
       counter += val;
       cb(null, null);
       log.silly("Bubble: UpdateItems(%s:%s) Done", self.slug, item.slug);
       return self;
    }); //update()
  }, 100); //async.queue()

  q.drain = function() {
    log.info('Bubble: UpdateItems(%s) - Updated %d items', self.slug, counter);
    callback(null, null);
    return self;
  }

  list.forEach(function(item) {
    q.push(item, function(error, val) {
      if (error) {
         //
         // Cant rollback any changes done to previous items. 
         // Should we abandon the updates mid-way if there is an error while
         // updating one of the records?
         
         //callback(error);
         //return self;
      }
    })
  })
}

Bubble.prototype.addItems = function(list, callback) {
  log.info("Bubble: addItems(%s) entered", this.slug);

  if (typeof list === 'function') {
    callback = list;
    list = [];
  }
  
  log.debug('Bubble: AddItems(%s). List = %s', this.slug, JSON.stringify(list));

  if (!list.isArray) {
    log.error('Bubble: AddItems(%s) - The first argument passed is not a proper array. Please pass the array of items.', this.slug);
    callback(new Error('Please pass an array of items to be added.'));
    return this;    
  }

  if (list.length === 0) {
    log.info('Bubble: AddItems(%s) - Nothing to update.', this.slug);    
    callback(null, null);
    return this;
  }


  var itemArray = [];
  var externalResources = [];
  var counters = {photos: 0, videos: 0, tweets: 0, articles: 0};
  var hyperLinkPrefix = Utils.linkToUploadedFiles() + this.slug + "/";
  var self = this;

  list.forEach(function(item) {
    var content = new BubbleContentModel();

    if (!item.resourceLink) {
      return; //Skip over records which do not link to a file or URI
    }
    
    content.type = item.type;
    content.slug = (new Date()).getTime() + '';
    if (item.resourceLink && ( !item.resourceLink.trim().match(/^http/i) )) {
      content.resourceLink = encodeURI(hyperLinkPrefix + item.resourceLink);
    } else {
      content.resourceLink = item.resourceLink;
    }
    content.description = item.description;
    content.title = item.title;

    //If a resource link was given, then use it to do the content classification (photo/video/article...) 
    if ((content.ClassifyResourceLink() == false) && content.resourceLink &&
        (content.resourceLink.trim().toLowerCase().match(/^http/i))) {
      externalResources.push(content);
    } else {
      itemArray.push(content);
    }

    switch (item.type) {
      case "photo":
        content.imageLink.small = content.resourceLink;
        content.imageLink.medium = content.imageLink.large = content.imageLink.small;
        counters.photos++;
        break;
      case "video":
        var screenCapFile = item.resourceLink.replace(/\..+$/, '') + "_1.png";
        var screenCapFilePath = Utils.fileUploadDirectory() + self.slug + "/" + screenCapFile; 
        content.imageLink.small = fs.existsSync(screenCapFilePath) ? hyperLinkPrefix + screenCapFile : null;
        content.imageLink.medium = content.imageLink.large = content.imageLink.small;
        counters.videos++;
      case "tweet":
        counters.tweets++;
        break;
      case "article":
        counters.articles++;
        break;
    }
  }); //forEach()
  
  if (externalResources.length > 0) {

    var urls = externalResources.map(function(r) { return r.resourceLink});
    log.debug("Bubble.AddItems(%s): Resolving URLs - %s", self.slug, JSON.stringify(urls));

    Utils.resolveURLsWithExtService(urls, function(error, origURLs, expandedURLs) {

      if (error) {
        log.error("Could not get information regarding one or more of these external links - %s. Error - %s", 
                  (urls? JSON.stringify(urls) : "[]"), (error.err || error.message));

        //For now, lets just shove the links into the database even if there is a failure to get more information
        //about them
        itemArray = itemArray.concat(externalResources);
        UpdateBubble(self, itemArray, counters, callback);
        return self;
      }

      log.debug("Bubble.AddItems(%s): Orig Urls - %s Expanded Urls - %s", self.slug, 
        JSON.stringify(origURLs), JSON.stringify(expandedURLs));

      //Now that the URLs have been resolved/decoded, update the content record with the
      //additonal details obtained
      externalResources = externalResources.map(function(r) {
        for (var i=0; i < origURLs.length; i++) {
          
          if (origURLs[i].localeCompare(r.resourceLink) !== 0) {
            continue; //This is not the content record we are looking for...
          }

          if (expandedURLs[i].type == 'error') {
            continue; 
          }
          
          r.embedlyData = expandedURLs[i];
          r.title = expandedURLs[i].title;
          if (!r.description || !r.description.trim()) r.description = expandedURLs[i].description;
          r.imageLink.small = expandedURLs[i].thumbnail_url;
          r.imageLink.medium = r.imageLink.large = r.imageLink.small;

          //Check to ensure that while resolving the embedded url, the type hasnt changed.
          //It is possible that the link was a video/photo link...
          counters[r.type + "s"]--;
          if (expandedURLs[i].type === 'link') {
            r.type = 'article';
          } else if (expandedURLs[i].embedlyData.type === 'rich') {
            r.type = 'photo';
          }

          counters[r.type +"s"]++;

        } //for()
        return r;
      });

      log.debug("Bubble.AddItems(%s): Items with embedded links after resolution - %s", self.slug, JSON.stringify(externalResources));
      itemArray = itemArray.concat(externalResources);
      UpdateBubble(self, itemArray, counters, callback);
      return self;
    });

  } else {
    UpdateBubble(self, itemArray, counters, callback);
    return self;
  }

  function UpdateBubble(self, itemArray, counters, callback) {
    BubbleModel.findOne({slug:self.slug}, function(error, bubble) {
      if (error) {
        log.error("Bubble.AddItems(%s) - Could not find the bubble! Error - ", self.slug, (error.err || error.message));
        callback(error);
        return self;
      }

      log.silly("Bubble.AddItems(%s):UpdateBubble. counters= %s, Items = %s", JSON.stringify(counters), JSON.stringify(itemArray));

      bubble.contents = bubble.contents.concat(itemArray);
      bubble.counters.photos += counters.photos;
      bubble.counters.videos += counters.videos;
      bubble.counters.tweets += counters.tweets;
      bubble.counters.articles += counters.articles;

      bubble.save(function(error, data) {
        if (error) {
          log.error("Bubble.AddItems(%s) failed. Error - ", self.slug, (error.err || error.message));
          callback(error);
          return self;
        }

        log.info("Bubble.AddItems(%s) done. Saved %d records.", self.slug, itemArray.length);
        callback(null, self.slug);
        return self;
      });
    }); //Bubble.findOne()
  }
}
