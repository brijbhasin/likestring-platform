var config   = require('config');
var async    = require('async');
var likeStringModel = require('../../app/models/likestring');
var Bubble   = require('../../app/models/bubble').Bubble;
var Utils    = require('../../app/models/utils');
var User     = require('../../app/models/user');
var log = require('../../app/log');

var LikeString = module.exports = function(user, name) {
  if (!user) throw new Error("likeStrings are user-specific, hence User is mandatory.");

  //Assume it is a valid user
  this.user = user;

  // Each instance of a likeString has a owner and a unique name (within that user's strings set)
  this.name = name;
};

//Default set of likeStrings handed to new user or anyone who doesnt have a likeString set.
LikeString.prototype.defaults = function(callback) {
  if (!config || !config.strings || !config.strings.length) {
  	//We dont seem to have any defaults defined, hence return a empty set.
  	callback(null, []);
    return this;
  }

  var likeStringArray = [];
  var self = this;
  async.map(config.strings, createString, function(error, likeStringArray) {
  	if (error) callback(error);
    callback(null, likeStringArray);
    return self;
  });

  function createString(likeStringConfig, callback) {
    var lstrModel = new likeStringModel();
    lstrModel.user = self.user;
    lstrModel.name = likeStringConfig.name || "Unknown";
    lstrModel.slug = Utils.createSlug(likeStringConfig.name);
    lstrModel.bubbles = [];

    async.map(likeStringConfig.bubbles, getBubble, function(error, bubbleRefs) {
      if (error) return callback(error);
      
      //Remove all nulls, because those bubbles dont exist in our database
      bubbleRefs = bubbleRefs.filter(function(b) { return b != null });

      lstrModel.bubbles = bubbleRefs;
      lstrModel.save(function(error, rec) {
        if (error) return callback(error);
        return callback(null, rec);
      });
    });
  }
  
  function getBubble(name, callback) {
    Bubble.findOne({slug: Utils.createSlug(name)}, {_id: 0, slug: 1}, function(error, rec) {
      if (error) return callback(error);
      return callback(null, (rec ? rec.slug : null));
    });
  }

}; //defaults

//Return the strings that belong to the user
LikeString.prototype.getUserLikeStrings = function(params, callback) {
  if (typeof params === 'function') {
    callback = params;
    params = {};
  }

  if (typeof callback != 'function') 
  	throw new Error("Callback is required");
  
  var self=this;
  if (!params.user) params.user = this.user;

  //First, find all the strings for the given user
  likeStringModel.find(params, function(error, likeStrs) {
    if (error) return callback(error);

    //Next, ensure that bubbles inside each string does really exist in the database
    //Delete the references for non-existent bubbles.
    async.map(likeStrs, checkBubbleRefs, function(error, strings) {
     callback(null, strings);
     return self;
    });

  }); //likeStrings.find()


  function checkBubbleRefs(likeStr, callback) {
    log.debug("likeString.getUserLikeStrings: For [%s], cross-checking if the referenced bubbles exist.", likeStr.name);

    async.map(likeStr.bubbles, getBubble, function(error, bubbleRefs) {
      if (error) return callback(error);
      //Remove all nulls, because those bubbles dont exist in our database
      bubbleRefs = bubbleRefs.filter(function(b) { return b != null });
      likeStr.bubbles = bubbleRefs;
      likeStr.save(function(error, rec) {
        if (error) return callback(error);
        callback(null, rec);
      });
    });
  }

  function getBubble(b, callback) {
    Bubble.findOne({slug: b}, function(error, rec) {
      if (error) return callback(error);
      log.debug("likeString.getUserLikeStrings: Found the bubble: [%s]", b.name);
      return callback(null, (rec ? rec.slug : null));
    });
  }

}

LikeString.prototype.create = function(params, callback) {
  if ((params == null) || (!params.name) || (params.name.isEmpty())) {
     throw new Error('Name is mandatory for adding a likeString');
  }
  if (typeof callback != 'function') 
  	throw new Error("Callback is required");

  var likeString = new likeStringModel();
  var self = this;
  likeString.user = this.user;
  likeString.slug = Utils.createSlug(params.name);
  likeString.name = params.name;
  likeString.bubbles = []; //New string will not have any bubbles...yet
  likeString.save(function(error, rec) {
    if (error) {
      log.error("likeString.create: Cannot insert a new string document. %s", error.err);
      return callback(error);
    }
    
    self.name = rec.name;
    callback(null, rec);
    return self;
  });
}

LikeString.prototype.remove = function(params, callback) {
  if (typeof params === 'function') {
    callback = params;
    params = {};
  }
  if (typeof callback != 'function') 
  	throw new Error("Callback is required");

  var name = (params.name || this.name);
  var key = Utils.createSlug(name);
  log.info('likeString.remove: Deleting the string: [%s], belonging to user: %s', key, JSON.stringify(this.user));
  if (!key || key.isEmpty()) {
     callback(null, 0);
     return this;
  }
  
  var self = this;
  likeStringModel.findOneAndRemove({user: this.user, slug: key}, {slug: 1, name: 1}, function(error, data) {
    if (error) {
      log.debug('likeString.remove: Cannot delete the string: [%s] in the database: %s', this.name, error.err);
      return callback(error);
    }

    if (!data || (data === 0)) {
      log.debug('likeString.remove: Cannot find the string: [%s] in the database!', name);
      return callback(new Error("[" + name + "] not found!"));
    }

    log.debug('likeString.remove: Deleted the string: [%s] in the database', name);
    callback(null, data);
    return self;
  });
}

LikeString.prototype.changeName = function(params, callback) {
  if (typeof params === 'function') {
  	callback = params;
  	params = {};
  }
  if (typeof callback != 'function') 
  	throw new Error("Callback is required");

  if (!params.name || params.name.isEmpty()) {
  	//There is nothing to change, just return
  	callback(null, 0);
    return this;
  }

  log.debug("likeString.changeName: About to change the name from [%s] to [%s].", this.name, params.name);
  var self = this;
  likeStringModel.findOneAndUpdate({user: this.user, slug: Utils.createSlug(this.name)}, 
  	                                  {name: params.name, slug: Utils.createSlug(params.name)}, 
  	                                  function(error, updatedDocument) {
    if (error) callback(error);
    self.name = params.name;
    callback(null, updatedDocument);
    log.debug("likeString.changeName. string: ", JSON.stringify(updatedDocument));
    return self;
  })
 };

 LikeString.prototype.addBubble = function(bubbleName, callback) {
  if ((typeof bubbleName === 'function') || (bubbleName == null))
    throw new Error('Bubble name is required');
  if (typeof callback != 'function') 
  	throw new Error("Callback is required");

  var self = this;
  likeStringModel.findOne({user: this.user, slug: Utils.createSlug(this.name)}, function(error, likeString) {
  	if (error) {
  	  callback(error);
  	  return self;
  	}
  	
  	if (!likeString) {
      log.debug('likeString.addBubble: String [%s] doesnt exist in the database!', this.name);      
  	  callback(new Error("Unknown likeString: [" + this.name + "]"));
      return self;
  	}

  	Bubble.findOne({slug: Utils.createSlug(bubbleName)}, function(error, bubble) {
      if (error) {
      	callback(error);
      	return self;
      }

      if (!bubble) {
        log.debug('likeString.addBubble: Bubble [%s] doesnt exist in the database!', bubbleName);      
        callback(new Error("Unknown Bubble: [" + bubbleName + "]"));
        return self;
      }

      log.debug('likeString.addBubble: Checking if the bubble already exists.');
      for(var i=0; i < likeString.bubbles.length; i++) {
        var b = (likeString.bubbles[i] ? likeString.bubbles[i].slug : "");
        if (b === bubble.slug) {
          log.error('likeString.addBubble: Bubble [%s] already on this string!', bubbleName);
          callback(new Error(bubbleName + " already on this string!"));
          return self;
        }
      }

      likeString.bubbles.push(bubble.slug);

      likeString.save(function(error) {
        log.debug('likeString.addBubble: Added Bubble [%s] to String [%s] belonging to the user - %s', 
                  bubbleName, self.name, JSON.stringify(self.user));      
        callback(error, bubble);
        return self;
      });
  	});

  });

 };

 LikeString.prototype.removeBubble = function(bubbleName, callback) {
  if ((typeof bubbleName === 'function') || (bubbleName == null))
    throw new Error('Bubble name is required');
  if (typeof callback != 'function') 
  	throw new Error("Callback is required");
   
  var self = this;   
  likeStringModel.findOne({user: this.user, slug: Utils.createSlug(this.name)}, function(error, likeString) {
    if (error) {
      callback(error);
      return self;
    }
    if (!likeString)
      throw new Error("Unknown likeString: " + this.name);

    Bubble.findOne({slug: Utils.createSlug(bubbleName)}, function(error, bubble) {
      if (error) {
      	callback(error);
      	return self;
      }

      if (!bubble) { //Non-existent bubble
         log.debug('likeString.removeBubble: Bubble[%s] doesnt exist in the database!', bubbleName);
         callback(null, likeString);
         return self;
      }

      var bubbleFound = false;
      for(var i=0; i < likeString.bubbles.length; i++) {
        if (likeString.bubbles[i] && (likeString.bubbles[i].slug === bubble.slug)) {
          bubbleFound = true;
          likeString.bubbles.splice(i, 1); //Delete the bubble Reference
          likeString.save(function(error, rec) {
            if (error) return callback(error);
            log.debug('likeString.removeBubble: Deleted the bubble[%s] in the string[%s] belonging to user - %s', 
                      bubbleName, self.name, JSON.stringify(self.user));
            callback(null, bubble);
            return self;
          });
        }
      }

      //In case the bubble is not on the given string, then just return without any error
      if (bubbleFound == false) {
        log.debug('likeString.removeBubble: Could not find the bubble[%s] in the string[%s] belonging to user - %s', 
                  bubbleName, self.name, JSON.stringify(self.user));
      	callback(null, null);
      	return self;
      } 
    });
  });
 };

LikeString.prototype.moveBubble = function(params, callback) {
  if ((typeof params === 'function') || (params == null))
    throw new Error('Missing parameters.');

  if (typeof callback != 'function') 
    throw new Error("Callback is required");

  log.info("likeString.moveBubble: Input parameters ", params);

  var self = this;   
  var srcLikeString = new LikeString(this.user, params.from_string_id);
  var dstLikeString = new LikeString(this.user, params.to_string_id);

  log.debug("likeString.moveBubble: Moving bubble [%s] from string [%s] to string [%s]",
            params.bubble_id, params.from_string_id, params.to_string_id);
  srcLikeString.removeBubble(params.bubble_id, function(error) {
    if (error) return callback(error);

    log.debug("likeString.moveBubble: Removed bubble [%s] from string [%s]",
              params.bubble_id, params.from_string_id);
    dstLikeString.addBubble(params.bubble_id, function(error, bubble) {
      if (error) return callback(error);
      log.debug("likeString.moveBubble: Added bubble [%s] to string [%s]",
              params.bubble_id, params.to_string_id);
      log.debug("likeString.moveBubble: Moved the bubble successfully!");
      callback(null, bubble);
      return self;
    });
  });
}
 
LikeString.prototype.toJSON = function(strings, callback) {
  if ((typeof strings === 'function') || !strings || !strings.length || (strings.length == 0)) {
     callback(null, null);
     return;
  }

  async.map(strings, likeStringToJSON, function(error, likeStrings) {
    if (error)
      callback(error);
    else 
      callback(null, likeStrings);
  });

  return this;

}

LikeString.prototype.searchBubbles = function(params, callback) {
  if (typeof params === 'function') {
    callback = params;
    params = {};
  }

  if (!params.query || params.query.isEmpty()) {
    params.query = '';
  }

  Bubble.find({name: {$regex : params.query, $options: 'i'}}, {_id: 0, contents: 0, __v: 0}, 
              callback);

  return this;
}

function likeStringToJSON(str, callback) {
  var jsonLikeStr = str.toJSON();

  //Remove fields not needed to be sent to the front-end (for now).
  delete jsonLikeStr._id;
  if (jsonLikeStr.bubbles) delete jsonLikeStr.bubbles;
  jsonLikeStr.bubbles = [];
  if (jsonLikeStr.user) delete jsonLikeStr.user;
  if (jsonLikeStr._v) delete(jsonLikeStr._v);

  if (!str.bubbles || !str.bubbles.length || (str.bubbles.length == 0))
     return callback(null, jsonLikeStr);

  async.map(str.bubbles, bubbleToJSON, function(error, jsonBubbles) {
    if (error) return callback(error);
    jsonBubbles = jsonBubbles.filter(function(n){ return n != undefined }); //Remove all the nulls
    jsonLikeStr.bubbles = jsonBubbles;
    callback(null, jsonLikeStr);
  });
};

function bubbleToJSON(bubble, callback) {
  Bubble.findOne({slug: bubble}, function(error, rec) {
    if (error) return callback(error);
    var jsonBubble = null;
    if (rec) {
      jsonBubble = rec.toJSON();
      delete jsonBubble._id;
      delete jsonBubble.contents; // No need to send out the entire contents since we only need the bubble's name, category and image.
    }
    return callback(null, jsonBubble);
  });
};
