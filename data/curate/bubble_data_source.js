//
// this is a sub-document which will always be part of the 'bubble' document in the database.
// Each bubble will have zero or more data sources like this which are utilized to flesh out 
// the bubble's contents and refresh it with latest data.
//
// This model is mainly for Twitter. It can either be created with a twitter handle (eg: @likestring) or
// it could be set with a search query (like a hashtag or any viable Twitter search).
// The sub-document will always remember the last curated tweet for a given handle or the last time 
// the search results were parsed and will fetch data beyond these and augment the bubble contents.
//
var request = require('request');
var URL = require('url');
var config   = require('config');
var vimeo = require('n-vimeo').video;
var async = require('async');
var cheerio = require('cheerio');

var log   = require('../../app/log');
var Utils = require('../../app/models/utils');
var BubbleSchemas = require('../../app/models/bubble'),
    BubbleModel = BubbleSchemas.Bubble,
    BubbleContentModel = BubbleSchemas.BubbleContent;
var TwitterUserProfileModel = require("../../app/models/twitter_user_profile");
var TwitterStore = require("../save/twitter");
var SavedTweets = require('../../app/models/twitter_tweet');


function BubbleDataSource(params) {
  this.name = "generic";

  this.getContent = function(callback) {
    return callback(new Error('getContent() is not implemented'));
  }

}

function TwitterBubbleDataSource(params) {
  BubbleDataSource.apply(this, arguments);

  this.name = "twitter";
  this.handle = params.handle || null;
  this.since_id = params.since_id || null; // since_id helps to get tweets beyond that id, when a twitter handle is given
  this.search = params.search || null;

  if (!this.handle && !this.search)
     throw new Error('Either a Twitter handle or a Search query is required');

  var self = this;
  this.getContent = function(callback) {
    if (typeof callback !== 'function') {
       throw new Error('callback is missing.');
    }

    if (self.handle && !self.handle.isEmpty()) {
      getContentFromTwitterHandle(self, callback);
      return self;
    }

    if (self.search && !self.search.isEmpty()) {
      // Last searched will provide a threshold for search related twitter queries.
      // It will only get search results beyond this date.
      if (params.last_searched)
        self.lastSearched = new Date.parse(params.last_searched);
      else
        self.lastSearched = null;

      getContentFromTwitterSearch(self, callback);
      return self;
    }

    callback(new Error('Either a Twitter handle or a Search query is required'));
    return self;
  } //getContent()
} //TwitterBubbleDataSource()

function getContentFromTwitterHandle(self, callback) {

  log.info("TwitterBubbleDataSource.getContentFromTwitterHandle: Handle [%s]", self.handle);

  TwitterUserProfileModel.findOne({"raw.screen_name": self.handle}, function(error, userProfile) {
    if (error) {
      log.error("TwitterBubbleDataSource.getContentFromTwitterHandle: Couldnt find the twitter profile with handle [%s]. Error: %s", 
                self.handle, error.err);
      callback(error);
      return self;
    }

    if (!userProfile) {
      log.error("TwitterBubbleDataSource.getContentFromTwitterHandle: Couldnt find the twitter profile with handle [%s].", self.handle);
      callback(null, []);
      return self;
    }

    log.debug("TwitterBubbleDataSource.getContentFromTwitterHandle: For handle [%s] found the twitter profile with ID [%s]", 
              self.handle, userProfile.id);

    var searchParams = {"user.id": userProfile.id};
    if (self.since_id) searchParams.id = {"$gt" : self.since_id};

    //Twitter Tweets are already collected and saved to our local database before this call.
    //Hence grab the tweets from the database.
    SavedTweets.find(searchParams, function(error, tweets) {
      if (error) {
        callback(error);
        return self;
      }

      if (!tweets || (tweets.length == 0)) {
        //No tweets found!
        log.debug("TwitterBubbleDataSource.getContentFromTwitterHandle: No tweets found for the user: %s", userProfile.id);
        callback(null, []);
        return self;
      }
        
      log.debug("TwitterBubbleDataSource.getContentFromTwitterHandle: %d tweets found for the user: %s", tweets.length, userProfile.id);

      parseTweets(self, tweets, "handle", callback);
      return self;

    });

  }); //Find the TwitterProfile...
} //getContentFromTwitterHandle()

function getContentFromTwitterSearch(self, callback) {

  log.info("TwitterBubbleDataSource.getContentFromTwitterSearch: Query [%s]", self.search);

  if (!self.search || self.search.isEmpty()) {
    log.error("TwitterBubbleDataSource.getContentFromTwitterSearch: Search query is empty!"); 
    callback(new Error("Search query is empty!"));
    return self;
  }

  //Twitter Tweets are already collected and saved to our local database before this call.
  //Hence grab the tweets from the database.
  //FIXME: Need to handle the Twitter Search operators. For now, we just look at hashtags or subtext.
  var searchText = self.search;
  searchText = searchText.replace(/-RT/i, ''); //discard the "suppress retweet" expression.
  searchText = searchText.replace(/\s+filter\:.*\s*/gi, ''); //discard search filters

  var searchParams = {text: {$regex: searchText, $options: 'i'}};
  if (self.lastSearched) searchParams.created_at = {"$gt" : self.lastSearched.toISOString()};

  log.debug("TwitterBubbleDataSource.getContentFromTwitterSearch: Database Search parameters [%s]", JSON.stringify(searchParams));

  SavedTweets.find(searchParams, function(error, tweets) {
    if (error) {
      callback(error);
      return self;
    }

    if (!tweets || (tweets.length == 0)) {
      //No tweets found!
      log.debug("TwitterBubbleDataSource.getContentFromTwitterSearch: No tweets found");
      callback(null, []);
      return self;
    }
    
    log.debug("TwitterBubbleDataSource.getContentFromTwitterSearch: %d tweets found.", tweets.length);
    
    parseTweets(self, tweets, "search", callback);
    return self;
  }); //tweets.find()
} //getContentFromTwitterSearch()

var tweetURL = "https://twitter.com/__TWITTER_ID__/status/__TWEET_ID__";

function transformTweetToContentRecord(tweet) {
  var content = new BubbleContentModel();
  content.type = 'tweet';
  content.slug = tweet.id_str;
  content.rawObjectId = tweet._id; //This is the reference to the Raw tweets that we have saved, for future reference
  content.created_at = new Date(Date.parse(tweet["created_at"].replace(/( \+)/, ' UTC$1')));
  content.stats.favoriteCount = tweet.favorite_count;
  content.stats.likeCount = tweet.retweet_count;
  content.stats.socialScore = Math.round((content.stats.favoriteCount + content.stats.likeCount +
                                          content.stats.viewCount + content.stats.commentCount)/4);

  //All our content records, should have some background image. So for now, adding the profile image for tweets if the
  //tweet itself doesnt have a photo/video
  content.imageLink.small = tweet.user.profile_image_url_https;
  content.imageLink.medium = content.imageLink.large = content.imageLink.small;

  //Is this a Re-tweet?
  var tweetRecord;
  if (tweet.retweeted_status) {
    tweetRecord = tweet.retweeted_status;
  } else {
    tweetRecord = tweet;
  }


  content.resourceLink = tweetURL.replace('__TWITTER_ID__', tweetRecord.user.screen_name).
                                       replace('__TWEET_ID__', tweetRecord.id_str);
  content.description = tweetRecord.text;
  content.title = tweetRecord.user.name + " on Twitter: " + content.description;

  return content;
} //transformTweetToContentRecord()

function transformTweetToPhotoRecord(tweet) {
  var photoRecords = [];

  log.debug("TwitterBubbleDataSource.transformTweetToPhotoRecord. Parsing Tweet with ID: %s", tweet.id_str);

  tweet.entities.media.forEach(function(mediaRecord) {
    if (!mediaRecord.media_url_https || (mediaRecord.media_url_https.isEmpty())) {
      log.silly("TwitterBubbleDataSource.transformTweetToPhotoRecord. No Photos in Tweet with ID: %s", tweet.id_str);
      return; //skip blank records
    }

    if (mediaRecord.type !== 'photo') {
      log.silly("TwitterBubbleDataSource.transformTweetToPhotoRecord. No Photos in Tweet with ID: %s", tweet.id_str);
      return;
    }
    
    var record = transformTweetToContentRecord(tweet);      
    record.type = 'photo';
    record.resourceLink = mediaRecord.media_url_https;

    if (mediaRecord.sizes.thumb)
      record.imageLink.small = mediaRecord.media_url_https + ":thumb";

    if (mediaRecord.sizes.small)
      record.imageLink.medium = mediaRecord.media_url_https + ":small";

    if (mediaRecord.sizes.large)
      record.imageLink.large = mediaRecord.media_url_https + ":large";

    //make sure there is some link for each size
    record.imageLink.small = record.imageLink.small   || mediaRecord.media_url_https;
    record.imageLink.medium = record.imageLink.medium || mediaRecord.media_url_https;
    record.imageLink.large = record.imageLink.large   || mediaRecord.media_url_https;

    log.silly("TwitterBubbleDataSource.transformTweetToPhotoRecord. Parsed Tweet with ID: %s. Record Type: %s", tweet.id_str, record.type);
    photoRecords.push(record); //Each photo is saved a separate bubble content record.
  }); //foreach entities.media record...

  return photoRecords;
} //transformTweetToPhotoRecord()


function transformTweetEntitiesToContentRecords(tweet) {
  // Check for videos or articles in embedded links within the tweet
  var returnRecords = [];
  var videoRecords = [];

  log.silly("TwitterBubbleDataSource.transformTweetEntitiesToContentRecords. Processing Tweet with ID: %s", tweet.id_str);
  
  tweet.entities.urls.forEach(function(url) {
    if (!url)
      return;
    var record = transformTweetToContentRecord(tweet);
    record.resourceLink = url.expanded_url;
    var urlComponents = null;
  

    var embedlyDataTypes = ['photo', 'video', 'link', 'rich'];

    log.silly("TwitterBubbleDataSource.transformTweetEntitiesToContentRecords. Tweet : %s", tweet.id_str);  

    if (url.embedlyData && (embedlyDataTypes.indexOf(url.embedlyData.type) !== -1)) { 
      if (url.embedlyData.type === 'link') {
        record.type = 'article';
      } else if (url.embedlyData.type === 'rich') {
        record.type = 'photo';
      } else {
        record.type = url.embedlyData.type;
      }

      record.resourceLink = (url.embedlyData.url || url.expanded_url); //Lets default to link in the tweet...
      record.title = url.embedlyData.title;
      record.description = url.embedlyData.description || "";
      record.imageLink.small = url.embedlyData.thumbnail_url || null;
      record.imageLink.medium = record.imageLink.large = record.imageLink.small;
      record.extended_data = url.embedlyData;

      log.silly("TwitterBubbleDataSource.transformTweetEntitiesToContentRecords. Tweet with ID: %s Processed by Embedly. Link: %s", 
                tweet.id_str, record.resourceLink);

      returnRecords.push(record);
      return;
    }

    if (!record.ClassifyResourceLink()) {
      if (record.resourceLink.match(/vimeo\.com/i)) {
         log.silly("TwitterBubbleDataSource.transformTweetEntitiesToContentRecords. Tweet with ID: %s Vimeo Link: %s", 
                tweet.id_str, record.resourceLink);
         
         videoRecords.push(function(thumbnailCB) {
           record.createVimeoThumbs(function(err, data) {
             if (err) return thumbnailCB(err);
             log.silly("createThumbnails. Vimeo Record: %s", JSON.stringify(data));
             thumbnailCB(null, data);
           });
         });
         return;

      } else if (record.resourceLink.match(/vine.co\/v\/(\w+)/i)) {
        log.silly("TwitterBubbleDataSource.transformTweetEntitiesToContentRecords. Tweet with ID: %s Vine Link: %s", 
                tweet.id_str, record.resourceLink);

         videoRecords.push(function(thumbnailCB) {
           record.createVineThumbs(function(err, data) {
             if (err) return thumbnailCB(err);
             log.silly("createThumbnails. Vine Record: %s", JSON.stringify(data));
             thumbnailCB(null, data);
           });
         });
        return;
      }
    }
   
    returnRecords.push(record);
  }); //Foreach entity URL in the given tweet...

  return {videos: videoRecords, records: returnRecords};
} //transformTweetEntitiesToContentRecords()

function parseTweets(self, tweets, tweetSource, callback) {
  var since_id = "";
  var contentArray = [];
  var postProcessRecords = [];

var BreakException= {};

  tweets.forEach(function(t) {
    var tweet = t.toJSON();      
    log.debug("TwitterBubbleDataSource.parseTweets. Parsing Tweet with ID: %s", tweet.id_str);

    if (tweet.entities && tweet.entities.media && (tweet.entities.media.length > 0)) {
      //Is there a photo in the tweet? If so, then peel it off and store it separately!
      var photoRecords = transformTweetToPhotoRecord(tweet);
      if (photoRecords.length > 0) {
        contentArray = contentArray.concat(photoRecords);
      } else {
        contentArray.push(transformTweetToContentRecord(tweet));
      }
      return;
    } //Is this a photo?
    else if (tweet.entities && tweet.entities.urls && (tweet.entities.urls.length > 0)) {
      var result = transformTweetEntitiesToContentRecords(tweet);
      if (result.records.length > 0) {
        contentArray = contentArray.concat(result.records);
      } 

      if (result.videos.length > 0) {
        postProcessRecords = postProcessRecords.concat(result.videos);
      }
      return;
    } //Is this a video?
    else { 
      //Save the simple tweet
      var content = transformTweetToContentRecord(tweet);
      log.debug("TwitterBubbleDataSource.parseTweets. Saving Tweet with ID: %s. Record Type: %s", tweet.id_str, content.type);
      contentArray.push(content);
    }
    
    //Save the highest tweet so that it marks the point from which point onwards, we will extract on the user timeline
    if (since_id.localeCompare(tweet.id_str) < 0) {
      since_id = tweet.id_str;
    }

  }); //foreach Tweet()

  if (tweetSource === "handle") {
    self.since_id = since_id;
  }

  log.debug("TwitterBubbleDataSource.parseTweets: %d tweets parsed!", contentArray.length);
  
  var blanks = 0;
  for (var i=0; i < contentArray.length; i++) {
    if (!contentArray[i]) blanks++;
  }

  if (postProcessRecords.length > 0) {
    log.info("TwitterBubbleDataSource.parseTweets. Processing %d Video links to get their thumbnails", postProcessRecords.length);    

    async.parallel(postProcessRecords, function(err, result) {
      if (err) {
        log.error("TwitterBubbleDataSource.parseTweets caused an error - %s", (err.err || err.message));
      } else {
        log.debug("TwitterBubbleDataSource.parseTweets. Done with post processing and got %d records", 
                 ((result && result.length) ? result.length : 0));
        contentArray = contentArray.concat(result);
      }
    
      return callback(null, contentArray);
    });
  } else {
    log.debug("TwitterBubbleDataSource.parseTweets: there are %d blanks", blanks);
    return callback(null, contentArray);
  }
}


//----------------------------------------------- EXPORTS ---------------------------------
var DataSource = module.exports = function(params) {

  if (!params) {
    params = {};
  }

  if (!params.name) {
    params.name = "generic";
  }

  switch(params.name) {
    case "twitter":
      this.source = new TwitterBubbleDataSource(params);
      break;
    default: //local
      this.source = new BubbleDataSource(params);
      break;
  }
};

DataSource.prototype.getContent = function(callback) {
  if (!this.source) {
    return callback(new Error('Unknown source. Cant get content.'));
  }
  
  return this.source.getContent(callback);
}
