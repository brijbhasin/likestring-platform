var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var twitterTweetSchema = Schema({ tweet : Schema.Types.Mixed }, { collection: 'twitter_tweets'});

// create the model  and expose it to our app
module.exports = mongoose.model('TwitterTweet', twitterTweetSchema);

