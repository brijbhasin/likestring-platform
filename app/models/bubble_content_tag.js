// 
// likeString Photos/Videos/text and other content could be tagged to help in users finding the right content.
// For example, photos could be tagged with people, place or even events.
//
var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var bubbleContentTagSchema = Schema({ 
      slug: String,
      name: String,
});

module.exports = mongoose.model('BubbleContentTag', bubbleContentTagSchema);