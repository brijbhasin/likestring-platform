//
// likeString Bubbles could be organized into a hierarchial category structure so that related bubbles could be found easily.
// For example: GSF could be of category "Company/VC_firm".
// Many bubbles could belong to one category. 
//
var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var bubbleCategorySchema = Schema({ 
      slug: String,
      name: String,
      parent: {type: Schema.Types.ObjectId, ref: "BubbleCategory"}
});

module.exports = mongoose.model('BubbleCategory', bubbleCategorySchema);