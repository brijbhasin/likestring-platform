var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var contentSourceSchema = Schema({ 
	source : {
	  twitter: {
	  	handle: String,
	  	search: String,
	  	since_id: String,
	  	updated: Date
	  }
	}
  }, { collection: 'content_sources'});

// create the model  and expose it to our app
module.exports = mongoose.model('ContentSource', contentSourceSchema);

