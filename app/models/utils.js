// Utility functions
var http = require("http");
var urlObj = require("url");
var embedly = require('embedly');
var async = require('async');
var ffmpeg = require('fluent-ffmpeg');

var log = require('../log');

module.exports.createSlug = function(text) {
  
  if (!text) return null;

  text = text.replace(/[^a-zA-Z0-9-_\s]/g,"");
  text = text.toLowerCase();
  text = text.replace(/\s/g,'-');    

  return text;
}

String.prototype.isEmpty = function() {
    return (this.length === 0 || !this.trim());
};

//Merge two JSONs and return the merged 'target'
// extend({}, Json1, Json2)
module.exports.extend = function(target) {
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
        for (var prop in source) {
        	if (source[prop]) //Ignore NULL values
              target[prop] = source[prop];
        }
    });
    return target;
}

module.exports.IGNORE_URL_LIST = [
  'yfrog\.com',
  'tmi\.me',
  'lockerz\.com',
  'rock\.li',
  'instagr\.am',
  'youtu\.be'    
];


module.exports.expandURL = function (url,callback) {

  var parsedURL = urlObj.parse(url);
  var options = {timeout: 1000,
                 host: parsedURL.hostname, 
                 path: ((parsedURL.pathname) ? (parsedURL.pathname) : "") + ((parsedURL.search) ? parsedURL.search : ""),
                 method:"HEAD"};
  var req = http.request(options, function(res) {
    //res.on('end', function (chunk) {
      var result = (res.headers && res.headers.location && (res.headers.location.indexOf("http") > -1)) ? res.headers.location : url;
      callback({shorturl:url,longurl:result});
    //});
  }).on('error', function (e) {callback({shorturl:url,longurl:url})}); //On error, return the original url passed
  req.end();
}

/**
 * Performs a binary search on the host array. This method can either be
 * injected into Array.prototype or called with a specified scope like this:
 * binaryIndexOf.call(someArray, searchElement);
 *
 * @param {*} searchElement The item to search for within the array.
 * @return {Number} The index of the element which defaults to -1 when not found.
 */
function binaryIndexOf(searchElement) {
    'use strict';

    var minIndex = 0;
    var maxIndex = this.length - 1;
    var currentIndex;
    var currentElement;

    while (minIndex <= maxIndex) {
        currentIndex = (minIndex + maxIndex) / 2 | 0;
        currentElement = this[currentIndex];

        if (currentElement < searchElement) {
            minIndex = currentIndex + 1;
        }
        else if (currentElement > searchElement) {
            maxIndex = currentIndex - 1;
        }
        else {
            return currentIndex;
        }
    }

    return -1;
}

Array.prototype.binaryIndexOf = binaryIndexOf;
Array.prototype.isArray = true;

module.exports.fileUploadDirectory = function() {
  return __dirname + '/../../public/files/';
}

//This is the hyperlink prefix that will be used for accessing the uploaded files via HTTP method
module.exports.linkToUploadedFiles = function() {
  return "/files/";
}

module.exports.resolveURLsWithExtService = function(urls, callback) {

  var key = require('../../config/auth').embedly.key; 

  //Lets expand the collected URLs using embedly service
  log.debug("Utils.resolveURLsWithExtService: %d URLs have to be sent to embed.ly.", urls.length);

  new embedly({key: key}, function(err, api) {
    if (!!err) {
      log.error('Error creating Embedly api');
      log.error(err.stack, api);
      callback(err);
      return;
    }

    /*
     * Embed.ly accepts a max of 20 URLs per call in the free plan, hence we have to slice the array here
     */
    var origURLs = urls.slice(0); //save the copy of URLs. This will be returned in the callback later ...
    var urlSubsets = [];
    while (urls.length) {
      urlSubsets.push( urls.splice(0, 20) );
    }

    async.concat(urlSubsets, embedlyAPI, function(error, newURLs) {
      if (error) {
        log.error("Utils.resolveURLsWithExtService: Failed during the call to Embedly. Error - %s", (error.err || error.message));
        callback(error);
        return;
      }
      
      callback(null, origURLs, newURLs);
      return;
    });

    function embedlyAPI(listOfUrls, cb) {
      var opts = { urls: listOfUrls,
                   maxWidth: 450,
                   wmode: 'transparent',
                   method: 'after' };

      api.oembed(opts, function(err, objs) {
        if (!!err) {
          log.error('request to embedly failed');
          log.error(err.stack, objs);
          if (objs && objs.type && (objs.type === "error")) {
            err.err = err.message = objs.error_message;
          }
          cb(err);
          return;
        }

        //Lets extract the embedded link to the photo/video or other media
        for (var i=0; i < objs.length; i++) {
          if (!objs[i].url && objs[i].html && objs[i].provider_name) {
            var link = null;
            switch(objs[i].provider_name.trim().toLowerCase()) {
              case "slideshare":
              case "vimeo":
              case "cnbc":
              case "eventbrite":
                link = objs[i].html.match(/url=http%3A%2F%2F(.*?)&/i);
                break;
              case "linkedin": 
                link = objs[i].html.match(/data-id=\\?"(.*?)\\?"\s+/i);
                break;
              case "medium":
              case "facebook":
                link = objs[i].html.match(/href=\\?"(.*?)\\?"/i);
                break;
            }

            if (link && link.isArray) {
              objs[i].url = link[1];
            }
          }
        }

        cb(null, objs);
      });
    } //embedlyAPI
  }); //new embedly()

}

module.exports.clone = function(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

module.exports.videoScreenShot = function(videoFilePath, screenShotFolder, callback) {
  var outputFile = null;
  
  new ffmpeg(videoFilePath)
    .on('filenames', function(filenames) {
       outputFile = filenames.join(', ');
    })
    .on('end', function() {
       console.log('Screenshots taken');
       callback(null, outputFile);
    })
    .on('error', function(err, stdout, stderr) {
       //    console.log('Cannot process video: ' + err.message);
      callback(err);
    })
    .screenshots({
        count: 1,
        folder: screenShotFolder,
        filename: '%b'
    });

}
