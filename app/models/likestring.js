// Strings for a single user
/* 
*/
var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;
var Bubble   = require('./bubble.js').Bubble;

var likeStringSchema = Schema({
	user: {type: Schema.Types.ObjectId, ref: "User", required: true}, // Each string is owned by someone
	slug: {type: String, required: true},
    name: String,
    bubbles: [{type: String, default: null}]
});

likeStringSchema.index({user:1, slug: 1}, {unique: true});

// create the model  and expose it to our app
module.exports = mongoose.model('LikeString', likeStringSchema);

