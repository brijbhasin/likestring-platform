// User model
/* Our user will have the ability to be linked to multiple social accounts and to a local account. 
   For local accounts, we will be keeping email and password. For the social accounts, we will be 
   keeping their id, token, and some user information.

   We will be hashing our password within our user model before it saves to the database. 
   This means we don’t have to deal with generating the hash ourselves. It is all handled nicely 
   and neatly inside our user model.   
*/
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({

    local            : {
        email        : String,
        password     : String,
        token        : String
    },
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    tokenCreationTime:  {type: Date, default: Date.now}
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);