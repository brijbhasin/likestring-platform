// likeString bubble model
/* 
*/
var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var vimeo = require('n-vimeo').video;
var request = require('request');
var cheerio = require('cheerio');

var Utils = require('./utils');
var BubbleCategory = require('./bubble_category');
var Tag = require('./bubble_content_tag');
var log   = require('../log');

var bubbleContentSchema = Schema({
  slug: {type: String, required: true, index: {unique: true}},
  type: {type: String, enum: ['dummy', 'photo', 'video', 'article', 'tweet']},
  title: {type: String, default: null},
  description: {type: String, default: null},
  resourceLink: {type: String, required: true}, //Link to the twitter/FB... image/video/post
  imageLink: {
    small: {type: String, default: null}, 
    medium:{type: String, default: null}, 
    large: {type: String, default: null},
  },  //Images are stored in SMALL/MEDIUM/LARGE sized links}
  created_at: {type: Date, default: Date.now},
  updated_at: {type: Date, default: null},
  tags: [{type: Schema.Types.ObjectId, ref: "BubbleContentTag"}],
  stats: {
    commentCount: {type: Number, default: 0},
    favoriteCount: {type: Number, default: 0},
    likeCount: {type: Number, default: 0},
    viewCount: {type: Number, default: 0},
    socialScore: {type: Number, defaults: 0}
  },
  extended_data: {type: Schema.Types.Mixed, default: null},
  rawObjectId: {type: Schema.Types.ObjectId, default: null} //links to the raw twitter/fb/google json object from which this document was derived.
});

var bubbleSchema = Schema({
	slug: {type: String, required: true, index: {unique: true}}, //Human friendly key to the bubble
  name: {type: String, default: null},
  category: { type: Schema.Types.ObjectId, ref: "BubbleCategory", default: null},

  created_at: {type: Date, default: Date.now},
  lastUpdated_at: {type: Date, default: Date.now},
  description: {type: String, default: null},
  image: {type: String, default: null}, //This is the one that shows up on the UI representing this bubble.
  owner: {type: Schema.Types.ObjectId, ref: "User", default: null}, //This user alone can modify the bubble contents.
  counters: {
    tweets: {type: Number, default: 0},
    photos: {type: Number, default: 0},
    videos: {type: Number, default: 0},
    articles: {type: Number, default: 0}
  },
  sources: [{type: Schema.Types.Mixed}],
  contents: [bubbleContentSchema] // Collection of photos/videos/posts etc around the topic represented by this bubble
}, { collection: 'bubbles' }).index({slug: 1, "contents.slug": 1}, {unique: true});

//This method uses the input parameters to set the various bubble properties.
//Missing fields are defaulted to NULL.
//Initially this bubble is empty and will be filled out content by other methods.
bubbleSchema.methods.fill = function(params, callback) {
  if (typeof params === 'function') {
    throw new Error('Bubble creation parameters are missing.');
    return this;
  }

  if (!params || !params.name || params.name.isEmpty()) {
    throw new Error('Bubble\'s name is mandatory.');
    return this;
  }

  if ( typeof callback !== 'function') {
     throw new Error('INVALID CALLBACK');
     return this;
  }

  var newBubble = {};
  newBubble.name = params.name;
  if (!params.slug) params.slug = Utils.createSlug(params.name);
  newBubble.slug = params.slug;
  newBubble.description = params.description || null;
  newBubble.image = params.image || null;
  newBubble.owner = params.user || null;
  newBubble.sources = [];
  //Bubbles have content records and each content record has a key. However, when a brand new bubble is
  //created, there is no content record. 
  //Inserting a dummy content record to please MongoDB, else we are not allowed to insert another
  //Bubble with no content.
  newBubble.contents = [{slug: params.slug, type: 'dummy', resourceLink: 'http://localhost'}];

  var self = this;
  if (params.category && !params.category.isEmpty()) {
    BubbleCategory.findOne({slug: Utils.createSlug(params.category)}, function(error, category) {
      if (error) callback(error);
      newBubble.category = category;
      callback(null, newBubble);
      return self;
    });
  } else {
    newBubble.category = null;
    callback(null, newBubble);
    return self;
  }

  return this;
}

bubbleContentSchema.methods.ClassifyResourceLink = function() {

  var link = this.resourceLink;

  if (!link) return false;

  if (link.match(/youtube\.com|youtu\.be/i)) {
    log.silly("BubbleContentModel.ClassifyResourceLink: Link: %s - VIDEO", link);

    this.type = 'video';
    //Create the thumbnails for the vide...
    var urlparts = link.match(/v=([A-Z,a-z,0-9,\-,_]+)/i);
    if (urlparts === null) {
      //Maybe this is a shortened URL...
      urlparts = link.match(/youtu.be\/([A-Z,a-z,0-9,\-,_]+)/i);
    }

    if (urlparts != null) {
      this.imageLink.small = "http://img.youtube.com/vi/" + urlparts[1] + "/default.jpg";
      this.imageLink.medium = "http://img.youtube.com/vi/" + urlparts[1] + "/mqdefault.jpg";
      this.imageLink.large = "http://img.youtube.com/vi/" + urlparts[1] + "/hqdefault.jpg";
      return true;
    }

    return false; //Couldnt process the YT link...

  } else if (urlComponents = link.match(/twitpic\.com\/(.*)/i)) {
    log.silly("BubbleContentModel.ClassifyResourceLink: link: %s - PHOTO", link);
      
    this.type = 'photo';
    this.resourceLink = "http://twitpic.com/show/full/" + urlComponents[1] + ".jpg";
    this.imageLink.small = "http://twitpic.com/show/thumb/" + urlComponents[1] + ".jpg";
    this.imageLink.medium = this.imageLink.large = this.resourceLink; 
    return true;

  } else if (urlComponents = link.match(/ow\.ly\/i\/(.*)/i)) {
    log.silly("BubbleContentModel.ClassifyResourceLink: link: %s - PHOTO", link); 
      
    this.type = 'photo';
    this.resourceLink = "http://static.ow.ly/photos/normal/" + urlComponents[1] + ".jpg";
    this.imageLink.small = "http://static.ow.ly/photos/thumb/" + urlComponents[1] + ".jpg";
    this.imageLink.medium = this.imageLink.large = this.resourceLink;
    return true;
    
  } else if (urlComponents = link.match(/instagram\.com\/p\/|instagr\.am\/p\//i)) {
    log.silly("BubbleContentModel.ClassifyResourceLink: link: %s - PHOTO", link); 
    this.type = 'photo';
    this.resourceLink = this.resourceLink + "/media";
    this.imageLink.small = this.resourceLink + "?size=t";
    this.imageLink.medium = this.resourceLink + "?size=m";
    this.imageLink.large = this.resourceLink + "?size=l";
    return true;
  
  } else if( (new RegExp( '\\b' + Utils.IGNORE_URL_LIST.join('\\b|\\b') + '\\b') ).test(link) ) {
    log.silly("BubbleContentModel.ClassifyResourceLink: link: %s - TWEET", link); 
    this.type = 'tweet';

    return true;
  }

  return false;

}

bubbleContentSchema.methods.createVimeoThumbs = function(callback) {
  var urlparts = this.resourceLink.match(/vimeo.com\/(.*)/);
  if (urlparts == null) {
    return callback(null, this);
  }

  log.silly("BubbleContentModel.createVimeoThumbs: Key: %s, Vimeo link: %s", this.slug, this.resourceLink);

  var record = this;
  vimeo(urlparts[1], function(err, data) {
    if (err) {
      log.error("BubbleContentModel.createVimeoThumbs. Failed to create thumbnail for Key: %s, vimeo link: %s, error: %s",
             record.slug, record.resourceLink, (err.err || err.message));      
      return callback(null, this);
    }

    record.type = 'video';
    record.imageLink = {small: null, medium: null, large: null}; //We want these fields to be present for all database documents
                                                                 // hence setting them to "null" else if it is undefined then the 
                                                                 //database record will be missing these fields.
    if (data) {
      record.imageLink.small = data.thumb.s;
      record.imageLink.medium = data.thumb.m;
      record.imageLink.large = data.thumb.l;
    }

    log.silly("BubbleContentModel.createVimeoThumbs: Thumbnail created Key: %s, vimeo link: %s", record.slug, record.resourceLink);

    return callback(null, record);
  });
} //createVimeoThumbs()

bubbleContentSchema.methods.createVineThumbs = function(callback) {
  var record = this;
  var urlparts = record.resourceLink.match(/vine.co\/v\/(\w+)/);
  if (urlparts == null) {
    return callback(null, record);
  }

  log.silly("BubbleContentModel.createVineThumbs: Key: %s, Vine link: %s", record.slug, record.resourceLink);

  request.get('https://vine.co/v/'+ urlparts[1], function(error, resp) {
    if (error) {
      log.error("BubbleContentModel.createVineThumbs. Failed to create thumbnail for Key: %s, vimeo link: %s, error: %s",
             record.slug, record.resourceLink, (err.err || err.message));
      return callback(null, record);
    }

    record.type = 'video';
    record.imageLink = {small: null, medium: null, large: null}; //We want these fields to be present for all database documents
                                                                 // hence setting them to "null" else if it is undefined then the 
                                                                 //database record will be missing these fields.
    var $ = cheerio.load(resp.body);
    var metaData = $('meta[itemprop="contentUrl"]');
    if (metaData && metaData.length) {

       if (metaData[0] && metaData[0].attribs && metaData[0].attribs.content)
         record.resourceLink = metaData[0].attribs.content;
      
       if (metaData[1] && metaData[1].attribs && metaData[1].attribs.content) {
         record.imageLink.small = metaData[1].attribs.content;
         record.imageLink.medium = record.imageLink.large = record.imageLink.small;
       }
    
    }

    log.silly("BubbleContentModel.createVineThumbs. Thumbnail created. Key: %s, vimeo link: %s", record.slug, record.resourceLink);

    return callback(null, record);
  });
} //createVineThumbs()


// create the model  and expose it to our app
module.exports.BubbleContent = mongoose.model('BubbleContent', bubbleContentSchema);
module.exports.Bubble = mongoose.model('Bubble', bubbleSchema);
