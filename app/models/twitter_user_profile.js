// Twitter user profile model
/* This model holds a subset of the 
   document fields returned by the Twitter REST api v1.1. The actual raw data obtained is also saved by   
   tacking it to the end of the document. It could be used to pull out more fields (if need be) in future.
*/
var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

// define the schema for our user model
var twitterUserProfileSchema = Schema({
	
	id: {type: Number, index: true, unique: true}, // This is the same as Twitter user_id. Had to pull it out to do indexing.
	friends: [Number], // List of friend ids for this user. This will be filled out by making an additional call to
	                   // Twitter.
	raw : Schema.Types.Mixed // This is the unmodified data, as obtained from Twitter

}, { collection: 'twitter_user_profiles' });

// create the model  and expose it to our app
module.exports = mongoose.model('TwitterUserProfile', twitterUserProfileSchema);

