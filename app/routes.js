var express = require('express');
var router = express.Router();        // get an instance of the express Router

// all the routes for our application 

module.exports = function(app, handlers) {
  
	app.get('/', handlers.page.homePage);
	app.get('/auth/local', handlers.page.loginPage);
  app.get('/signup', handlers.page.signupPage);
  //app.get('/profile', isLoggedIn, handlers.page.profilePage);

	app.post('/auth/local', handlers.auth.setOauthProvider("local").login);
	app.post('/signup', handlers.auth.setOauthProvider("local").signup);

  //FIXME: I couldnt push the passport.authenticate calls to a handler
  //because nodejs middleware doesnt like to call passport.auth wrapped in a function.
  //Will have to look into this later. 
  var passport = handlers.passport;
  var isAuthenticated = passport.authenticate('bearer', { session : false });

	app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));
	app.get('/auth/facebook/callback', handlers.auth.setOauthProvider("facebook").loginCB);

	app.get('/auth/twitter', passport.authenticate('twitter'));
	app.get('/auth/twitter/callback', handlers.auth.setOauthProvider("twitter").loginCB);

  app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));
  app.get('/auth/google/callback', handlers.auth.setOauthProvider("google").loginCB);

  app.get('/auth/isloggedin', handlers.auth.isLoggedIn.bind(handlers.auth));
	app.post('/auth/logout', handlers.auth.Logout.bind(handlers.auth));

  app.post('/auth/twitter', handlers.auth.setOauthProvider("twitter").login);

// =============================================================================
// AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
// =============================================================================
	// locally --------------------------------
	app.post('/connect/local', handlers.auth.setOauthProvider("local").authorizeCB);

	app.get('/connect/facebook', passport.authorize('facebook', { scope : 'email' }));
	app.get('/connect/facebook/callback', handlers.auth.setOauthProvider("facebook").authorizeCB);
	
	app.get('/connect/twitter', passport.authorize('twitter', { scope : 'email' }));
	app.get('/connect/twitter/callback', handlers.auth.setOauthProvider("twitter").authorizeCB);

	app.get('/connect/google', passport.authorize('google', { scope : ['profile', 'email'] }));
	app.get('/connect/google/callback', handlers.auth.setOauthProvider("google").authorizeCB);

	// =============================================================================
	// UNLINK ACCOUNTS =============================================================
	// =============================================================================
	// used to unlink accounts. for social accounts, just remove the token
	// for local account, remove email and password
	// user account will stay active in case they want to reconnect in the future

  app.get('/unlink/local', handlers.auth.setOauthProvider("local").unlink);
  app.get('/unlink/facebook', handlers.auth.setOauthProvider("facebook").unlink);
  app.get('/unlink/twitter', handlers.auth.setOauthProvider("twitter").unlink);
  app.get('/unlink/google', handlers.auth.setOauthProvider("google").unlink);

  app.post('/file/upload', handlers.api.uploadFile);
  app.get('/file/:name', handlers.api.getUploadedFile);
  // REGISTER OUR API ROUTES ------------------------------- 

  // all of our routes will be prefixed with /v1/api
  app.use('/v1/api', router); 
  app.set('json spaces', 4);

  router.route('/strings')
    .get(isAuthenticated, handlers.api.getLikeStrings);

  router.route('/strings/:user_id')
    .get(isAuthenticated, handlers.api.getLikeStrings);

  router.route('/string/:string_id')      
    .post(isAuthenticated, handlers.api.addLikeString)
    .delete(isAuthenticated, handlers.api.deleteLikeString)
    .put(isAuthenticated,  handlers.api.modifyLikeString);

  router.route('/string/:string_id/bubbles')
    .get(isAuthenticated, handlers.api.getBubbles);

  router.route('/string/:string_id/bubble/:bubble_id')
    // Add a existing bubble to the given likeString
    .post(isAuthenticated, handlers.api.addBubbleToLikeString)
    //Delete a bubble from the given likeString
    .delete(isAuthenticated, handlers.api.deleteBubbleFromLikeString);

  router.route('/string/:from_string_id/bubble/:bubble_id/to/:to_string_id')
    // Move the referenced bubble from one string to another
    .put(isAuthenticated, handlers.api.moveBubble);

  router.route('/bubble/:bubble_id')
    .post(isAuthenticated, handlers.api.addBubble)
    .delete(isAuthenticated, handlers.api.deleteBubble)
    .put(isAuthenticated, handlers.api.modifyBubble);

  //Bubble Details APIs
  router.route('/bubble/:bubble_id')
    .get(isAuthenticated, handlers.api.getBubbleDetails);

  router.route('/bubble/:bubble_id/refresh')
    .get(isAuthenticated, handlers.api.refreshBubbleContent);

  router.route('/bubble/:bubble_id/items/latest')
    .get(isAuthenticated, handlers.api.getBubbleContent);

  router.route('/bubble/:bubble_id/items/:type')
    .get(isAuthenticated, handlers.api.getBubbleContent);

  router.route('/bubble/:bubble_id/items/:type/:item_id')
    .get(isAuthenticated, handlers.api.getBubbleContent);

  router.route('/bubbles/search')
    .get(isAuthenticated, handlers.api.searchBubbles);

  router.route('/bubbles/search/:query')
    .get(isAuthenticated, handlers.api.searchBubbles);

  router.route('/bubble/:bubble_id/items/update')
    .post(isAuthenticated, handlers.api.changeBubbleContent);

  router.route('/bubble/:bubble_id/items/add')
    .post(isAuthenticated, handlers.api.addBubbleContent);

  router.route('/users')
    .get(isAuthenticated, handlers.api.getUsers);

}; // === END OF ROUTES =====

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on 
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
}