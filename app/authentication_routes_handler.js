var passport = require('passport');
var ObjectId = require('mongodb').ObjectID;
var config = require('../config/auth');
var Util   = require('./models/utils');
// load up the user model
var User          = require('./models/user');

var log = require('./log');

function GenericOauthProvider() {

  this.signup = function(req, res, next) {
    res.status(501);
    return next(new Error("Signup not implemented"));
  }

  this.login = function(req, res, next) {
    res.status(501);
    return next(new Error("Login not implemented"));
  }

  this.loginCB = function(req, res, next) {
    res.status(501);
    return next(new Error("LoginCB not implemented"));
  }

  this.logout = function(req, res, next) {
    res.status(501);
    return next(new Error("Logout not implemented"));
  }

  this.authorizeCB = function(req, res, next) {
    res.status(501);
    return next(new Error("Authorize not implemented"));
  }

  this.unlink = function(req, res, next) {
    res.status(501);
    return next(new Error("Unlink not implemented"));
  }

  this.getUser = function(req, res, next) {
    res.status(501);
    return next(new Error("getUser not implemented"));       
  }
} //GenericOauthProvider()

function LocalOauthProvider() {

  GenericOauthProvider.apply(this, arguments);

  this.signup = function(req, res, next) {
    passport.authenticate('local-signup', function(err, user, info) {
      if (err) { return next(err); }
      if (!user) {
        res.status(500);
        log.error('Local signup failed!');
        res.send({ error: "Local signup failed!."});
        return;      
      }
      req.login(user, function(err) {
        if (err) { return next(err); }
        res.status(200);
        log.debug('Local signup successful. User: %s', JSON.stringify(user));
        res.send({ access_token: user.local.token, access_token_provider: 'local' });
        return;
        //return res.redirect('/profile'); // redirect to the secure profile section
      });
    })(req, res, next);
  }

  this.login = function(req, res, next) {
    passport.authenticate('local-login', function(err, user, info) {
      if (err) { return next(err); }
      if (!user) { 
        res.status(404);
        log.error('Local login failed. User not found!');
        res.send({ error: "User ID not found."});
        return;
      }
      req.login(user, function(err) {
        if (err) { return next(err); }
        res.status(200);
        log.debug('Local login successful. User: %s', JSON.stringify(user));
        res.send({ access_token: user.local.token, access_token_provider: 'local' });
        return;
        //return res.redirect('/profile'); // redirect to the secure profile section
      });
    })(req, res, next);
  }

  this.loginCB = function(req, res, next) {
    res.status(501);
    return next(new Error("LoginCB not implemented"));
  }

  this.logout = function(req, res, next) {

    log.debug("Logout LOCAL User: User Record = %s", JSON.stringify(req.user.local));

    User.findOne({"local.token": req.user.local.token}, function(error, user) {
      if (error) return next(error);
      if (!user) {
        res.status(401);
        res.json({error: 'User does not exist OR User session has expired.'});
        return;
      }
          
      req.logout();
      
      user.local.token = null;      
      user.save(function(error) {
        if (error) return next(error);
        res.status(200);
        res.json({message: 'Logout successful!'});
        return;
      });
    });
  }

  this.authorizeCB = function(req, res, next) {
    passport.authenticate('local-signup', function(err, user, info) {
      if (err) { return next(err); }
      if (!user) {
        res.status(500);
        log.error('Local signup failed!');
        res.send({ error: "Local signup failed!."});
        return;      
      }

      req.login(user, function(err) {
        if (err) { return next(err); }
        res.status(200);
        log.debug('Local signup successful. User: %s', JSON.stringify(user));
        res.json(user);
        return;

        //return res.redirect('/profile'); // redirect to the secure profile section
      });
    })(req, res, next);
  }

  this.unlink = function(req, res, next) {
    var token           = null;
    if (req.user) {
      token = req.user.local.token;
    } else {
      token = req.query.access_token;
    }

    if (!token)
      return;

    User.findOne({"local.token": token}, function(error, user) {
      if (error) return next(error);

      user.local.email    = undefined;
      user.local.password = undefined;
      user.save(function(err) {
        res.status(200);
        log.debug('Unlinked local account. User: %s', JSON.stringify(user));
        res.json(user);
        //res.redirect('/profile');
      });
    }) //find the User document for the given token
  }

  this.getUser = function(user, done) {

    User.findOne({"local.token": user.local.token}, done);

  }

} //LocalOauthProvider()


function TwitterOauthProvider() {

  GenericOauthProvider.apply(this, arguments);

  //
  // iOS or any client will handle the Twitter OAuth workflow, independent of our backend. Once that is done,
  // the client will post the authorized twitter profile and the accessToken to the backend. 
  // This routine will handle that POST call and create a new USER record or update existing one.
  //
  this.login = function(req, res, next) {
    //
    // FIXME: Check the Twitter reverse auth process. We need our backend to be able to make API calls using the 
    // accesstoken posted by the front-end.
    log.info("Twitter Login (POST) - Client has already authenticated the account and now posting the profile to backend.");
    //POST's form data is empty or missing?
    if (Object.keys(req.body).length === 0) {
      return next(new Error('Twitter access_token and profile data is missing.'));
    }

    var formData = req.body;
    log.debug("Twitter Login(POST) - formData: %s", JSON.stringify(formData));
    if (!formData.profile || (Object.keys(JSON.parse(formData.profile)).length == 0)) {
      return next(new Error('Twitter profile data is missing!'));
    } else {
      formData.profile = JSON.parse(formData.profile);
    }

    process.nextTick(function() {
      
      //FIXME: We have to figure Twitter Reverse Auth and then uncomment the code below.
      //We will be tying our consumer key+secret with the access_token+secret sent by the
      //client for the user who has logged into the client.
      // Ref: https://dev.twitter.com/docs/ios/using-reverse-auth
      //
      /* var twit = require('twit');
      var cfg = {};

      cfg.consumer_key = config.twitterAuth.consumerKey;
      cfg.consumer_secret = config.twitterAuth.consumerSecret;
      cfg.access_token = formData.profile.oauth_token;
      cfg.access_token_secret = formData.profile.oauth_token_secret;
    
      log.debug("Twitter Login(POST) - Twitter Client configuration: %s", JSON.stringify(cfg));

      var Twitter = new twit(cfg);

      log.info("Twitter Login(POST) - Verifying the twitter credentials");

      Twitter.get('account/verify_credentials', null, function(error, profile) {
        if (error) return next(error);

        log.debug("Twitter Login(POST) - Twitter account verified. Profile: %s", JSON.stringify(profile));
        //Twitter account is fine. Lets create the User record, if it doesnt already exist in our database
        */
        //FIXME: - Delete the below 'profile' local variable, once we use Twitter to authenticate later.
        var profile = formData.profile;
        profile.id = formData.profile.user_id;
        profile.name = profile.screen_name;

        User.findOne({ 'twitter.id' : profile.id }, function(err, user) {
          if (err) return next(err);

          // if the user is found then log them in
          if (user) {
            // if there is a user id already but no token (user was linked at one point and then removed)
            // just add our token and profile information
            log.debug("Twitter Login(POST) - Found the user record with twitter id: %s", profile.id);

            user.twitter.token = formData.profile.oauth_token;
            user.twitter.username  = profile.screen_name;
            user.twitter.displayname = profile.name;

            user.save(function(err) {
              if (err) return next(err);
              req.login(user, function(err) {
                if (err) { return next(err); }
                res.status(200);
                log.debug('Twitter Login(POST) - Updated the access token for user with twitter id: %s', user.twitter.id);
                res.send({ message: 'Login successful!'});
                return;
              });
            });

          } else {
            log.debug("Twitter Login(POST) - Creating a new user record with twitter id: %s", profile.id);
            // if there is no user, create them
            var newUser = new User();

            // set all of the user data that we need
            newUser.twitter.id          = profile.id;
            newUser.twitter.token       = formData.profile.oauth_token;
            newUser.twitter.username    = profile.screen_name;
            newUser.twitter.displayName = profile.name;

            // save our user into the database
            newUser.save(function(err, user) {
              if (err) return next(err);

              req.login(user, function(err) {
                if (err) { return next(err); }
                res.status(200);
                log.debug('Twitter Login(POST) - Updated the access token for user with twitter id: %s', user.twitter.id);
                res.send({ message: 'Login successful!'});
                return;
              });
            });
          }
        });
      //}); Twitter.get()
    }); //process.nextTick()
  }

  //
  // This method is called by Twitter (registered with the twitter app) after the user has been
  // successfully authenticated by twitter.
  //
  this.loginCB = function(req, res, next) {
    passport.authenticate('twitter', function(err, user, info) {
      if (err) { return next(err); }
      if (!user) { 
        res.status(502);
        log.error('Twitter login failed!');
        res.send({ error: "Twitter login failed. Couldnt locate the user on local database."});
        return;      
      }      
      req.login(user, function(err) {
        if (err) { return next(err); }
        res.status(200);
        log.debug('Twitter signup successful. User: %s', JSON.stringify(user));
        res.send({ access_token: user.twitter.token, access_token_provider: 'twitter' });
        return;
        //return res.redirect('/profile'); // redirect to the secure profile section
      });
    })(req, res, next);
  }

  this.logout = function(req, res, next) {

    log.debug("Logout TWITTER User: User Record = %s", JSON.stringify(req.user.twitter));

    var params = {};
    params["twitter.token"] = req.user.twitter.token;
    if (req.body.userid) params["twitter.id"] = req.body.userid;
    User.findOne(params, function(error, user) {
      if (error) return next(error);
      if (!user) return next(new Error("User does not exist OR User session has expired."));
          
      req.logout();
      
      user.twitter.token = null;      
      user.save(function(error) {
        if (error) return next(error);
        res.status(200);
        res.send({message: 'Logout successful!'});
        return;
      });
    });
  }

  this.authorizeCB = function(req, res, next) {
    passport.authorize('twitter', function(err, user, info) {
      if (err) { return next(err); }
      if (!user) {
        res.status(502);
        log.error('Twitter authorization failed!');
        res.send({ error: "Twitter authorization failed. Couldnt locate the user on local database."});
        return;      
      }      

      req.login(user, function(err) {
        if (err) { return next(err); }
        res.status(200);
        log.debug('Twitter authorization successful. User: %s', JSON.stringify(user));
        res.json(user);
        return;

        //return res.redirect('/profile'); // redirect to the secure profile section
      });
    })(req, res, next);
  }

  this.unlink = function(req, res, next) {
    var user           = req.user;
    user.twitter.token = undefined;
    user.save(function(err) {
      res.status(200);
      log.debug('Unlinked Twitter account. User: %s', JSON.stringify(user));
      res.json(user);
      //res.redirect('/profile');
    });
  }  

} //TwitterOauthProvider()

function FacebookOauthProvider() {

  GenericOauthProvider.apply(this, arguments);

  this.login = function(req, res, next) {
    res.status(501);
    return next(new Error("Login not implemented"));
  }

  this.loginCB = function(req, res, next) {
    passport.authenticate('facebook', function(err, user, info) {
      if (err) { return next(err); }
      if (!user) { 
        res.status(502);
        log.error('Facebook login failed!');
        res.send({ error: "Facebook login failed. Couldnt locate the user on local database."});
        return;      
      }
      req.login(user, function(err) {
        if (err) { return next(err); }
        res.status(200);
        log.debug('Facebook signup successful. User: %s', JSON.stringify(user));
        res.send({ access_token: user.local.token, access_token_provider: 'facebook' });
        return;
      });
    })(req, res, next);
  }

  this.logout = function(req, res, next) {

    log.debug("Logout FACEBOOK User: User Record = %s", JSON.stringify(req.user.facebook));

    var params = {};
    params["facebook.token"] = req.user.facebook.token;
    if (req.user.query.userid) params["facebook.id"] = req.user.query.userid;
    User.findOne(params, function(error, user) {
      if (error) return next(error);
      if (!user) return next(new Error("User does not exist OR User session has expired."));
          
      req.logout();
      
      user.facebook.token = null;      
      user.save(function(error) {
        if (error) return next(error);
        res.status(200);
        res.send({message: 'Logout successful!'});
        return;
      });
    });  }

  this.authorizeCB = function(req, res, next) {
    passport.authorize('facebook', function(err, user, info) {
      if (err) { return next(err); }
      if (!user) { 
        res.status(502);
        log.error('Facebook authorization failed!');
        res.send({ error: "Facebook authorization failed. Couldnt locate the user on local database."});
        return;      
      }      

      req.login(user, function(err) {
        if (err) { return next(err); }
        res.status(200);
        log.debug('Facebook authorization successful. User: %s', JSON.stringify(user));
        res.json(user);
        return;
      });
    })(req, res, next);
  }

  this.unlink = function(req, res, next) {
    var user            = req.user;
    user.facebook.token = undefined;
    user.save(function(err) {
      res.status(200);
      log.debug('Unlinked Facebook account. User: %s', JSON.stringify(user));
      res.json(user);
    });
  }

  this.getUser = function(user, done) {

    User.findOne({"twitter.token": user.twitter.token}, done);

  }

} //FacebookOauthProvider()

function GoogleOauthProvider() {

  GenericOauthProvider.apply(this, arguments);

  this.login = function(req, res, next) {
    res.status(501);
    return next(new Error("Login not implemented"));
  }

  this.loginCB = function(req, res, next) {
    passport.authenticate('google', function(err, user, info) {
      if (err) { return next(err); }
      if (!user) {
        res.status(502);
        log.error('Google login failed!');
        res.send({ error: "Google login failed. Couldnt locate the user on local database."});
        return;      
      }      
      req.login(user, function(err) {
        if (err) { return next(err); }
        res.status(200);
        log.debug('Google signup successful. User: %s', JSON.stringify(user));
        res.send({ access_token: user.local.token, access_token_provider: 'google' });
        return;
      });
    })(req, res, next);
  }

  this.logout = function(req, res, next) {

    log.debug("Logout GOOGLE User: User Record = %s", JSON.stringify(req.user.google));

    var params = {};
    params["google.token"] = req.user.google.token;
    if (req.user.query.userid) params["google.id"] = req.user.query.userid;
    User.findOne(params, function(error, user) {
      if (error) return next(error);
      if (!user) return next(new Error("User does not exist OR User session has expired."));
          
      req.logout();
      
      user.google.token = null;      
      user.save(function(error) {
        if (error) return next(error);
        res.status(200);
        res.send({message: 'Logout successful!'});
        return;
      });
    });  }

  this.authorizeCB = function(req, res, next) {
    passport.authorize('google', function(err, user, info) {
      if (err) { return next(err); }
      if (!user) {
        res.status(502);
        log.error('Google authorization failed!', JSON.stringify(user));
        res.send({ error: "Google authorization failed. Couldnt locate the user on local database."});
        return;      
      }      

      req.login(user, function(err) {
        if (err) { return next(err); }
        res.status(200);
        log.debug('Google authorization successful. User: %s', JSON.stringify(user));
        res.json(user);
        return;
      });
    })(req, res, next);
  }

  this.unlink = function(req, res, next) {
    var user          = req.user;
    user.google.token = undefined;
    user.save(function(err) {
      res.status(200);
      log.debug('Unlinked Google account. User: %s', JSON.stringify(user));
      res.json(user);
    });
  }

  this.getUser = function(user, done) {

    User.findOne({"google.token": user.google.token}, done);

  }

} //GoogleOauthProvider()


var Handler = module.exports = function() {
  this.provider = new GenericOauthProvider();
};

Handler.prototype.setOauthProvider = function(oauthProvider) {
  if (oauthProvider === null) oauthProvider = "local";

  switch(oauthProvider) {
    case "twitter":
      this.provider = new TwitterOauthProvider();
      break;
    case "facebook":
      this.provider = new FacebookOauthProvider();
      break;
    case "google":
      this.provider = new GoogleOauthProvider();
      break;
    default: //local
      this.provider = new LocalOauthProvider();
      break;
  }

  return this.provider;
}

Handler.prototype.Logout= function(req, res, next) {
  if (!req.user) {
    //Maybe, we dont have session management. So there is no user record serialized to the session. 
    //Lets parse the request body to get the user record key, etc.
    req.user = {};

    //POST's form data is empty or missing?
    if (Object.keys(req.body).length === 0) {
      return next(new Error('User access_token and profile data is missing.'));
    }

    if (!req.body.access_token || req.body.access_token.isEmpty()) {
      res.status(401);
      res.json({error: 'User access_token and profile data is missing.'});
      return;
    } else {
      log.debug("Logout: Setting the access_token and provider, as provided in the POST request");
      req.body.access_token_provider = req.body.access_token_provider || "local";
      req.user[req.body.access_token_provider] = {token: req.body.access_token};
    }
  }  

  log.debug("Logout: Current logged in User = %s", JSON.stringify(req.user));

  if (req.body.access_token_provider && !req.body.access_token_provider.isEmpty()) {
    this.setOauthProvider(req.body.access_token_provider);
    return this.provider.logout(req, res, next);
  }

  //In case the client hasnt told us the Oauth provider they have used for the current session, we will
  //use the serialized user object found in the HTTP request.
  log.debug("Logout: Access_token_provider was not specified in the request. ");
  log.debug("Logout: Trying to get a best guess of the token_provider using request's user object: %s", JSON.stringify(req.user));

  var self = this;
  User.findOne({_id: new ObjectId(req.user.id)}, function(error, userRecord) {
    if (error) return next(error);
    if (!userRecord) {
      res.status(401);
      res.json({error: "Session expired or Unknown user"});
      return;
    }

    log.debug("Logout: User retrieved from the database using the serialized user record in the request: %s", JSON.stringify(userRecord));

    if (userRecord.twitter.token)
      self.setOauthProvider("twitter");
    else if (userRecord.facebook.token)
      self.setOauthProvider("facebook");
    else if (userRecord.google.token)
      self.setOauthProvider("google");
    else
      self.setOauthProvider("local");

    return self.provider.logout(req, res, next);

  });

}

Handler.prototype.isLoggedIn = function(req, res, next) {
  // if user is authenticated in the session, then return true
  if (req.isAuthenticated()) {
    res.status(200);
    res.json({message: true});
    return;
  }

  //Maybe, we dont have session management. So there is no user record serialized to the session. 
  //Lets parse the request URL to get the user record key, etc.
  req.user = {};

  if (!req.query.access_token || req.query.access_token.isEmpty()) {
    res.status(401);
    res.json({error: 'User access_token or session data is missing.'});
    return;
  } else {
    log.debug("isLoggedIn: Setting the access_token and provider, as provided in the GET request");
    req.query.access_token_provider = req.query.access_token_provider || "local";
    req.user[req.query.access_token_provider] = {token: req.query.access_token};
  }

  //Query the database to make sure we have a valid token (in other words, the user is logged in)
  this.provider.getUser(req.user, function(error, user) {
    if (error) return next(error);
    
    log.debug("isLoggedIn: Database user document = %s", JSON.stringify(user));

    if (!user) {
      res.status(401);
      res.json({message: false});
    } else {
      res.status(200);
      res.json({message: true});
    }
    return;
  })
}