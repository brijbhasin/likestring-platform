var Handler = module.exports = function() {
};

// =====================================
// HOME PAGE (with login links) ========
// =====================================
Handler.prototype.homePage = function(req, res) {
  res.render('index.ejs'); // load the index.ejs file
};

// =====================================
// LOGIN ===============================
// =====================================
// show the login form
Handler.prototype.loginPage = function(req, res) {
  // render the page and pass in any flash data if it exists
  res.render('login.ejs', { message: req.flash('loginMessage') });
};

// =====================================
// SIGNUP ==============================
// =====================================
// show the signup form
Handler.prototype.signupPage = function(req, res) {
  // render the page and pass in any flash data if it exists
  res.render('signup.ejs', { message: req.flash('signupMessage') });
};

// =====================================
// PROFILE SECTION =====================
// =====================================
Handler.prototype.profilePage = function(req, res) {
  res.render('profile.ejs', { user : req.user });
};

