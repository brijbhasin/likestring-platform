var fs = require('fs');
var mkdirp = require('mkdirp');
var mime = require('mime');
var async = require('async');

var LikeStringAPI = require('../data/curate/likestring');
var BubbleAPI = require('../data/curate/bubble');

var Utils = require('./models/utils');
var UserModel  = require('./models/user');

var log = require('./log');
var Handler = module.exports = function() {
};

Handler.prototype.getLikeStrings = function(req, res) {

  async.waterfall([ 
    function getUser(asyncCB) {
      if (!req.params.user_id || req.params.user_id.isEmpty()) {
        asyncCB(null, req.user);
      } else {
        UserModel.findById(req.params.user_id.trim(), asyncCB);
      }
    },
    function getStrings(user, asyncCB) {
      var api = new LikeStringAPI(user);

      log.info("API.getLikeStrings: Getting the likeStrings belonging to user: %s", JSON.stringify(user));
      api.getUserLikeStrings(function(error, strings) {
        if (error) {
          asyncCB(error);
          return;
        }

        //User doesnt have any strings, lets create some defaults and send
        if (!strings || (strings.length == 0)) {
          
          log.debug("API.getLikeStrings: creating default set of strings and bubbles for the new user.");
          api.defaults(function(error, strings) {
            if (error) {
              asyncCB(error);
              return;
            }

            api.toJSON(strings, function(error, data) {
              if (error) {
                asyncCB(error);
                return;
              }
              log.debug("API.getLikeStrings: Created set of strings: ", JSON.stringify(data));
              asyncCB(null, data);
            });
          });
        } else {
          api.toJSON(strings, asyncCB);
        }
      });
    }],
    function(error, strings) {
      if (error) {
        log.error("API.getLikeStrings: Error -  %s", (error.err || error.message));
        res.status(500);
        return res.send({error: (error.err || error.message)});
      }
      log.debug("GET likeStrings: Obtained these strings: ", JSON.stringify(strings));
      res.json(strings);
    }
  );

}

Handler.prototype.addLikeString = function(req, res) {
  var api = new LikeStringAPI(req.user);

  log.info("API.addLikeString: Adding a new likeString [%s] belonging to user: %s", req.params.string_id, JSON.stringify(req.user));

  api.create({name: (req.body.name || req.params.string_id)}, function(error, data) {
    if (error) {
      log.error("API.addLikeString: Error - %s", (error.err || error.message));
      res.status(500);
      return res.json({error: (error.err || error.message)});
    }
    log.debug("API.addLikeString: Added the string: ", JSON.stringify(data));
    if (!data) data = {};
    res.status(200);
    res.json(data);
  });	
}

Handler.prototype.deleteLikeString = function(req, res) {

  log.info("API.deleteLikeString: Deleting a likeString [%s] belonging to user: %s", req.params.string_id, JSON.stringify(req.user));

  if (!req.params.string_id || req.params.string_id.isEmpty()) {
    
    log.debug("API.deleteLikeString: Nothing to delete. String ID is NULL.");
    res.status(204);
    return res.json({message: "No deletes done."});
  }

  var api = new LikeStringAPI(req.user, req.params.string_id);

  //There is an option to delete a different likeString instead of the one named in the URL.
  api.remove({name: req.params.string_id}, function(error, data) {
    if (error) {
      log.error("API.deleteLikeString: %s", (error.err || error.message));
      res.status(500);
      return res.json({error: (error.err || error.message)});
    }
    res.status(200);
    res.json(data);
  });
}

Handler.prototype.modifyLikeString = function(req, res) {

  log.info("API.modifyLikeString: Changing meta data for likeString [%s] belonging to  user: %s", req.params.string_id, JSON.stringify(req.user));

  var api = new LikeStringAPI(req.user, req.params.string_id);

  if ((typeof req.body.name === 'undefined') || !req.body.name || req.body.name.isEmpty()) {
    res.status(204);
    res.json({message: "No updates done."});
  }

  log.debug("API.modifyLikeString: Changing the string name from [%s] to [%s]", req.params.string_id, req.body.name);
  api.changeName({name: req.body.name}, function(error, data) {
    if (error) {
      log.error("API.modifyLikeString: Error - %s", (error.err || error.message));
      res.status(500);
      return res.json({error: (error.err || error.message)});
    }
   
    log.debug("API.modifyLikeString: Updated the database successfuly.");
    res.json(data);
  });
}

Handler.prototype.getBubbles = function(req, res) {
  var api = new LikeStringAPI(req.user, req.params.string_id);

  log.info("API.getBubbles: Getting the bubbles for string [%s] belonging to user: %s", 
           req.params.string_id, JSON.stringify(req.user));

  api.getUserLikeStrings({user: req.user, slug: Utils.createSlug(req.params.string_id)}, function(error, strings) {
    if (error) {
      log.error("API.getBubbles: Error - %s", (error.err || error.message));
      res.status(500);
      return res.json({error: (error.err || error.message)});
    }

    //User doesnt have any strings matching the input
    if (!strings || (strings.length == 0)) {
      log.error("API.getBubbles: string [%s] not found for user: %s", req.params.string_id, JSON.stringify(req.user));
      res.status(404);
      res.json({message: "No such string available for this user."});
      return;
    }
      
    api.toJSON(strings, function(error, data) {
      if (error) {
        log.error("API.getBubbles: Error - %s", (error.err || error.message));
        return res.send(error);
      }
      log.debug("API.getBubbles: response: ", data);
      res.json(data);
    });
  });
}

Handler.prototype.addBubbleToLikeString = function(req, res) {

  log.info("API.addBubbleToLikeString: Adding the bubble [%s] to string [%s] belonging to  user: %s", req.params.bubble_id, req.params.string_id, JSON.stringify(req.user));

  var api = new LikeStringAPI(req.user, req.params.string_id);

  api.addBubble(req.params.bubble_id, function(error, data) {
    if (error) {
      log.error("API.addBubbleToLikeString: Error - %s", (error.err || error.message));
      res.status(500);
      return res.json({error: (error.err||error.message)});
    }
    log.debug("API.addBubbleToLikeString: Added the bubble [%s] to the string [%s]", req.params.bubble_id, req.params.string_id);
    res.json(data);
  });
}

Handler.prototype.deleteBubbleFromLikeString = function(req, res) {

  log.info("API.deleteBubbleFromLikeString: Deleting the bubble [%s] from string [%s] belonging to  user: %s", req.params.bubble_id, req.params.string_id, JSON.stringify(req.user));

  var api = new LikeStringAPI(req.user, req.params.string_id);

  api.removeBubble(req.params.bubble_id, function(error, data) {
    if (error) {
      log.error("API.deleteBubbleFromLikeString: Error - %s", (error.err || error.message));
      res.status(500);
      return res.json({error: (error.err || error.message)});
    }
    log.debug("API.deleteBubbleFromLikeString: Deleted the bubble [%s] from the string [%s]", req.params.bubble_id, req.params.string_id);
    if (!data) data = {};
    res.status(200);
    res.json(data);
  });
}

Handler.prototype.moveBubble = function(req, res) {
  log.info("API.moveBubble: Move the bubble [%s] from string [%s] to string [%s]", 
           req.params.bubble_id, req.params.from_string_id, req.params.to_string_id);

  if ((!req.params.from_string_id || req.params.from_string_id.isEmpty()) ||
      (!req.params.to_string_id   || req.params.to_string_id.isEmpty())   ||
      (!req.params.bubble_id      || req.params.bubble_id.isEmpty())) {
    log.error("API.moveBubble: Error - Missing parameters. Please provide FROM string, TO string and the Bubble ID to move.");
    res.status(404);
    res.json({error: "Missing parameters. Please provide FROM string, TO string and the Bubble ID to move."});
    return;
  }
  var api = new LikeStringAPI(req.user, req.params.from_string_id);
  api.moveBubble(req.params, function(error, data) {
    if (error) {
      log.error("API.moveBubble: Error - %s", (error.err || error.message));
      res.status(500);
      return res.json({error: (error.err || error.message)});
    }
    log.debug("API.moveBubble: Moved the bubble [%s] from the string [%s] to string [%s]", 
              req.params.bubble_id, req.params.from_string_id, req.params.to_string_id);
    res.status(200);
    res.json(data);
  });
}

Handler.prototype.getBubbleDetails = function(req, res) {
  var api = new BubbleAPI(req.params.bubble_id);

  log.info("API.getBubbleDetails: [%s].\n  Query params: %s", req.params.bubble_id, JSON.stringify(req.query));
  api.getDetails({slug: req.params.bubble_id}, function(error, data) {
    if (error) {
      log.error("API.getBubbleDetails: Error - %s", (error.err || error.message));
      res.status(500);
      return res.json({error: (error.err || error.message)});
    }

    if (!data) {
      log.error("API.getBubbleDetails: Bubble [%s] not found in the database", req.params.bubble_id);
      res.status(404);
      return res.json({error: "Bubble with key: [" + (req.params.bubble_id || '') + "] does not exist."});
    }

    //Some of the content might be linked to files uploaded to the server previously
    //Hence, update the links appropriately before sending it to the client.
    var serverAddress = req.protocol + "://" + (req.lsServer || req.ip) + (req.lsPort ? (":" + req.lsPort) : "");
    if (data.image && (data.image.trim().charAt(0) == '/')) {
        data.image = serverAddress + '/files/others' + data.image;
    }

    res.json(data);
  })

}

Handler.prototype.getBubbleContent = function(req, res) {
  var api = new BubbleAPI(req.params.bubble_id);

  log.debug("API.getBubbleContent: Getting contents for the Bubble: %s.\n  Query params: %s", req.params.bubble_id, JSON.stringify(req.query));
  var params = {};
  params.type = req.params.type || null;
  params.page = req.query.page || 0;
  params.count = req.query.count || 6;
  params.item_id = req.params.item_id || null;

  api.getItems(params, function(error, data) {
    if (error) {
      log.error("API.getBubbleContent: Error - %s", (error.err || error.message));
      res.status(500);
      return res.json({error: (error.err || error.message)});
    }

    //Some of the content might be linked to files uploaded to the server previously
    //Hence, update the links appropriately before sending it to the client.
    var serverAddress = req.protocol + "://" + (req.lsServer || req.ip) + (req.lsPort ? (":" + req.lsPort) : "");
    data.data.forEach(function(rec) {
      if (rec.resourceLink.trim().charAt(0) == '/') {
        rec.resourceLink = serverAddress + rec.resourceLink;
      }

      if (rec.imageLink.small && (rec.imageLink.small.trim().charAt(0) == '/')) {
        rec.imageLink.small = serverAddress + rec.imageLink.small;
        rec.imageLink.medium = rec.imageLink.large = rec.imageLink.small;
      }
    });

    //Set up the next/previous page links
    if (data && data.page) {
      log.debug("API.getBubbleContent: Page = %d", data.page);
      delete req.query.page;
      req.query.page = data.page;
      data.nextPage = apiEndPoint(req) + "?" + queryParams(req.query);
      if (data.page > 0) 
        req.query.page = data.page - 1;
      else
        delete req.query.page;
      data.prevPage = apiEndPoint(req) +"?" + queryParams(req.query);

      delete data.page;
    }

    res.json(data);
  })
}

Handler.prototype.searchBubbles = function(req, res) {
  var api = new LikeStringAPI(req.user, null /*string_id*/); //If you want to search all the bubbles in the database, set
                                                             //string_id to NULL, else only bubbles within that string are accessed.

  log.info("API.searchBubbles: Searching the database for bubbles matching the partial string [%s]", 
           req.params.query);

  api.searchBubbles({query: req.params.query}, function(error, bubbles) {
    if (error) {
      log.error("API.searchBubbles: Error - %s", (error.err || error.message));
      res.status(500);
      return res.json({error: (error.err || error.message)});
    }

    if (!bubbles || (bubbles.length == 0)) {
      log.error("API.searchBubbles: No matching bubbles found.");
      res.status(404);
      res.json({message: "No bubbles match the given search query."});
      return;
    }
    
    var serverAddress = req.protocol + "://" + (req.lsServer || req.ip) + (req.lsPort ? (":" + req.lsPort) : "");
    bubbles.forEach(function(bubble) {
      //Some of the content might be linked to files uploaded to the server previously
      //Hence, update the links appropriately before sending it to the client.
      if (bubble.image && (bubble.image.trim().charAt(0) == '/')) {
          bubble.image = serverAddress + '/files/others' + bubble.image;
      }
    });

    log.info("API.searchBubbles: Number of matching bubbles - %d", bubbles.length);
    log.debug("API.searchBubbles: Bubbles returned: [%s]", JSON.stringify(bubbles));
    res.json(bubbles);
  });
}

Handler.prototype.changeBubbleContent = function(req, res) {
  var api = new BubbleAPI(req.params.bubble_id);

  log.debug("API.changeBubbleContent: Updating contents for the Bubble: %s.", req.params.bubble_id);

  if (!req.body) {
    res.status(400);
    res.json({message: "Could not find anything to change"});
    return;
  }

  var data = req.body;
  log.debug("API.changeBubbleContent: Posted Data: %s", JSON.stringify(data));
  if (!data.delete) {
    data.delete = [];
  }

  if (!data.update) {
    data.update = [];
  }

  api.deleteItems(data.delete, function(delError, delResult) {
    api.updateItems(data.update, function(updError, updResult) {
      var error = delError || updError;
      if (error) {
        res.status(500);
        res.json({message: (error.err || error.message)});
        return;
      }

      res.status(205);
      res.json({message: "changes accepted"});
      return;
    });
  });

}

Handler.prototype.uploadFile = function(req, res) {
  var fstream;
  var uploadDir = Utils.fileUploadDirectory() + (req.query.bubble || 'others');

  req.pipe(req.busboy);
  req.busboy.on('file', function (fieldname, file, filename) {
    var fullPathName = uploadDir + "/" + filename;

    log.debug("API.uploadFile: Uploading file %s", fullPathName);
    mkdirp(uploadDir, function(error) { 
      if (error) {
        res.status(500);
        res.json({message: (error.err || error.message)});
        return;
      }

      fstream = fs.createWriteStream(fullPathName);
      file.pipe(fstream);
      fstream.on('close', function () {
        //res.redirect('back');

        //If this is a video file, then lets generate some snapshots...
        var fileType = mime.lookup(fullPathName);
        if (fileType.match(/video/i)) {
          Utils.videoScreenShot(fullPathName, uploadDir, function(error, screenShotFile) {
            if (error) {
              log.error("API.uploadFile. Unable to generate the screenshot. Error - %s", error.message);
              //Lets ignore error caused while screenshot generation for now...
            }
            res.status(200);
            res.json({file: filename});
          });

        } else {
          res.status(200);
          res.json({file: filename});
        }
      });
    });
  });
}

Handler.prototype.getUploadedFile = function(req, res) {
  if (!req.query.filename || (req.query.filename.trim().length == 0)) {
    res.status(404);
    res.json({message: 'File not found!'});
    return;
  }

  fs.readFile(Utils.linkToUploadedFiles() + req.query.filename, {encoding: 'utf-8'}, function(error,data){
    if (!error){
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write(data);
      res.end();
    }else{
      res.status(404);
      res.json({message: 'Error - ' + (error.err || error.message)});
    }

    return;

});  
  
}

Handler.prototype.addBubbleContent = function(req, res) {
  var api = new BubbleAPI(req.params.bubble_id);

  log.debug("API.addBubbleContent: Adding new contents for the Bubble: %s.", req.params.bubble_id);

  if (!req.body) {
    res.status(500);
    res.json({message: "New contents are missing - call failed! "});
    return;
  }

  var data = req.body;
  var itemArray = [];
  log.debug("API.addBubbleContent: Posted Data: %s", JSON.stringify(data));

  if (data.files && data.files.isArray) {
    var files = data.files;
    delete data.files;
    delete data.resourceLink;
    
    var item;
    files.forEach(function(file) {
      item = Utils.clone(data);
      item.resourceLink =  file;
      itemArray.push(item);
    });
  } else {
    itemArray.push(data);
  }

  log.debug("API.addBubbleContent: About to insert %d items. %s", itemArray.length, JSON.stringify(itemArray));

  var self = this;
  api.addItems(itemArray, function(error, data) {
    if (error) {
      res.status(500);
      res.json({message: "Adding new contents to bubble: " + self.slug + ". Error: " + (error.err || error.message)});
      return;
    } 

    res.status(200);
    res.json({message: "New contents added to the bubble: " + req.params.bubble_id});
  });

}

Handler.prototype.addBubble = function(req, res) {

  log.info("API.addBubble: Adding a new bubble");

  if (!req.body) {
    res.status(500);
    res.json({message: "New bubble details are missing - call failed! "});
    return;
  }

  var data = req.body;
  log.debug("API.addBubble: Posted Data: %s", JSON.stringify(data));
  
  if (data.image && (data.image.indexOf("://") < 0)) {
    //This is a local file
    data.image = "/" + data.image;
  }

  try {
    var bubble = new BubbleAPI(data.name);
    log.debug("API.addBubble: Created a JS instance of bubble", JSON.stringify(bubble));

    data.slug = bubble.slug;
    bubble.create(data, function(error, dbRec) {
      if (error) {
        log.error("API.addBubble: Could not create the object with name - [%s]", data.slug);
        log.error(error);
        res.status(500);
        res.json({message: (error.message || error.err)});
        return;
      }

      bubble.setProperties(data, function(error, rec) {
        if (error) {
          log.error("API.addBubble: Could not set the properties for the bubble [%s]", data.slug);
          log.error(error);
          res.status(500);
          res.json({message: (error.message || error.err)});
          return;
        }
     
        if (!data.sources || !data.sources.isArray) {
          log.info("API.addBubble: Added the new bubble - %s", rec.name);

          res.status(200);
          res.json({message: "New bubble created."});
          return;
        }

        if (data.sources && data.sources.isArray) {
          addDataSources(bubble, data.sources, function(error) {
            if (error) {
              log.error("API.addBubble: Could not data sources to the bubble [%s]", data.slug);
              log.error(error);
              res.status(500);
              res.json({message: (error.message || error.err)});
              return;
            }
            res.status(200);
            res.json({message: "New bubble created."});
            return;                      
          });
        }  //Are there any sources?
        else {
            res.status(200);
            res.json({message: "New bubble created."});
            return;          
        }
      });
    });
  } catch (ex) {
    log.error("API.addBubble. Exception caught!");
    log.error(ex);
    res.status(500);
    res.json({message: (ex.message || ex.err)});
    return;
  }

}

Handler.prototype.deleteBubble = function(req, res) {
  log.info("API.deleteBubble: Deleting a bubble - %s", req.params.bubble_id);

  var bubble = new BubbleAPI(req.params.bubble_id);
  bubble.remove(function(error) {
    if (error) {
      log.error("API.deleteBubble: Could not delete the bubble [%s]", req.params.bubble_id);
      log.error(error);
      res.status(500);
      res.json({message: (error.message || error.err)});
      return;
    }

    res.status(200);
    res.json({message: "Bubble deleted!"});
    return;
  });
}

Handler.prototype.modifyBubble = function(req, res) {
  log.info("API.modifyBubble: modifying a bubble's meta data");

  if (!req.body) {
    res.status(204);
    res.json({message: "Nothing to update!"});
    return;
  }

  var data = req.body;
  log.debug("API.modifyBubble: Posted Data: %s", JSON.stringify(data));
  
  if (data.image && (data.image.indexOf("://") < 0)) {
    //This is a local file
    data.image = "/" + data.image;
  }

  try {
    var bubble = new BubbleAPI(data.name);
    log.debug("API.modifyBubble: Created a JS instance of bubble", JSON.stringify(bubble));
    data.slug = bubble.slug;
    bubble.update(data, function(error, dbRec) {
      if (error) {
        log.error("API.modifyBubble: Failed to update the bubble [%s]", data.slug);
        log.error(error);
        res.status(500);
        res.json({message: (error.message || error.err)});
        return;
      }

      if (data.sources && data.sources.isArray) {
        addDataSources(bubble, data.sources, function(error) {
          if (error) {
            log.error("API.modifyBubble: Could not data sources to the bubble [%s]", data.slug);
            log.error(error);
            res.status(500);
            res.json({message: (error.message || error.err)});
            return;
          }
          res.status(200);
          res.json({message: "Changes have been saved."});
          return;               
        });
      }  //Are there any sources?
      else {
          res.status(200);
          res.json({message: "Changes have been saved"});
          return;          
      }
    });
  } catch (ex) {
    log.error("API.modifyBubble. Exception caught!");
    log.error(ex);
    res.status(500);
    res.json({message: (ex.message || ex.err)});
    return;
  }
}

Handler.prototype.refreshBubbleContent = function(req, res) {
  log.info("API.refreshBubbleContent: Refreshing the contents of the bubble - %s", req.params.bubble_id);

  var bubble = new BubbleAPI(req.params.bubble_id);
  bubble.refreshContent(function(error) {
    if (error) {
      log.error("API.refreshBubbleContent: Could not refresh the bubble [%s]", req.params.bubble_id);
      log.error(error);
      res.status(500);
      res.json({message: (error.message || error.err)});
      return;
    }

    res.status(200);
    res.json({message: "Bubble contents refreshed!"});
    return;
  });
}

Handler.prototype.getUsers = function(req, res) {
  log.info("API.getUsers: Get all the registered users");

  var fields = {"local.email": 1, 
                "facebook.email": 1, 
                "facebook.name": 1, 
                "twitter.displayName": 1, 
                "twitter.username": 1,
                "google.email": 1,
                "google.name": 1};
  UserModel.find({}, fields, function(error, users) {
    if (error) {
      log.error("API.getUsers: Database query failed.");
      log.error(error);
      res.status(500);
      res.json({message: (error.message || error.err)});
      return;
    }
    
    res.status(200);
    res.json(users);
    return;
  });

}

/*************************************************************
 * L O C A L  F U N C T I O N S 
 *************************************************************/
function queryParams(data) {
  return Object.keys(data).map(function(k) {
    return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
  }).join('&');
}

function apiEndPoint(req) {
  var url = req.originalUrl;
  return url.substr(0, url.indexOf("?"));
}

function addDataSources(bubble, sources, callback) {

  if (!sources || !sources.isArray || (sources.length == 0)) {
    return;
  }

  log.debug("addDataSources: bubble - %s, adding the data sources %s", bubble.name, JSON.stringify(sources));
          
  var q = async.queue(function(src, callback) {
    bubble.addContentSource(src, callback);
  }, 5);

  q.drain = function() {
    log.debug("addDataSources: bubble - %s, Finished adding data sources %s", bubble.name, JSON.stringify(sources));
    log.info("addDataSources: Added the new bubble - %s", bubble.name);
    callback(null);
    return;            
  }

  sources.forEach(function(src) {
    q.push(src, function(error) {
      if (error) {
        log.error("addDataSources: bubble - %s, Could not add data source - %s", bubble.name, JSON.stringify(src));
        log.error(error);
      }
    });
  });
}