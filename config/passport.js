var crypto   = require('crypto');
var log      = require('../app/log');
var config   = require('config');

// configuring the strategies for passport
var LocalStrategy    = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy  = require('passport-twitter').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var BearerStrategy = require('passport-http-bearer').Strategy;

// load up the user model
var User       		= require('../app/models/user');

// load the auth variables
var configAuth = require('./auth');

function RandomToken() {
  //return crypto.randomBytes(32).toString('base64');
  return Math.random().toString(36).slice(2);
}


// expose this function to our app using module.exports
module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

 	// =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
	// by default, if there was no name, it would just be called 'local'

    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) {

        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function() {

		  // find a user whose email is the same as the forms email
		  // we are checking to see if the user trying to login already exists
            log.info("local-signup. Finding/Creating user: %s", email);
            User.findOne({ 'local.email' :  email }, function(err, existingUser) {
                // if there are any errors, return the error
                if (err)
                    return done(err);

                // check to see if theres already a user with that email
                if (existingUser)
                    //return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    return done(new Error('That email is already taken.'));
               
                //  If we're logged in, we're connecting a new local account.
                if(req.user) {
                  var user            = req.user;
                  user.local.email    = email;
                  user.local.password = user.generateHash(password);
                  user.local.token    = RandomToken();
                  user.tokenCreationTime = new Date();
                  user.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, user);
                  });
                } else {

				// if there is no user with that email
                // create the user
                var newUser            = new User();

                // set the user's local credentials
                newUser.local.email    = email;
                newUser.local.password = newUser.generateHash(password);
                newUser.local.token = RandomToken();
                newUser.tokenCreationTime = new Date();                
				// save the user
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, newUser);
                });
                }

            });    

        });

    }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
	// we are using named strategies since we have one for login and one for signup
	// by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) { // callback with email and password from our form
		// find a user whose email is the same as the forms email
		// we are checking to see if the user trying to login already exists     
        User.findOne({ 'local.email' :  email }, function(err, user) {
            // if there are any errors, return the error before anything else
            if (err)
                return done(err);

            // if no user is found, return the message
            if (!user)
                //return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
                return done(new Error('No user found'));

			// if the user is found but the password is wrong
            if (!user.validPassword(password)) 
                //return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata
                return done(new Error('Oops! Wrong password.'));

            //Generate new token, if required.
            if (!user.local.token) {
              user.local.token    = RandomToken();
            }

            //Update the token creation time, to prevent expiry!
            user.tokenCreationTime = new Date();
            user.save(function(err) {
              if (err) throw err;
              return done(null, user);
            });
        });

    }));

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({

        // pull in our app id and secret from our auth.js file
        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },

    // facebook will send back the token and profile
    function(req, token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // check if the user is already logged in
            if (!req.user) {

                // find the user in the database based on their facebook id
                User.findOne({ 'facebook.id' : profile.id }, function(err, user) {

                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);

                    // if the user is found, then log them in
                    if (user) {
                        // if there is a user id already but no token (user was linked at one point and then removed)
                        // just add our token and profile information
                        if (!user.facebook.token) {
                            user.facebook.token = token;
                            user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                            user.facebook.email = profile.emails[0].value;
                            user.tokenCreationTime = new Date();

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });
                        }

                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user found with that facebook id, create them
                        var newUser            = new User();

                        // set all of the facebook information in our user model
                        newUser.facebook.id    = profile.id; // set the users facebook id                   
                        newUser.facebook.token = token; // we will save the token that facebook provides to the user                    
                        newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
                        newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
                        newUser.tokenCreationTime = new Date();

                        // save our user to the database
                        newUser.save(function(err) {
                            if (err)
                                throw err;

                            // if successful, return the new user
                            return done(null, newUser);
                        });
                    }

                });
            } else {
                // user already exists and is logged in, we have to link accounts
                var user            = req.user; // pull the user out of the session

                // update the current users facebook credentials
                user.facebook.id    = profile.id;
                user.facebook.token = token;
                user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                user.facebook.email = profile.emails[0].value;
                user.tokenCreationTime = new Date();

                // save the user
                user.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });
            }
        });

    }));

    // =========================================================================
    // TWITTER =================================================================
    // =========================================================================
    passport.use(new TwitterStrategy({

        consumerKey     : configAuth.twitterAuth.consumerKey,
        consumerSecret  : configAuth.twitterAuth.consumerSecret,
        callbackURL     : configAuth.twitterAuth.callbackURL,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, tokenSecret, profile, done) {

        // make the code asynchronous
    // User.findOne won't fire until we have all our data back from Twitter
        process.nextTick(function() {

            // check if the user is already logged in
            if (!req.user) {

                User.findOne({ 'twitter.id' : profile.id }, function(err, user) {

                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);

                    // if the user is found then log them in
                    if (user) {
                        // if there is a user id already but no token (user was linked at one point and then removed)
                        // just add our token and profile information
                        if (!user.twitter.token) {
                            user.twitter.token = token;
                            user.tokenCreationTime = new Date();
                            user.twitter.username  = profile.username;
                            user.twitter.displayname = profile.displayName;

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });
                        }

                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user, create them
                        var newUser                 = new User();

                        // set all of the user data that we need
                        newUser.twitter.id          = profile.id;
                        newUser.twitter.token       = token;
                        newUser.twitter.username    = profile.username;
                        newUser.twitter.displayName = profile.displayName;
                        newUser.tokenCreationTime = new Date();

                        // save our user into the database
                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });
            } else {
                // user already exists and is logged in, we have to link accounts
                var user            = req.user; // pull the user out of the session

                // update the current users facebook credentials
                user.twitter.id    = profile.id;
                user.twitter.token = token;
                user.twitter.username  = profile.username;
                user.twitter.displayName = profile.displayName;
                user.tokenCreationTime = new Date();

                // save the user
                user.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });
            }
        });

    }));

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({

        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done) {

        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(function() {

            // check if the user is already logged in
            if (!req.user) {

                // try to find the user based on their google id
                User.findOne({ 'google.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {
                        // if there is a user id already but no token (user was linked at one point and then removed)
                        // just add our token and profile information
                        if (!user.google.token) {
                            user.google.token = token;
                            user.tokenCreationTime = new Date();
                            user.google.name  = profile.displayName;
                            user.google.email = profile.emails[0].value;

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });
                        }

                        return done(null, user);
                    } else {
                        // if the user isnt in our database, create a new user
                        var newUser          = new User();

                        // set all of the relevant information
                        newUser.google.id    = profile.id;
                        newUser.google.token = token;
                        newUser.tokenCreationTime = new Date();
                        newUser.google.name  = profile.displayName;
                        newUser.google.email = profile.emails[0].value; // pull the first email

                        // save the user
                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });
            } else {
                // user already exists and is logged in, we have to link accounts
                var user            = req.user; // pull the user out of the session

                // update the current users facebook credentials
                user.google.id    = profile.id;
                user.google.token = token;
                user.google.name  = profile.displayName;
                user.google.email = profile.emails[0].value;
                user.tokenCreationTime = new Date();

                // save the user
                user.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });
            }
        });

    }));

passport.use(new BearerStrategy({ "passReqToCallback": true },
  function(req, token, done) {
    // asynchronous validation, for effect...
    process.nextTick(function () {
      
      // Find the user by token.  If there is no user with the given token, set
      // the user to `false` to indicate failure.  Otherwise, return the
      // authenticated `user`.  Note that in a production-ready application, one
      // would want to validate the token for authenticity.
      if (!token) {
        return done(new Error("Access Token parameters are missing"));
      }

      log.debug("Request Auth Check: Request: %s. Query params: %s", req.path, JSON.stringify(req.query));

      var params = {};
      switch(req.query.access_token_provider) {
        case "twitter":
          log.debug("Request Auth Check: Using the TWITTER credentials for this request");
          params['twitter.token'] = token;
          if (req.query.userid) { params['twitter.id'] = req.query.userid; }
          break;
        case "facebook":
          log.debug("Request Auth Check: Using the FACEBOOK credentials for this request");
          params['facebook.token'] = token;
          if (req.query.userid) { params['facebook.id'] = req.query.userid; }
          break;
        case "google":
          log.debug("Request Auth Check: Using the GOOGLE credentials for this request");
          params['google.token'] = token;
          if (req.query.userid) { params['google.id'] = req.query.userid; }
          break;        
        default: //local auth provider
          log.debug("Request Auth Check: Using the LOCAL credentials for this request");
          params['local.token'] = token;
          break;
      } //switch
      
      User.findOne(params, function(err, user) {
        if (err) return done(err);
        if (!user) return done(new Error("User does not exist OR User session has expired."));
        
        log.debug("Request Auth Check: Found the user record for token: %s. Record = %s", req.query.access_token, JSON.stringify(user));

        if( Math.round((Date.now()-user.created)/1000) > config.security.tokenLife) {
          log.debug("Request Auth Check: User: %s access token will be deleted", JSON.stringify(user));

          switch(req.query.access_token_provider) {
            case "twitter":
              user.twitter.token = null;
              break;
            case "facebook":
              user.facebook.token = null;
              break;
            case "google":
              user.google.token = null;
              break;
            default:
              user.local.token = null;
              break;
          }

          user.save(function(error) {
            if (error) return done(error);
            return done(new Error('Token expired')); 
          });
        } //Has the access_token expired?

        return done(null, user);
      });
    });
  }
));

};