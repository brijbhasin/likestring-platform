// server.js

// set up ======================================================================
// get all the tools we need
var fs = require('fs');
var express  = require('express');
var app      = express();
var config   = require('config');
var port     = process.env.PORT || config.Server.port;
var mongoose = require('mongoose');
var passport = require('passport');
var flash 	 = require('connect-flash');
var cors     = require('cors')

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var busboy       = require('connect-busboy');

var log = require('./app/log');
//Controllers
var PageHandler           = require('./app/page_routes_handler');
var AuthenticationHandler = require('./app/authentication_routes_handler');
var ApiHandler            = require('./app/api_routes_handler');

var shutting_down = false;

// configuration ===============================================================
var securityOptions = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('certificate.pem'),
    requestCert: true
};
var server        = require('https').createServer(securityOptions, app);

mongoose.connect(config.Database.url);
var db = mongoose.connection;

db.on('error', function (err) {
    log.error('connection error:', err.message);
});

db.once('open', function callback () {
    log.info("Connected to Database: %s", config.Database.url);
});

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
//app.use(morgan('dev')); // log every request to the console
app.use(morgan({ "stream": log.stream }));
app.use(cookieParser()); // read cookies (needed for auth)
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
app.use(busboy()); 

app.set('view engine', 'ejs'); // set up ejs for templating
app.use(express.static(__dirname + '/public'));

// required for passport
app.use(session({ secret: 'likestringisgoingtokickbutt' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(cors());
// ------------------------------------------------------
// helper functions for auth

// .............................................
// true if req is a login/signup request

function isLoginRequest (req) {
    if ((req.path !== '/') && !req.path.match(/\/login|logout|auth|signup|\.ico\/|\.png\//)) { log.info("Request Path: %s", req.path);  return false; }
    if (( req.method != "GET" ) && (req.method != "POST")) { return false; }
    return true;
} // ()

// .............................................
// your auth policy  here:
// true if req does have permissions
// (you may check here permissions and roles 
//  allowed to access the REST action depending
//  on the URI being accessed)

function reqHasPermission (req) {
    // decode req.accessToken, extract 
    // supposed fields there: userId:roleId:expiryTime
    // and check them

    // Support sessions/cookies, if available.
    // Logged in users are serialized/deserialized to the session by the passport module
    //
    if (req.user) {

      if (!req.query.access_token && !req.query_acces_token_provider)
        req.query.access_token = req.user.local.token;
      return true;
    }

    if (!req.query.access_token)
      return false;
    //FIXME: Ensure the auth token is encoded base64 string
    return true;
} // ()

// ------------------------------------------------------
// install a function to transparently perform the auth check
// of incoming request, BEFORE they are actually invoked

app.use (function(req, res, next) {
    if (! isLoginRequest (req) ) {
        if (! reqHasPermission (req) ){
            res.status(401);  //unauthorized
            res.send({ error: 'Access token missing - unauthorized access. Please login first.' });
            return; // don't call next()
        }
    } else {
        log.info (" * is a login request ");
    }

    //Hack!!! 
    //We have to store the server ip and port in order to prefix the hyperlink paths for all the
    //files have been uploaded manually by the user (for a bubble).
    //Cant seem to get the port by any other means and hence this ugly hack!
    req.lsServer = (server.address ? server.address().address : req.ip);
    if (req.lsServer && (req.lsServer.trim() === "0.0.0.0")) {
      req.lsServer = "localhost";
    }
    req.lsPort = (server.address ? server.address().port  : port);

    next(); // continue processing the request
});


// routes ======================================================================
var handlers = {
  page: new PageHandler(),
  passport: passport,
  auth: new AuthenticationHandler(), //Authentication handler
  api: new ApiHandler()
};

require('./app/routes.js')(app, handlers); // load our routes and pass in our app and fully configured passport

// Process cleanup
app.use(function(req, res, next){
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({ error: 'Not found' });
    return;
});

app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({ error: err.message });
    return;
});


app.use(function (req, resp, next) {
 if(!shutting_down)
   return next();

 resp.setHeader('Connection', "close");
 resp.send(503, "Server is in the process of restarting");
});

process.on('uncaughtException', function (err) {
  log.error('uncaughtException:', err.message)
  log.error(err.stack.trim());
  process.exit(1);
});

app.start = function() {
  server.listen(port, function() {
    log.info("Server listening on port " + port);
  });
};

app.stop = function() {
  shutdown();
};

function shutdown () {
  shutting_down = true;
  if (!server) {
    process.exit();
  } else {

    try {
      server.close( function (err) {
        if (err) {
          log.error(err);
          return;
        }
        log.info( "APP Shutdown: Closed out remaining connections.");
        // Close db connections, other chores, etc.
        process.exit();
      });
    } catch(error) {
      process.exit();
    }
  }

  setTimeout( function () {
   log.error("APP Shutdown: Could not close connections in time, forcing shut down");
   process.exit(1);
  }, 10*1000);

}

process.on('SIGINT', shutdown);
process.on('SIGTERM', shutdown);

// launch ======================================================================
module.exports = app;
if (!module.parent) app.start();
