// force the test environment to 'test'
process.env.NODE_ENV = 'test';
//process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

var config = require('config');
var should = require('should');
var api = require('supertest');

var app = require('../../server');
var logger = require('../../app/log');
var User = require("../../app/models/user.js");

logger.transports.console.level = 'error'; //This could be any of these values: silly/debug/verbose/info/warn/error
var testUserEMAIL = "tester@example.com";

describe('User signups', function() {

  before(function() {
    User.remove({"local.email": testUserEMAIL}, function() {});
    api = api(app); //this starts the server, connects to database, etc...
  });

  after(function(done) {
    User.remove({"local.email": testUserEMAIL}, done);
  });

  //Need to set this timeout, else mocha might fail with a timeout error!
  this.timeout(15000);
 
 
  it('should show signup form', function(done) {
    api.get('/signup')
      .expect(200)
      .expect('Content-Type', /html/)
      .expect(/<form action=\"\/signup\" method=\"post\">/)
      .expect(/<input type=\"text\" class=\"form-control\" name=\"email\">/)
      .expect(/<input type=\"password\" class=\"form-control\" name="password\">/, done);
  });

  it('should refuse empty submissions', function(done) {
    api.post('/signup')
      .send({email: '', password: ''})
      .expect(500)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.error.should.exist;
        done();
      });
  });

  it('should accept complete submissions', function(done) {
    api.post('/signup')
      .send({email: testUserEMAIL, password: 'itsasecret1234'})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.access_token.should.exist;
        User.findOne({ "local.email": testUserEMAIL}, function(err, user) {
          user.local.email.should.eql(testUserEMAIL);
          user.local.token.should.eql(res.body.access_token);
          done();
        });
      });
  });

  it('should not accept duplicate email', function(done) {
    var localUser = new User({local: {email: testUserEMAIL, password: "itsasecret1234" }});
    localUser.save(function(error) {
      if (error) done('error' + error.message);
    });

    api.post('/signup')
      .send({email: testUserEMAIL, password: 'itsasecret1234'})
      .expect(500)
      .expect('Content-Type', /json/)
      .expect(/email is already taken/, done);
  });

}); /* End of User signups */
