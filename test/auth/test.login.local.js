// force the test environment to 'test'
process.env.NODE_ENV = 'test';
var config = require('config');
var should = require('should');
var api = require('supertest');
var ObjectId = require('mongodb').ObjectID;

var app = require('../../server');
var logger = require('../../app/log');
var User = require("../../app/models/user.js");

logger.transports.console.level = 'error'; //This could be any of these values: silly/debug/verbose/info/warn/error
var testUserEMAIL = "tester@example.com";

describe('login with local credentials', function() {

  before(function(done) {
    User.remove({"local.email": testUserEMAIL}, function() {});
    api = api(app); //this starts the server, connects to database, etc...

    //Add the test account to be used for the tests
    api.post('/signup')
      .send({email: testUserEMAIL, password: 'itsasecret1234'})
      .expect(200)
      .expect('Content-Type', /json/, done);
  });

  after(function(done) {
    User.remove({"local.email": testUserEMAIL}, done);
  });

  //Need to set this timeout, else mocha might fail with a timeout error!
  this.timeout(15000);
  
  it('should show the login page', function(done) {
    api.get('/auth/local')
      .expect(200)
      .expect('Content-Type', /html/)
      .expect(/<form action=\"\/auth\/local\" method=\"post\">/)
      .expect(/<input type=\"text\" class=\"form-control\" name=\"email\">/)
      .expect(/<input type=\"password\" class=\"form-control\" name="password\">/, done);
  });

  it('should refuse empty submissions', function(done) {
    api.post('/auth/local')
      .send({email: '', password: ''})
      .expect(404)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.error.should.exist;
        done();
      });
  });

  it('should reject unregistered user', function(done) {
    api.post('/auth/local')
      .send({email: "fake_tester@example.com", password: 'itsasecret1234'})
      .expect(500)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.error.should.exist;
        done();
      });
  });
 
  it('should reject if the password is invalid', function(done) {
    api.post('/auth/local')
      .send({email: testUserEMAIL, password: 'foobar'})
      .expect(500)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.error.should.exist;
        done();
      });
  });

  it('should login user successfully', function(done) {
    api.post('/auth/local')
      .send({email: testUserEMAIL, password: 'itsasecret1234'})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.access_token.should.exist;
        (res.body.access_token !== null).should.be.true;
        done();
      });
  });

  it('should refresh the access token, if it is expired and login user successfully', function(done) {
    //Expire the user session, by manually setting the token to null (in the actual program a timer will
    //do this after a predefine time interval)
    User.findOne({"local.email" : testUserEMAIL}, function(error, user) {
      if (error) return done(error);
      user.local.token = null;
      user.save(function(error, updatedUser) {
        if (error) return done(error);
        (updatedUser.local.token === null).should.be.true;

        api.post('/auth/local')
          .send({email: testUserEMAIL, password: 'itsasecret1234'})
          .expect(200)
          .expect('Content-Type', /json/)
          .end(function(err, res) {
            if (err) return done(err);
            res.body.access_token.should.exist;
            (res.body.access_token !== null).should.be.true;
            done();
          });
      })
    });
  });

  it('should logout user successfully', function(done) {
    
    var access_token = null;
    //First do a login to get the access_token
    api.post('/auth/local')
      .send({email: testUserEMAIL, password: 'itsasecret1234'})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
          if (err) return done(err);
          res.body.access_token.should.exist;
          (res.body.access_token !== null).should.be.true;
          access_token = res.body.access_token;

          //Now attempt a logout.
          api.post('/auth/logout')
            .send({access_token: encodeURI(access_token)})
            .expect(200)
            .expect(/Logout successful/)
            .end(function(err, res) {
               if (err) return done(err);
               User.findOne({"local.email" : testUserEMAIL}, function(error, user) {
                 if (error) return done(error);
                 (user.local.token === null).should.be.true;
                 done();
               });
        });
    }); //Login POST()
  });

  it('should return error if logout is called on an expired session or user hasnt logged in yet', function(done) {
    
    var access_token = null;
    //First do a login to get the access_token
    api.post('/auth/local')
      .send({email: testUserEMAIL, password: 'itsasecret1234'})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.access_token.should.exist;
        (res.body.access_token !== null).should.be.true;
        access_token = res.body.access_token;

        //Now logout cleanly!
        api.post('/auth/logout')
          .send({access_token: encodeURI(access_token)})
          .expect(200)
          .expect(/Logout successful/)
          .end(function(err, res) {
              if (err) return done(err);
              //Try logging out again and this time there should be an error!
              api.post('/auth/logout')
                .send({access_token: encodeURI(access_token)})
                .expect(401)
                .expect(/session has expired/, done);
                });
    }); //Login POST()
  });

  it('should check if the user has logged in or not', function(done) {
    
    var access_token = null;
    //First do a login to get the access_token
    api.post('/auth/local')
      .send({email: testUserEMAIL, password: 'itsasecret1234'})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.access_token.should.exist;
        (res.body.access_token !== null).should.be.true;
        access_token = res.body.access_token;

        api.get('/auth/isloggedin')
          .query({access_token: encodeURI(access_token)})
          .expect(200)
          .expect(/true/, done);
    }); //Login POST()
  });

});
