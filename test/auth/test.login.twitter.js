// force the test environment to 'test'
process.env.NODE_ENV = 'test';
var config = require('config');
var should = require('should');
var api = require('supertest');

var app = require('../../server');
var logger = require('../../app/log');
var User = require("../../app/models/user.js");

logger.transports.console.level = 'error'; //This could be any of these values: silly/debug/verbose/info/warn/error
var twitterUser = "likeStringTweet";

describe('login with Twitter credentials', function() {

  before(function(done) {
    done();
  });

  after(function(done) {
    done();
  });

  //Need to set this timeout, else mocha might fail with a timeout error!
  this.timeout(15000);

  it('should create a new user record and populate with twitter credentials');
  it('should return an error if twitter verification fails');
  it('should be able to make a successful Twitter API call with the new credentials');
  it('should ')
});
