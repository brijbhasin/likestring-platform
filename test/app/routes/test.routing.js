process.env.NODE_ENV = 'test';
var config = require('config');
var should = require("should");
var request = require('supertest');  

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var app = require('../../../server'); 
var User = require("../../../app/models/user");
var Api = require('../../../data/serve/likestring');
var likeStringModel = require('../../../app/models/likestring');


describe('Routing', function() {

  var url = "http://localhost:" + (process.env.PORT || config.Server.port);
   
  before(function(done) {
  	app.start();
    done();
  });

  describe('Pages', function() {
    it ('should render the home page', function(done) {
      request(url)
        .get('/')
        .expect(200)
        .end(function(err, res) {
           if (err) return done(err);
           done();
        });
    });

    it ('should render the local account login page', function(done) {
      request(url)
        .get('/login')
        .expect(200)
        .end(function(err, res) {
           if (err) return done(err);
           done();        	
        });
    });

    it ('should render the signup page', function(done) {
      request(url)
        .get('/signup')
        .expect(200)
        .end(function(err, res) {
           if (err) return done(err);
           done();        	
        });
    });

    it ('should redirect user to login page when trying to access the profile without logging in', function(done) {
      request(url)
        .get('/profile')
        .expect('Content-Type', /text/)
        .expect(302)
        .end(function(err, res) {
           if (err) return done(err);
           done();
        });
    });
  });

  describe('REST APIs', function() {
  	var user = null;
  	var api = null;
    //Create our 'test' local user before running the tests...
    before(function(done) {
      User.remove({"local.email" : "tester@example.com"}, function(error) {});      
      user = new User({"local.email": "tester@example.com", "local.password": "test1234"});
      user.local.password = user.generateHash("test1234");
      user.save(function(error, rec) {
        if (error) done(error);
        api = new Api(user);
        done();
      });
    });

    after(function(done) {
      likeStringModel.remove({user: user}, function(error) {});
      User.remove({"local.email" : "tester@example.com"}, function(error) {});
      done();
    });

    it ('should log in the local user', function(done) {
      var params = {
      	email: user.local.email,
      	password: user.local.password
      };
      request(url)
        .post('/login')
        .type('form')
        .send(params)
        .expect(302) //redirect to the profile page
        .end(function(err, res) {
           if (err) return done(err);
           done();
        });
    });

    it ('should return the array of likeStrings for the user logged in', function(done) {
      request(url + "/v1/api/")
        .get('strings')
        .end(function(err, res) {
           if (err) return done(err);
           console.log(res);
           done();
        })
    });

    it ('should create a new likeString');
    it ('should throw an error for a duplicate likeString');
    it ('should delete the current likeString for the logged in user');
    it ('should delete the likeString matching the given name, for the logged in user');
    it ('should change the name of the current/selected likeString');
    it ('should add a bubble to the current/selected likeString');
    it ('should remove an existing bubble from the current/selected likeString ')
  });


});