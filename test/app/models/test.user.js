process.env.NODE_ENV = 'test';
var config = require('config');
var http = require('http');
var assert = require('assert');
var should = require("should");

// get the application server module
var app = require('../../server');
var User = require("../../app/models/user.js");

describe('User Model', function() {

before(function(done) {
  done();
});

after(function(done) {
  done();
});

beforeEach(function(done) {
  var localUser = new User({
     local: {email: "tester@example.com", password: "test1234" }
  });

  localUser.save(function(error) {
    if (error) console.log('error' + error.message);
    done();
  });
});

it('find a local user by email', function(done) {
    User.findOne({ "local.email": "tester@example.com"}, function(err, user) {
      user.local.email.should.eql("tester@example.com");
      console.log("   email: ", user.local.email)
      done();
    });
 });

 afterEach(function(done) {
    /* User.remove({}, function() {
      done();
    });*/ done();
 });

});
