process.env.NODE_ENV = 'test';
var config = require('config');
var should = require("should");
var api = require('supertest');
var ObjectId = require('mongodb').ObjectID;
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// get the application server module
var app = require('../../../server');
var User = require('../../../app/models/user');
var Utils = require('../../../app/models/utils');
var Category = require('../../../app/models/bubble_category')
var BubbleModelSchema = require('../../../app/models/bubble'),
    BubbleModel = BubbleModelSchema.Bubble,
    BubbleContentModel = BubbleModelSchema.BubbleContent;

var testUserEMAIL = "tester@example.com";

describe('Bubble Model', function() {

  var user = null;
  var access_token = null;
  var category = null;

  before(function(done) {
      User.remove({"local.email": testUserEMAIL}, function() {});
      BubbleModel.remove({slug: 'test-bubble'}, function() {});
      Category.remove({slug: 'test'}, function() {});

      api = api(app); //this starts the server, connects to database, etc...

      //Add the test account to be used for the tests
      api.post('/signup')
        .send({email: testUserEMAIL, password: 'itsasecret1234'})
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) return done(err);
          res.body.access_token.should.exist;
          access_token = res.body.access_token;

          User.findOne({"local.email": testUserEMAIL}, function(error, data) {
            if (error) return done(error);
            user = data;
      
            var newCat = new Category( { name: "Test", slug: "test", parent: null});
            newCat.save(function(error, data) {
              if (error) done(error);
              category = data;
              done();
            });
          }); //Users.findOne()
        }); //Create new test user via POST signup
  });


  after(function(done) {
    User.remove({"local.email": testUserEMAIL}, function() {});
    BubbleModel.remove({slug: 'test-bubble'}, function() {});
    Category.remove({slug: 'test'}, done);    
  });


  // Bubble CRUD operations
  it('should create a new bubble', function(done) {

    var newBubble = new BubbleModel();
    newBubble.name = "Test Bubble";
    newBubble.slug = Utils.createSlug(newBubble.name);
    newBubble.description = "Bubble used for testing out the bubble database model.";
    newBubble.category = category;
    newBubble.admin_user = user;
    contents = [];

    newBubble.save(function(error) {
      if (error) done(error);
      BubbleModel.findOne({slug: Utils.createSlug(newBubble.name)}, function(error, bubble) {
        if (error) done(error);
        bubble.should.exist;
        bubble.name.should.equal("Test Bubble");
        BubbleModel.remove({slug: bubble.slug}, done);
      });
    });
  });

  it('should throw an throw an error while creating a duplicate bubble', function(done) {

    var newBubble = new BubbleModel();
    newBubble.name = "Duplicate Bubble";
    newBubble.slug = Utils.createSlug(newBubble.name);
    newBubble.save(function(error) {
      if (error) done(error);

      var duplicate = new BubbleModel();
      duplicate.name = newBubble.name;
      duplicate.slug = newBubble.slug;
      duplicate.save(function(error) { 
        error.should.exist;
        error.err.should.match(/duplicate key/);
        BubbleModel.remove({slug: newBubble.slug}, done);
      });
    });

  });

  it('should modify an existing bubble', function(done) {
    var newBubble = new BubbleModel();
    newBubble.name = "Test Bubble";
    newBubble.slug = Utils.createSlug(newBubble.name);
    newBubble.description = "Bubble used for testing out the bubble database model.";
    newBubble.category = category;
    newBubble.admin_user = user;
    contents = [];

    newBubble.save(function(error) {
      if (error) done(error);
      BubbleModel.findOne({slug: Utils.createSlug(newBubble.name)}, function(error, bubble) {
        if (error) done(error);
        bubble.should.exist;
        bubble.update({slug: bubble.slug}, {$set: {description: "Modified description"}}, function(error, updateCount) {
          if (error) done(error);
          updateCount.should.equal(1);
          BubbleModel.remove({slug: bubble.slug}, done);
        });
      });
    });
  });

  it('should delete the bubble and all its contents', function(done) {
    var newBubble = new BubbleModel();
    newBubble.name = "Test Bubble";
    newBubble.slug = Utils.createSlug(newBubble.name);
    newBubble.description = "Bubble used for testing out the bubble database model.";
    newBubble.category = category;
    newBubble.admin_user = user;
    contents = [];

    newBubble.save(function(error) {
      if (error) done(error);
      BubbleModel.findOne({slug: Utils.createSlug(newBubble.name)}, function(error, bubble) {
        if (error) done(error);
        bubble.should.exist;
        BubbleModel.remove({slug: bubble.slug}, function(error, deleteCount) {
          if (error) done(error);
          deleteCount.should.equal(1);
          done(); 
        });
      });
    });
  });

  it('should only allow the bubble administrator to change the bubble contents');
  it('should throw an error if a bubble content type is not a permissible one');
});
