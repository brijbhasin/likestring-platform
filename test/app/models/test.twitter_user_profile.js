process.env.NODE_ENV = 'test';
var config = require('config');
var http = require('http');
var assert = require('assert');
var should = require("should");

// get the application server module
var app = require('../../server');
var TwitterUserProfile = require("../../app/models/twitter_user_profile.js");

describe('Twitter User Profile Model', function() {

before(function(done) {
  done();
});

after(function(done) {
  TwitterUserProfile.remove({}, function(error) {
    if (error) done(error);
  });
  done();
});

beforeEach(function(done) {
  var user = new TwitterUserProfile({
     id: 12345,
     friends: [],
     raw : { 
             id : 12345,
             screen_name: 'test_user',
             text: 'This would be the actual JSON document returned by twitter'
           }
  });

  user.save(function(error) {
    if (error) console.log('error' + error.message);
    done();
   });
});

it('find a profile by twitter ID', function(done) {
    TwitterUserProfile.findOne({ "id": 12345 }, function(err, user) {
      user.raw.screen_name.should.eql("test_user");
      done();
    });
});

it('show throw error when adding a profile with duplicate twitter ID', function(done) {
    var dupUser = new TwitterUserProfile({
      id: 12345,
      friends: [],
      raw : {}
    });

    //(function() { throw new Error('duplicate key error');}).should.throw('duplicate key error');
    dupUser.save(function(error) {
      should.exist(error);
    });
    done();
});


});
