// force the test environment to 'test'
process.env.NODE_ENV = 'test';
var config = require('config');
var http = require('http');
var assert = require('assert');
var should = require("should");

// get the application server module
var app = require('../../../server');
var User = require("../../../app/models/user.js");
var twitter = require("../../../data/capture/twitter.js");

describe('Fetch Twitter Data', function() {
  
  var server;
  var Twitter = new twitter();

  before(function() {
    server = http.createServer(app).listen(config.Server.port);
    // initialize the browser using the same port as the test application
  });
 
  after(function(done) {
    server.close(done);
  });

  it('should return the complete profile of authenticated user', function(done) {
    Twitter.verifyCredentials(function(err, data) {
      if (err) return done(err);
      data.screen_name.should.not.be.empty;
      data.profile_background_image_url.should.be.ok;
      done();
    });
  });

  it('should throw an error when trying to get a user profile without sending screen_name/user_id', function(done) {
    (function(){
      Twitter.getUserProfile(function(err, data) {
        if (err) throw err;
      });
    }).should.throw();
    done();
  });

  it('should return the profile of a given screen_name', function(done) {
    Twitter.getUserProfile('twitterapi', function(err, data) {
      data.screen_name.should.be.equal('twitterapi');
      done();
    });
  });

  it('should return the profile of a given User ID', function(done) {
    Twitter.getUserProfile(6253282, function(err, data) {
      data.id.should.be.equal(6253282);
      done();
    });
  });

  it('should get the friend profiles for a given user', function(done) {
    Twitter.getFriends('twitterapi', function(err, data) {
      data.length.should.be.above(0);
      data[0].screen_name.should.exist;
      done();
    });
  });

  it('should throw an error when calling search without a pattern', function(done) {
    (function(){
      Twitter.search(function(err, data) {
        if (err) throw err;
      });
    }).should.throw();
    done();
  });

  it('should return the tweets matching a search pattern', function(done) {
    Twitter.search('-RT #i7nw', function(err, data) {
      (err === null).should.be.true;
      data.should.exist;
      console.log("Tweets#: " + data.length);
      console.log(data[0]);
      done();
    });
  });

  it('should throw an error when trying to get tweets without providing a user id', function(done) {
    (function(){
      Twitter.tweets(function(err, data) {
        if (err) throw err;
      });
    }).should.throw();
    done();
  });

  it('should get all the tweets for a user if the optional parameters are not given', function(done) {
    Twitter.tweets('dummy', function(err, data) {
      if (err) throw err;
      data.should.be.ok;
      done();
    });
  });

  it('should get filtered tweets for a user', function(done) {
    Twitter.tweets('twitterapi', {count: 5, include_rts: false}, function(err, data) {
      if (err) throw err;
      data.length.should.be.above(0);
      done();
    });
  });

});