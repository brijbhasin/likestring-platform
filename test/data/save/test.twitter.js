// force the test environment to 'test'
process.env.NODE_ENV = 'test';
var config = require('config');
var http = require('http');
var assert = require('assert');
var should = require("should");
var mongoose = require('mongoose');
var async = require('async');

// get the application server module
var app = require('../../../server');
var TwitterUserProfile = require("../../../app/models/twitter_user_profile.js");
var twitterClient = require("../../../data/capture/twitter.js");
var twitterStore = require("../../../data/save/twitter.js");

//var twitterHandle = 'gsfindia';
//var twitterHandle = 'robinwilliams';
var twitterHandle = 'pmarca';

describe('Save the fetched Twitter data: ', function() {
  
  var browser, server;
  var TwitterClient = new twitterClient();
  var TwitterStore;
  var twitterProfile;

  before(function() {
    server = http.createServer(app).listen(config.Server.port);
    TwitterStore = new twitterStore();
  });
 
  beforeEach(function(done) {
    TwitterClient.getUserProfile(twitterHandle, function(err, user) {
      if (err) return done(err);
      twitterProfile = user;
      done();
    });
  });

  after(function(done) {
    //Delete the residual Twitter profiles that were saved while executing the test cases
    /*TwitterUserProfile.remove({}, function(e, d) {
      if (e) done(e);
    });*/
    server.close(done);
  });

  it('should have a valid database connection', function(done) {
    // 1 - connected
    // 2 - connecting
    mongoose.connection.readyState.should.be.within(1,2);
    done();
  });

  it('should save the extracted user profile to the database', function(done) {
    TwitterStore.saveUser(twitterProfile, function(error, user) {
      if (error) return done(error);
      TwitterUserProfile.find({id: user.id}, function(error, savedTwitterProfile) {
        if (error) done(error);
        savedTwitterProfile.should.exist;
      });
      done();
    });
  });

  it('should update user profile if it already exists in the database', function(done) {

    async.series([
      //Remove the profile from our database, if it exists
      function(callback) {
        TwitterUserProfile.remove({"raw.screen_name" : twitterProfile.screen_name}, function(error, r) {
          if (error) return callback(error);
          callback(null, null);
        });
      },

      // Create the fresh twitter profile database record
      function(callback) {
        TwitterStore.saveUser(twitterProfile, function(error, d) {
          if (error) return callback(error);
          callback(null, null);
        });
      },

      //Try changing some user field and saving again.
      function(callback) {
        twitterProfile.name = "New Name for Twitter API";
        TwitterStore.saveUser(twitterProfile, function(error, rec) {
          if (error) callback(error);

          //ACTUAL TEST - Is the existing database record updated with information?
          rec.raw.name.should.equal(twitterProfile.name);
          callback(null, null);              
        });
      }
      ], // End of series of functions

      function(error, results) {
        if (error) done(error);
        done();
      });
  });

  it('should update an existing user profile with a list of friend IDs', function(done) {
    TwitterClient.getFriends(twitterProfile.id, function(error, friends) {
      if (error) return done(error);

      // PLEASE NOTE: the number of friend profiles returned could be less than the 
      // "friends_count". Maybe some of the friend IDs are disabled by twitter.

      TwitterStore.saveFriends(twitterProfile, friends, function(error, friendIDs) {
        if (error) return done(error);
        friendIDs.length.should.be.equal(friends.length);

        TwitterUserProfile.findOne({id: twitterProfile.id}, {_id: 0, friends: 1}, function(error, rec) {
          if (error) return done(error);
          rec.friends.length.should.be.equal(friends.length);
          done();
        });
      });
    });
  });

  it('should create a user profile document, if it doesnt exist, while saving the friend IDs', function(done) {

    var twitterFriends;
    async.series([
      //Remove the profile from our database, if it exists
      function(callback) {
        TwitterUserProfile.remove({"raw.screen_name" : twitterProfile.screen_name}, function(error, r) {
          if (error) return callback(error);
          callback(null, null);
        });
      },

      //Get the friends for the test twitter profile
      function(callback) {
        TwitterClient.getFriends(twitterProfile.id, function(error, friends) {
          if (error) return callback(error);
          twitterFriends = friends;
          callback(null, null);
        })
      },

      //Save the extracted friends to the database
      function(callback) {
        TwitterStore.saveFriends(twitterProfile, twitterFriends, function(error, friendIDs) {
          if (error) return callback(error);
          friendIDs.length.should.be.equal(twitterFriends.length);
          callback(null, null);
        });
      },

      //User record should get created and its friend list updated!
      function(callback) {
        TwitterUserProfile.findOne({id: twitterProfile.id}, {_id: 0, id: 1, friends: 1}, function(error, rec) {
          if (error) callback(error);
          rec.should.exist; // 
          rec.friends.length.should.be.equal(twitterFriends.length);
          callback(null, null);
        });
      }
    ],
    function(error, results) {
      if (error) done(error);
      done();
    });
  });

  it('should get all the tweets for a given twitter handle and save it to the database', function(done) {
    TwitterClient.tweets(twitterHandle, {include_rts: true}, function(error, data) {
      if (error) done(error);
      TwitterStore.saveTweets(data, function(error, result) {
        if (error) done(error);
        result.should.exist;
        done();
      })
    });    
  });

  it('should save the tweet search results to the database', function(done) {
    TwitterClient.search('-RT @'+twitterHandle, function(error, data) {
      if (error) done(error);
      TwitterStore.saveTweets(data, function(error, result) {
        if (error) done(error);
        result.should.exist;
        done();
      })
    });    
  });


});