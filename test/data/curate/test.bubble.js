process.env.NODE_ENV = 'test';
var config = require('config');
var should = require("should");
var api = require('supertest');
var ObjectId = require('mongodb').ObjectID;
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// get the application server module
var app = require('../../../server');
var User = require('../../../app/models/user');
var Utils = require('../../../app/models/utils');
var Category = require('../../../app/models/bubble_category')
var BubbleModelSchema = require('../../../app/models/bubble'),
    BubbleModel = BubbleModelSchema.Bubble,
    BubbleContentModel = BubbleModelSchema.BubbleContent;
var logger = require('../../../app/log');

logger.transports.console.level = 'debug'; //This could be any of these values: silly/debug/verbose/info/warn/error
var testUserEMAIL = "tester@example.com";
var apiPrefix = '/v1/api/';
var bubbleNAME = "gsfindia";

describe('likeString Bubbles', function() {
  var user = null;
  var likeString = null;
  var access_token = null;

  before(function(done) {
    User.remove({"local.email": testUserEMAIL}, function() {});
    BubbleModel.remove({slug: Utils.createSlug(bubbleNAME)}, function() {});

    api = api(app); //this starts the server, connects to database, etc...

    //Add the test account to be used for the tests
    api.post('/signup')
      .send({email: testUserEMAIL, password: 'itsasecret1234'})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.access_token.should.exist;
        access_token = res.body.access_token;

        User.findOne({"local.email": testUserEMAIL}, function(error, data) {
          if (error) return done(error);
          user = data;
          done();
        }); //Users.findOne()
      }); //Create new test user via POST signup
  });

  after(function(done) {
    BubbleModel.remove({slug: Utils.createSlug(bubbleNAME)}, function() {});
    done();//User.remove({_id: new ObjectId(user.id)}, done);
  });

  describe(' Fill Bubble Contents', function() {

    it('should fill the bubble contents using a given twitter handle');
    it('should fill the bubble contents using a given twitter search string');
    it('should fill the bubble contents using a given youtube handle');

  });

  describe(' Enumeration and Meta data', function() {
    it('should return a list of available bubbles');
    it('should return a list of bubbles matching a search criteria');
    it('should return the next page of the available bubbles');
    it('should return the previous page of the available bubbles');
    it('should return the meta data for a given bubble', function(done) {
      api.get(apiPrefix + 'bubble/' + Utils.createSlug(bubbleNAME))
         .expect(200)
         .expect('Content-Type', /json/, done);
    });
  });

  describe(' Serve contents of a bubble', function() {
    it('should return latest content');
    it('should return latest photos');
    it('should return latest tweets');
    it('should return latest news/articles');
    it('should return latest videos');

    it('should return the next page of the photos');
    it('should return the previous page of the photos');
    it('should return the selected item');

  });

});


/*

  before(function(done) {
    this.timeout(45000);
    BubbleDocument.remove({slug: Utils.createSlug("GSFIndia")}, function(err, data) {});

    bubble = new Bubble("GSFIndia");

    var params = {handle: 'GSFIndia'};
    var t = new TwitterSource(params);
    curatedbubble = new CuratedBubble("GSFIndia");
    curatedbubble.create({}, function(error, data) {
      if (error) return done(error);
      curatedbubble.addContentSource(t, function(error, data) {
        if (error) return done(error);
        curatedbubble.refreshContents(function(error, data) {
          if (error) return done(error);
          done();
        });
      });
    });
  });

  after(function(done) {
    done();
  });

  it('should list all the existing bubbles');
  it('should display the bubbles matching a search term');
  it('should return the meta data for a given bubble');

  it('should return the most recent contents for a given bubble', function(done) {
    bubble.getContent(function(error, data) {
      should.not.exist(error);
      data.should.be.ok;
      done();
    });
  });

  it('should return the latest photos for a given bubble', function(done) {
    bubble.getContent({type: "photo"}, function(error, photos) {
      should.not.exist(error);
      photos.should.be.ok;
      photos.data.forEach(function(photo) {
        if (photo.contents.type !== "photo") return done(new Error("Invalid data returned"));
      });
      done();
    });
  });

  it('should return the next page of the bubble\'s photo collection', function(done) {
    bubble.getContent({type: "photo", page: 1}, function(error, photos) {
      should.not.exist(error);
      photos.should.be.ok;
      photos.data.forEach(function(photo) {
        if (photo.contents.type !== "photo") return done(new Error("Invalid data returned"));
      });
      done();
    });
  });

  it('should return the previous page of the bubble\'s photo collection', function(done) {
    //Note: Page 0 is the first page returned
    bubble.getContent({type: "photo", page: 0}, function(error, photos) {
      should.not.exist(error);
      photos.should.be.ok;
      photos.data.forEach(function(photo) {
        if (photo.contents.type !== "photo") return done(new Error("Invalid data returned"));
      });
      done();
    });
  });

  //FIXME: We might want to add test cases for videos and other content types if they are handled differently from photos.
  // For now, only photos is enough to test the paging implementation.
});
*/