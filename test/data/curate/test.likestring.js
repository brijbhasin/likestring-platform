process.env.NODE_ENV = 'test';
var config = require('config');
var should = require("should");
var api = require('supertest');
var ObjectId = require('mongodb').ObjectID;
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var app = require('../../../server'); //Need this for database connects

var User = require("../../../app/models/user");
var LikeString = require('../../../data/curate/likestring');
var likeStringModel = require('../../../app/models/likestring');
var BubbleModel = require('../../../app/models/bubble').Bubble;
var Utils  = require('../../../app/models/utils');
var logger = require('../../../app/log');


logger.transports.console.level = 'error'; //This could be any of these values: silly/debug/verbose/info/warn/error
var testUserEMAIL = "tester@example.com";
var apiPrefix = '/v1/api/';


describe('LikeStrings', function() {

  var user = null;
  var likeString = null;
  var access_token = null;

  before(function(done) {
    User.remove({"local.email": testUserEMAIL}, function() {});
    likeStringModel.remove({}, function() {});
    BubbleModel.remove({slug: 'test'}, function() {});
    BubbleModel.remove({slug: 'test-bubble'}, function() {});

    api = api(app); //this starts the server, connects to database, etc...

    //Add the test account to be used for the tests
    api.post('/signup')
      .send({email: testUserEMAIL, password: 'itsasecret1234'})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.access_token.should.exist;
        access_token = res.body.access_token;

        User.findOne({"local.email": testUserEMAIL}, function(error, data) {
          if (error) return done(error);
          user = data;
          likeString = new LikeString(user);
          done();
        }); //Users.findOne()
      }); //Create new test user via POST signup
  });

  after(function(done) {
    likeStringModel.remove({user: user}, function(error) {});
    done();//User.remove({_id: new ObjectId(user.id)}, done);
  });

  it('should return a default set of strings and bubbles for a new user', function(done) {
    api.get(apiPrefix + 'strings')
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(error, res) {
        //Cross check the database to ensure that strings for this user are created too!
        if (error) return done(error);
        likeStringModel.find({user: likeString.user}).count(function(error, stringCount) {
          should.not.exist(error);
          stringCount.should.equal(config.strings.length);
          done();
        });
      });
  });

  it('should return the strings for a given user', function(done) {
    api.get(apiPrefix + 'strings')
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/, done);
  });

  it('should only return bubbles that exist', function(done) {
    api.get(apiPrefix + 'strings')
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(error, res) {
         if (error) return done(error);
         res.body.should.exist;
         var strings = res.body;
         for (var i in strings) {
           strings[i].bubbles.forEach(function(b) {
             (b != null).should.be.true; // Make sure null is not of the bubbles on a string.
             BubbleModel.find({_id: b._id}, function (error, rec) {
               should.not.exist(error);
               (rec != null).should.be.true; //The reference should point to a actual bubble document
             });
           }); //For each bubble...
         } //For each string...
         done();
      });
  });

  it('should add a new string', function(done) {
    api.post(apiPrefix + 'string/' + encodeURI("New String"))
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(/new-string/)
      .end(function(error, res) {
         if (error) return done(error);
         likeStringModel.remove({name: "New String"}, done);
      });
  });

  it('should reject addition of a duplicate string', function(done) {
    api.post(apiPrefix + 'string/' + encodeURI("New String"))
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(/new-string/)
      .end(function(error, res) {
          if (error) return done(error);
          api.post(apiPrefix + 'string/' + encodeURI("New String"))
            .query({access_token: encodeURI(access_token)})
            .expect(500)
            .expect(/duplicate key/)
            .end(function(error, res) {
               if (error) return done(error);
               likeStringModel.remove({name: "New String"}, done); 
            });
      });
  });

  it('should delete a string', function(done) {
    api.post(apiPrefix + 'string/' + encodeURI("Delete String"))
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(/delete-string/)
      .end(function(error, res) {
          if (error) return done(error);
          api.delete(apiPrefix + 'string/' + encodeURI("Delete String"))
            .query({access_token: encodeURI(access_token)})
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(/delete-string/, done);
      });
  });

  it('should throw an error when deleting a non-existent string', function(done) {
    api.post(apiPrefix + 'string/' + encodeURI("Delete String"))
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(/delete-string/)
      .end(function(error, res) {
          if (error) return done(error);
          api.delete(apiPrefix + 'string/' + encodeURI("Delete String"))
            .query({access_token: encodeURI(access_token)})
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(/delete-string/)
            .end(function(error, res) {
              api.delete(apiPrefix + 'string/' + encodeURI("Delete String"))
                .query({access_token: encodeURI(access_token)})
                .expect(500)
                .expect(/not found/, done);
            });
      });
  });

  it('should update the string meta data', function(done) {
    api.post(apiPrefix + 'string/' + encodeURI("Update String"))
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(/update-string/)
      .end(function(error, res) {
          if (error) return done(error);
          api.put(apiPrefix + 'string/' + encodeURI("Update String"))
            .query({access_token: encodeURI(access_token)})
            .send({name: "Updated String"})
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(/updated-string/, done);
      });
  });

  it('should get the list of bubbles for a given string', function(done) {
    //Create a sample string first
    likeStringModel.remove({slug: 'test-string'}, function() {});
    api.post(apiPrefix + 'string/' + encodeURI("Test String"))
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(/test-string/)
      .end(function(error, res) {
        //Create a sample bubble
        BubbleModel.remove({slug: 'test-bubble'}, function() {});
        var newBubble;        
        newBubble = new BubbleModel();
        newBubble.name = "Test Bubble";
        newBubble.slug = Utils.createSlug(newBubble.name);
        newBubble.description = "Test Bubble used for testing string operations";
        newBubble.contents = [];
        newBubble.save(function(error, bubble) { 
          if (error) done(error);
          (bubble !== null).should.be.true;
          //Add the new bubble to the newly created test string
          api.post(apiPrefix + 'string/' + encodeURI("Test String") + '/bubble/' + encodeURI('Test Bubble'))
            .query({access_token: encodeURI(access_token)})
            .expect(200)
            .expect(/test-bubble/)
            .end(function(error, res) {
              if (error) return done(error);
              //testing of the API is here
              api.get(apiPrefix + 'string/' + encodeURI("Test String") + '/bubbles')
                .query({access_token: encodeURI(access_token)})
                .expect(200)
                .expect(/test-bubble/)
                .end(function(error, res) {
                  if (error) return done(error);
                  res.body[0].bubbles.length.should.equal(1);
                  res.body[0].bubbles[0].name.should.equal(newBubble.name);
                  likeStringModel.findOneAndRemove({slug: 'test-string'}, function() {});
                  BubbleModel.findOneAndRemove({slug: 'test-bubble'}, done); //cleanup
                }); //Get bubbles for the given string 
            }); //Add a bubble to the string
        }); //Create a new bubble in the database
      }); //Creating the string() POST
  });

  it('should not return bubbles which have been deleted', function(done) {
    //Create a sample string first
    likeStringModel.remove({slug: 'test-string'}, function() {});
    api.post(apiPrefix + 'string/' + encodeURI("Test String"))
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(/test-string/)
      .end(function(error, res) {
        //Create a sample bubble
        BubbleModel.remove({slug: 'test-bubble'}, function() {});
        var newBubble;        
        newBubble = new BubbleModel();
        newBubble.name = "Test Bubble";
        newBubble.slug = Utils.createSlug(newBubble.name);
        newBubble.description = "Test Bubble used for testing string operations";
        newBubble.contents = [];
        newBubble.save(function(error, bubble) { 
          if (error) done(error);
          (bubble !== null).should.be.true;
          //Add the new bubble to the newly created test string
          api.post(apiPrefix + 'string/' + encodeURI("Test String") + '/bubble/' + encodeURI('Test Bubble'))
            .query({access_token: encodeURI(access_token)})
            .expect(200)
            .expect(/test-bubble/)
            .end(function(error, res) {
              if (error) return done(error);
              api.get(apiPrefix + 'string/' + encodeURI("Test String") + '/bubbles')
                .query({access_token: encodeURI(access_token)})
                .expect(200)
                .expect(/test-bubble/)
                .end(function(error, res) {
                  if (error) return done(error);
                  res.body[0].bubbles.length.should.equal(1);
                  res.body[0].bubbles[0].name.should.equal(newBubble.name);
                  //After the bubble was connected to the string, delete it!!!!
                  BubbleModel.findOneAndRemove({slug: 'test-bubble'}, function(error) {
                    if (error) return done(error);
                    //testing of the API is here: Now the bubble should not be returned for this string.
                    api.get(apiPrefix + 'string/' + encodeURI("Test String") + '/bubbles')
                      .query({access_token: encodeURI(access_token)})
                      .expect(200)
                      .end(function(error, res) {
                        if (error) return done(error);
                        res.body[0].bubbles.length.should.equal(0);
                        likeStringModel.findOneAndRemove({slug: 'test-string'}, function() {});
                        done();
                      });
                  });
                }); //Get bubbles for the given string 
            }); //Add a bubble to the string
        }); //Create a new bubble in the database
      }); //Creating the string() POST
  });

  it('should add a bubble to the given string', function(done) {
    //Create a sample string first
    likeStringModel.remove({slug: 'test-string'}, function() {});

    api.post(apiPrefix + 'string/' + encodeURI("Test String"))
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(/test-string/)
      .end(function(error, res) {
        //Create a sample bubble
        BubbleModel.remove({slug: 'test-bubble'}, function() {});
        var newBubble;        
        newBubble = new BubbleModel();
        newBubble.name = "Test Bubble";
        newBubble.slug = Utils.createSlug(newBubble.name);
        newBubble.description = "Test Bubble used for testing string operations";
        newBubble.contents = [];
        newBubble.save(function(error, bubble) { 
          if (error) done(error);
          (bubble !== null).should.be.true;
      
          //testing of the REST API is here.
          api.post(apiPrefix + 'string/' + encodeURI("Test String") + '/bubble/' + encodeURI('Test Bubble'))
            .query({access_token: encodeURI(access_token)})
            .expect(200)
            .expect(/test-bubble/)
            .end(function(error, res) {
              if (error) return done(error);
              //check the database to ensure the string has a reference to the new bubble
              likeStringModel.findOne({user: user, slug: 'test-string'}, function(error, dbRecord) {
                if (error) return done(error);
                (dbRecord !== null).should.be.true;
                dbRecord.bubbles[0].equals(bubble._id);
                likeStringModel.findOneAndRemove({slug: 'test-string'}, function() {});
                BubbleModel.findOneAndRemove({slug: 'test-bubble'}, done); //cleanup
              });
            });
        });
      }); //Creating the string() POST
  });

  it('should delete an existing bubble from a string', function(done) {
    //Create a sample string first
    likeStringModel.remove({slug: 'test-string'}, function() {});

    api.post(apiPrefix + 'string/' + encodeURI("Test String"))
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(/test-string/)
      .end(function(error, res) {
        //Create a sample bubble
        BubbleModel.remove({slug: 'test-bubble'}, function() {});
        var newBubble;        
        newBubble = new BubbleModel();
        newBubble.name = "Test Bubble";
        newBubble.slug = Utils.createSlug(newBubble.name);
        newBubble.description = "Test Bubble used for testing string operations";
        newBubble.contents = [];
        newBubble.save(function(error, bubble) { 
          if (error) done(error);
          (bubble !== null).should.be.true;
          //Add the newly created bubble to the test string
          api.post(apiPrefix + 'string/' + encodeURI("Test String") + '/bubble/' + encodeURI('Test Bubble'))
            .query({access_token: encodeURI(access_token)})
            .expect(200)
            .expect(/test-bubble/)
            .end(function(error, res){
              //testing of the REST API is here.
              api.delete(apiPrefix + 'string/' + encodeURI("Test String") + '/bubble/' + encodeURI('Test Bubble'))
                .query({access_token: encodeURI(access_token)})
                .expect(200)
                .expect(/test-bubble/)
                .end(function(error, res) {
                  if (error) return done(error);
                  //check the database to ensure the string has a reference to the new bubble
                  likeStringModel.findOne({user: user, slug: 'test-string'}, function(error, dbRecord) {
                    if (error) return done(error);
                    (dbRecord !== null).should.be.true;
                    dbRecord.bubbles.length.should.equal(0);
                    likeStringModel.findOneAndRemove({slug: 'test-string'}, function() {});
                    BubbleModel.findOneAndRemove({slug: 'test-bubble'}, done); //cleanup
                  });
                }); //Delete bubble API
            }); //Add Bubble POST
        }); //Create Bubble database record
      }); //Creating the string() POST
  });

  it('should not throw error when deleting a non-existent bubble from a string', function(done) {
    //Create a sample string first
    likeStringModel.remove({slug: 'test-string'}, function() {});

    api.post(apiPrefix + 'string/' + encodeURI("Test String"))
      .query({access_token: encodeURI(access_token)})
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(/test-string/)
      .end(function(error, res) {
        api.delete(apiPrefix + 'string/' + encodeURI("Test String") + '/bubble/' + encodeURI('Test Bubble'))
          .query({access_token: encodeURI(access_token)})
          .expect(200)
          .end(function(error, res) {
            if (error) return done(error);
            likeStringModel.findOneAndRemove({slug: 'test-string'}, done);
          }); //Delete bubble API
      }); //Creating the string() POST
  });

  it('should move a bubble from one likestring to another', function(done) {

    //Create a sample bubble
    BubbleModel.remove({slug: 'test-bubble'}, function() {});
    var newBubble;        
    newBubble = new BubbleModel();
    newBubble.name = "Test Bubble";
    newBubble.slug = Utils.createSlug(newBubble.name);
    newBubble.description = "Test Bubble used for moving bubble from one string to another";
    newBubble.contents = [];
    newBubble.save(function(error, bubble) { 
      if (error) done(error);
      (bubble !== null).should.be.true;
  
      //Create a sample strings first
      likeStringModel.remove({slug: 'from-string'}, function() {});
      likeStringModel.remove({slug: 'to-string'}, function() {});
      api.post(apiPrefix + 'string/' + encodeURI("From String"))
        .query({access_token: encodeURI(access_token)})
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(/from-string/)
        .end(function(error, res) {
          api.post(apiPrefix + 'string/' + encodeURI("To String"))
            .query({access_token: encodeURI(access_token)})
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(/to-string/)
            .end(function(error, res) {
              //Add the newly created bubble to the from string
              api.post(apiPrefix + 'string/' + encodeURI("From String") + '/bubble/' + encodeURI('Test Bubble'))
                .query({access_token: encodeURI(access_token)})
                .expect(200)
                .expect(/test-bubble/)
                .end(function(error, res){
                  //testing of the REST API is here.
                  api.put(apiPrefix + 'string/' + encodeURI("From String") + '/bubble/' + encodeURI('Test Bubble') +
                                      '/to/' + encodeURI("To String"))
                    .query({access_token: encodeURI(access_token)})
                    .expect(200)
                    .expect(/test-bubble/)
                    .end(function(error, res) {
                      if (error) return done(error);
                      //check the database to ensure the TO string has a reference to the bubble
                      likeStringModel.findOne({user: user, slug: 'to-string'}, function(error, dbRecord) {
                        if (error) return done(error);
                        (dbRecord !== null).should.be.true;
                        dbRecord.bubbles.length.should.equal(1);
                        dbRecord.bubbles[0].equals(bubble._id);
                        likeStringModel.findOneAndRemove({slug: 'from-string'}, function() {});
                        likeStringModel.findOneAndRemove({slug: 'to-string'}, function() {});                        
                        BubbleModel.findOneAndRemove({slug: 'test-bubble'}, done); //cleanup
                      });
                    }); //Move the bubble FROM string TO string
                }); //Add Bubble to FROM string
            }); //Add Bubble POST
        }); //Create FROM string
      }); //Create Bubble
  });

});