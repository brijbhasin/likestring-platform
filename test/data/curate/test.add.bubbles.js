// force the test environment to 'test'
process.env.NODE_ENV = 'test';
var config = require('config');
var http = require('http');
var assert = require('assert');
var should = require("should");

var app = require('../../../server');

//Database schemas
var Utils    = require('../../../app/models/utils');
var User = require("../../../app/models/user.js");
var BubbleCategory = require('../../../app/models/bubble_category');
var BubbleSchemas = require('../../../app/models/bubble'),
    BubbleModel = BubbleSchemas.Bubble,
    BubbleContentModel = BubbleSchemas.BubbleContent;

var twitterClient = require("../../../data/capture/twitter.js");
var twitterStore = require("../../../data/save/twitter.js");
var TwitterSource = require('../../../data/curate/twitter');
var TwitterUserProfile = require("../../../app/models/twitter_user_profile.js");
var TwitterClient = new twitterClient();
var TwitterStore = new twitterStore();



var Bubble = require('../../../data/curate/bubble');

describe('Content curation', function() {

  describe(' Bubble: ', function() {
  	var localUser = null;

    var bubble = new Bubble("Robin Williams");

  	before(function(done) {
      TwitterClient.getUserProfile('robinwilliams', function(err, user) {
        if (err) return done(err);
        TwitterStore.saveUser(user, function(error, user) {
          if (error) return done(error);
          TwitterUserProfile.find({id: user.id}, function(error, savedTwitterProfile) {
            if (error) done(error);
          });
        });
      });

      BubbleModel.remove({slug: 'robin-williams'}, function(err) {});
      User.remove({"local.email": "tester@example.com"}, function(err) {
      	//ignore errors
      	localUser = new User({local: {email: "tester@example.com", password: "test1234" }});
        localUser.save(function(error) {
          if (error) console.log('error' + error.message);
          done();
        });
      });
  	});


    it('should get all the tweets for a given twitter handle and save it to the database', function(done) {
      this.timeout(100000);
      TwitterClient.tweets('robinwilliams', {include_rts: false}, function(error, data) {
        if (error) done(error);
        TwitterStore.saveTweets(data, function(error, result) {
          if (error) done(error);
          result.should.exist;
          done();
        })
      });    
    });
    
    it ('should curate twitter data and add to the bubble contents', function(done) {
       this.timeout(100000);
       var params = {handle: 'robinwilliams'};
       var t = new TwitterSource(params);
       bubble.create({}, function(error, data) {
         should.not.exist(error);
         bubble.addContentSource(t, function(error, data) {
           should.not.exist(error);
           bubble.refreshContents(function(error, data) {
             should.not.exist(error);
             done();
           });
         });
       });
    });

  });
});