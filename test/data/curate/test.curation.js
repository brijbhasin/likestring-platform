// force the test environment to 'test'
process.env.NODE_ENV = 'test';
var config = require('config');
var http = require('http');
var assert = require('assert');
var should = require("should");

var app = require('../../../server');
var logger = require('../../../app/log');
logger.transports.console.level = 'debug'; //This could be any of these values: silly/debug/verbose/info/warn/error

//Database schemas
var Utils    = require('../../../app/models/utils');
var User = require("../../../app/models/user.js");
var BubbleCategory = require('../../../app/models/bubble_category');
var BubbleSchemas = require('../../../app/models/bubble'),
    BubbleModel = BubbleSchemas.Bubble,
    BubbleContentModel = BubbleSchemas.BubbleContent;

var BubbleDataSource = require('../../../data/curate/bubble_data_source');
var Bubble = require('../../../data/curate/bubble');


var bubbleName = "GSF India";
var bubbleDescription = "India’s leading technology accelerator & seed funding platform. 24 startups, 2 exits. 30 EiRs. #GSFGlobal: 6 demo days, 7 world tech cities, 13 weeks";
var bubbleImage = "http://pbs.twimg.com/profile_images/378800000220991383/918c9361ad3e05382e978e74cd67160f_normal.png";
var twitterHandle = "GSFIndia";

/*
var bubbleName = "Robin Williams";
var bubbleDescription = "Actor, Comedian, Cyclist, Retired Mime";
var bubbleImage = "https://pbs.twimg.com/profile_images/3161854344/11af6cfb0a45ec0ff0bedd8a5e1583a0_normal.jpeg";
var twitterHandle = "robinwilliams";


var bubbleName = 'Marc Andreessen';
var bubbleDescription = "FOR creators & contributors to technology, science, art, ideas, a better world. STRONG VIEWS, WEAKLY HELD. Proud solutionist since 1994.";
var bubbleImage = "https://pbs.twimg.com/profile_images/436973293053034496/Rc_YcyTy_normal.png";
var twitterHandle = 'pmarca';
*/


describe('Content curation', function() {

  describe(' Bubble: ', function() {
  	var localUser = null;

    var bubble = new Bubble(bubbleName);

  	before(function(done) {
      //BubbleModel.remove({slug: Utils.createSlug(bubbleName)}, function(err) {});
      User.remove({"local.email": "tester@example.com"}, function(err) {
      	//ignore errors
      	localUser = new User({local: {email: "tester@example.com", password: "test1234" }});
        localUser.save(function(error) {
          if (error) console.log('error' + error.message);
          done();
        });
      });
  	});

    it ('should create a bubble in the database', function(done) {
       var params = {};
       params.description = bubbleDescription;
       bubble.create(params, function(error, data) {
         should.not.exist(error);
         data.name.should.equal(params.name);
         done();
       });
    });

    it ('should throw error during creation if bubble already exists in the database', function(done) {
       bubble.create({}, function(error, data) {
         should.exist(error);
         error.err.should.containEql('duplicate key');
         done();
       });
    });

    it ('should modify the bubble\'s metadata with the given parameters', function(done) {
      var params = {};
      params.description = bubbleDescription;
      params.image = bubbleImage;
      bubble.setProperties(params, function(error, data) {
        should.not.exist(error);
        BubbleModel.findOne({slug: Utils.createSlug(bubbleName)}, {_id: 0, description: 1}, function(error, rec) {
          should.not.exist(error);
          rec.description.should.equal(params.description);
          done();
        })
      });
    });

    it ('should delete the bubble in the database', function(done) {
      BubbleModel.remove({slug: Utils.createSlug("GSFIndia")}, function(error, data) {
        should.not.exist(error);
        done();
      });
    });
    
    it ('should curate twitter data and add to the bubble contents', function(done) {
       this.timeout(150000);
       var params = {};
       params.name = "twitter";
       params.handle = twitterHandle;
       bubble.addContentSource(params, function(error, src) {
        (error === null).should.be.true;
          bubble.refreshContent(function(error, data) {
            (error === null).should.be.true;
            data.should.exist;
            done();
          });
       })
    });

    it ('should not add duplicate content to bubble');
  });
});